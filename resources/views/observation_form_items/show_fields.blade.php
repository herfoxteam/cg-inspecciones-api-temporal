<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $observationFormItem->id !!}</p>
</div>

<!-- Inspection Forms Items Code Field -->
<div class="form-group">
    {!! Form::label('inspection_forms_items_code', 'Inspection Forms Items Code:') !!}
    <p>{!! $observationFormItem->inspection_forms_items_code !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $observationFormItem->description !!}</p>
</div>

<!-- Hash Form Field -->
<div class="form-group">
    {!! Form::label('hash_form', 'Hash Form:') !!}
    <p>{!! $observationFormItem->hash_form !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $observationFormItem->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $observationFormItem->updated_at !!}</p>
</div>

