@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Observation Form Item
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($observationFormItem, ['route' => ['observationFormItems.update', $observationFormItem->id], 'method' => 'patch']) !!}

                        @include('observation_form_items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection