@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Status Inspection Form
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($statusInspectionForm, ['route' => ['statusInspectionForms.update', $statusInspectionForm->id], 'method' => 'patch']) !!}

                        @include('status_inspection_forms.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection