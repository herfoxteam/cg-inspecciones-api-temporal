@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Action Plan
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($actionPlan, ['route' => ['actionPlans.update', $actionPlan->id], 'method' => 'patch']) !!}

                        @include('action_plans.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection