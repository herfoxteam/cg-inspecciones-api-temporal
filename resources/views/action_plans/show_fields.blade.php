<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $actionPlan->id !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $actionPlan->description !!}</p>
</div>

<!-- Date Time End Field -->
<div class="form-group">
    {!! Form::label('date_time_end', 'Date Time End:') !!}
    <p>{!! $actionPlan->date_time_end !!}</p>
</div>

<!-- Assigned User Id Field -->
<div class="form-group">
    {!! Form::label('assigned_user_id', 'Assigned User Id:') !!}
    <p>{!! $actionPlan->assigned_user_id !!}</p>
</div>

<!-- Priorities Id Field -->
<div class="form-group">
    {!! Form::label('priorities_id', 'Priorities Id:') !!}
    <p>{!! $actionPlan->priorities_id !!}</p>
</div>

<!-- Inspections Form Item Code Field -->
<div class="form-group">
    {!! Form::label('inspections_form_item_code', 'Inspections Form Item Code:') !!}
    <p>{!! $actionPlan->inspections_form_item_code !!}</p>
</div>

<!-- Closed Field -->
<div class="form-group">
    {!! Form::label('closed', 'Closed:') !!}
    <p>{!! $actionPlan->closed !!}</p>
</div>

<!-- Hash Form Field -->
<div class="form-group">
    {!! Form::label('hash_form', 'Hash Form:') !!}
    <p>{!! $actionPlan->hash_form !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $actionPlan->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $actionPlan->updated_at !!}</p>
</div>

