<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $priorityActionPlan->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $priorityActionPlan->title !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $priorityActionPlan->description !!}</p>
</div>

<!-- Icon Class Field -->
<div class="form-group">
    {!! Form::label('icon_class', 'Icon Class:') !!}
    <p>{!! $priorityActionPlan->icon_class !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $priorityActionPlan->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $priorityActionPlan->updated_at !!}</p>
</div>

