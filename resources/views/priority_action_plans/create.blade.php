@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Priority Action Plan
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'priorityActionPlans.store']) !!}

                        @include('priority_action_plans.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
