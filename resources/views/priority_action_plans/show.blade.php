@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Priority Action Plan
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('priority_action_plans.show_fields')
                    <a href="{!! route('priorityActionPlans.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
