<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{!! route('roles.index') !!}"><i class="fa fa-edit"></i><span>Roles</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('userGroups*') ? 'active' : '' }}">
    <a href="{!! route('userGroups.index') !!}"><i class="fa fa-edit"></i><span>User Groups</span></a>
</li>

<li class="{{ Request::is('groupMembers*') ? 'active' : '' }}">
    <a href="{!! route('groupMembers.index') !!}"><i class="fa fa-edit"></i><span>Group Members</span></a>
</li>

<li class="{{ Request::is('avatars*') ? 'active' : '' }}">
    <a href="{!! route('avatars.index') !!}"><i class="fa fa-edit"></i><span>Avatars</span></a>
</li>

<li class="{{ Request::is('statusTemplateInspections*') ? 'active' : '' }}">
    <a href="{!! route('statusTemplateInspections.index') !!}"><i class="fa fa-edit"></i><span>Status Template Inspections</span></a>
</li>

<li class="{{ Request::is('templateInspections*') ? 'active' : '' }}">
    <a href="{!! route('templateInspections.index') !!}"><i class="fa fa-edit"></i><span>Template Inspections</span></a>
</li>

<li class="{{ Request::is('statusInspectionForms*') ? 'active' : '' }}">
    <a href="{!! route('statusInspectionForms.index') !!}"><i class="fa fa-edit"></i><span>Status Inspection Forms</span></a>
</li>

<li class="{{ Request::is('inspectionForms*') ? 'active' : '' }}">
    <a href="{!! route('inspectionForms.index') !!}"><i class="fa fa-edit"></i><span>Inspection Forms</span></a>
</li>

<li class="{{ Request::is('inspectionResponses*') ? 'active' : '' }}">
    <a href="{!! route('inspectionResponses.index') !!}"><i class="fa fa-edit"></i><span>Inspection Responses</span></a>
</li>

<li class="{{ Request::is('categoryInspectionForms*') ? 'active' : '' }}">
    <a href="{!! route('categoryInspectionForms.index') !!}"><i class="fa fa-edit"></i><span>Category Inspection Forms</span></a>
</li>

<li class="{{ Request::is('statusCategoryForms*') ? 'active' : '' }}">
    <a href="{!! route('statusCategoryForms.index') !!}"><i class="fa fa-edit"></i><span>Status Category Forms</span></a>
</li>

<li class="{{ Request::is('typeFormItems*') ? 'active' : '' }}">
    <a href="{!! route('typeFormItems.index') !!}"><i class="fa fa-edit"></i><span>Type Form Items</span></a>
</li>

<li class="{{ Request::is('inspectionFormItems*') ? 'active' : '' }}">
    <a href="{!! route('inspectionFormItems.index') !!}"><i class="fa fa-edit"></i><span>Inspection Form Items</span></a>
</li>

<li class="{{ Request::is('priorityActionPlans*') ? 'active' : '' }}">
    <a href="{!! route('priorityActionPlans.index') !!}"><i class="fa fa-edit"></i><span>Priority Action Plans</span></a>
</li>

<li class="{{ Request::is('actionPlans*') ? 'active' : '' }}">
    <a href="{!! route('actionPlans.index') !!}"><i class="fa fa-edit"></i><span>Action Plans</span></a>
</li>

<li class="{{ Request::is('observationFormItems*') ? 'active' : '' }}">
    <a href="{!! route('observationFormItems.index') !!}"><i class="fa fa-edit"></i><span>Observation Form Items</span></a>
</li>

<li class="{{ Request::is('optionFormItems*') ? 'active' : '' }}">
    <a href="{!! route('optionFormItems.index') !!}"><i class="fa fa-edit"></i><span>Option Form Items</span></a>
</li>

<li class="{{ Request::is('photographicEvidences*') ? 'active' : '' }}">
    <a href="{!! route('photographicEvidences.index') !!}"><i class="fa fa-edit"></i><span>Photographic Evidences</span></a>
</li>

<li class="{{ Request::is('typeRecurrences*') ? 'active' : '' }}">
    <a href="{!! route('typeRecurrences.index') !!}"><i class="fa fa-edit"></i><span>Type Recurrences</span></a>
</li>

<li class="{{ Request::is('customDateScheduleds*') ? 'active' : '' }}">
    <a href="{!! route('customDateScheduleds.index') !!}"><i class="fa fa-edit"></i><span>Custom Date Scheduleds</span></a>
</li>

<li class="{{ Request::is('recurrenceScheduleds*') ? 'active' : '' }}">
    <a href="{!! route('recurrenceScheduleds.index') !!}"><i class="fa fa-edit"></i><span>Recurrence Scheduleds</span></a>
</li>

