@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Status Category Form
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($statusCategoryForm, ['route' => ['statusCategoryForms.update', $statusCategoryForm->id], 'method' => 'patch']) !!}

                        @include('status_category_forms.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection