<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $customDateScheduled->id !!}</p>
</div>

<!-- Custom Date Field -->
<div class="form-group">
    {!! Form::label('custom_date', 'Custom Date:') !!}
    <p>{!! $customDateScheduled->custom_date !!}</p>
</div>

<!-- Recurrence Schedules Id Field -->
<div class="form-group">
    {!! Form::label('recurrence_schedules_id', 'Recurrence Schedules Id:') !!}
    <p>{!! $customDateScheduled->recurrence_schedules_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $customDateScheduled->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $customDateScheduled->updated_at !!}</p>
</div>

