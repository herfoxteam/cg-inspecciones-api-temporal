<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $categoryInspectionForm->id !!}</p>
</div>

<!-- Location Value Field -->
<div class="form-group">
    {!! Form::label('location_value', 'Location Value:') !!}
    <p>{!! $categoryInspectionForm->location_value !!}</p>
</div>

<!-- Label Value Field -->
<div class="form-group">
    {!! Form::label('label_value', 'Label Value:') !!}
    <p>{!! $categoryInspectionForm->label_value !!}</p>
</div>

<!-- Icon Class Field -->
<div class="form-group">
    {!! Form::label('icon_class', 'Icon Class:') !!}
    <p>{!! $categoryInspectionForm->icon_class !!}</p>
</div>

<!-- Template Code Field -->
<div class="form-group">
    {!! Form::label('template_code', 'Template Code:') !!}
    <p>{!! $categoryInspectionForm->template_code !!}</p>
</div>

<!-- Status Categories Forms Id Field -->
<div class="form-group">
    {!! Form::label('status_categories_forms_id', 'Status Categories Forms Id:') !!}
    <p>{!! $categoryInspectionForm->status_categories_forms_id !!}</p>
</div>

<!-- Hash Category Field -->
<div class="form-group">
    {!! Form::label('hash_category', 'Hash Category:') !!}
    <p>{!! $categoryInspectionForm->hash_category !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $categoryInspectionForm->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $categoryInspectionForm->updated_at !!}</p>
</div>

