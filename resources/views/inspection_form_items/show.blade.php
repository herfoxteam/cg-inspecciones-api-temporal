@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Inspection Form Item
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('inspection_form_items.show_fields')
                    <a href="{!! route('inspectionFormItems.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
