<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $inspectionFormItem->id !!}</p>
</div>

<!-- Category Hash Field -->
<div class="form-group">
    {!! Form::label('category_hash', 'Category Hash:') !!}
    <p>{!! $inspectionFormItem->category_hash !!}</p>
</div>

<!-- Label Field -->
<div class="form-group">
    {!! Form::label('label', 'Label:') !!}
    <p>{!! $inspectionFormItem->label !!}</p>
</div>

<!-- Type Forms Items Id Field -->
<div class="form-group">
    {!! Form::label('type_forms_items_id', 'Type Forms Items Id:') !!}
    <p>{!! $inspectionFormItem->type_forms_items_id !!}</p>
</div>

<!-- Cod Item Field -->
<div class="form-group">
    {!! Form::label('cod_item', 'Cod Item:') !!}
    <p>{!! $inspectionFormItem->cod_item !!}</p>
</div>

<!-- Required Field -->
<div class="form-group">
    {!! Form::label('required', 'Required:') !!}
    <p>{!! $inspectionFormItem->required !!}</p>
</div>

<!-- Published Field -->
<div class="form-group">
    {!! Form::label('published', 'Published:') !!}
    <p>{!! $inspectionFormItem->published !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $inspectionFormItem->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $inspectionFormItem->updated_at !!}</p>
</div>

