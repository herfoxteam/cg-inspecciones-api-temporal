<!-- Required Field -->
<div class="form-group col-sm-12">
    {!! Form::label('required', 'Required:') !!}
    <label class="radio-inline">
        {!! Form::radio('required', "1", null) !!} Si
    </label>

    <label class="radio-inline">
        {!! Form::radio('required', "0", null) !!} No
    </label>

</div>

<!-- Published Field -->
<div class="form-group col-sm-12">
    {!! Form::label('published', 'Published:') !!}
    <label class="radio-inline">
        {!! Form::radio('published', "1", null) !!} Si
    </label>

    <label class="radio-inline">
        {!! Form::radio('published', "0", null) !!} No
    </label>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('inspectionFormItems.index') !!}" class="btn btn-default">Cancel</a>
</div>
