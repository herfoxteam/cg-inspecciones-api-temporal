@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Inspection Form Item
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($inspectionFormItem, ['route' => ['inspectionFormItems.update', $inspectionFormItem->id], 'method' => 'patch']) !!}

                        @include('inspection_form_items.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection