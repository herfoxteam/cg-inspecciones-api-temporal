<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $inspectionResponse->id !!}</p>
</div>

<!-- Form Response Field -->
<div class="form-group">
    {!! Form::label('form_response', 'Form Response:') !!}
    <p>{!! $inspectionResponse->form_response !!}</p>
</div>

<!-- Inspections Forms Hash Field -->
<div class="form-group">
    {!! Form::label('inspections_forms_hash', 'Inspections Forms Hash:') !!}
    <p>{!! $inspectionResponse->inspections_forms_hash !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $inspectionResponse->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $inspectionResponse->updated_at !!}</p>
</div>

