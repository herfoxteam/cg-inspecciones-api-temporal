@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Inspection Response
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($inspectionResponse, ['route' => ['inspectionResponses.update', $inspectionResponse->id], 'method' => 'patch']) !!}

                        @include('inspection_responses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection