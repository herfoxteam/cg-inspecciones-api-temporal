@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Recurrence Scheduled
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($recurrenceScheduled, ['route' => ['recurrenceScheduleds.update', $recurrenceScheduled->id], 'method' => 'patch']) !!}

                        @include('recurrence_scheduleds.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection