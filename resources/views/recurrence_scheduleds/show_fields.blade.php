<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $recurrenceScheduled->id !!}</p>
</div>

<!-- Type Recurrence Id Field -->
<div class="form-group">
    {!! Form::label('type_recurrence_id', 'Type Recurrence Id:') !!}
    <p>{!! $recurrenceScheduled->type_recurrence_id !!}</p>
</div>

<!-- Active Field -->
<div class="form-group">
    {!! Form::label('active', 'Active:') !!}
    <p>{!! $recurrenceScheduled->active !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $recurrenceScheduled->user_id !!}</p>
</div>

<!-- Template Id Field -->
<div class="form-group">
    {!! Form::label('template_id', 'Template Id:') !!}
    <p>{!! $recurrenceScheduled->template_id !!}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{!! $recurrenceScheduled->start_date !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $recurrenceScheduled->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $recurrenceScheduled->updated_at !!}</p>
</div>

