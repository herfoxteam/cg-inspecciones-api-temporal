<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $inspectionForm->id !!}</p>
</div>

<!-- Hash Field -->
<div class="form-group">
    {!! Form::label('hash', 'Hash:') !!}
    <p>{!! $inspectionForm->hash !!}</p>
</div>

<!-- Template Code Field -->
<div class="form-group">
    {!! Form::label('template_code', 'Template Code:') !!}
    <p>{!! $inspectionForm->template_code !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $inspectionForm->user_id !!}</p>
</div>

<!-- Status Inspection Form Id Field -->
<div class="form-group">
    {!! Form::label('status_inspection_form_id', 'Status Inspection Form Id:') !!}
    <p>{!! $inspectionForm->status_inspection_form_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $inspectionForm->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $inspectionForm->updated_at !!}</p>
</div>

