@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Status Template Inspection
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($statusTemplateInspection, ['route' => ['statusTemplateInspections.update', $statusTemplateInspection->id], 'method' => 'patch']) !!}

                        @include('status_template_inspections.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection