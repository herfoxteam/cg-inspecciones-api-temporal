<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $photographicEvidence->id !!}</p>
</div>

<!-- Inspection Forms Items Code Field -->
<div class="form-group">
    {!! Form::label('inspection_forms_items_code', 'Inspection Forms Items Code:') !!}
    <p>{!! $photographicEvidence->inspection_forms_items_code !!}</p>
</div>

<!-- Thumbnail Field -->
<div class="form-group">
    {!! Form::label('thumbnail', 'Thumbnail:') !!}
    <p>{!! $photographicEvidence->thumbnail !!}</p>
</div>

<!-- Original File Field -->
<div class="form-group">
    {!! Form::label('original_file', 'Original File:') !!}
    <p>{!! $photographicEvidence->original_file !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $photographicEvidence->image !!}</p>
</div>

<!-- Hash Form Field -->
<div class="form-group">
    {!! Form::label('hash_form', 'Hash Form:') !!}
    <p>{!! $photographicEvidence->hash_form !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $photographicEvidence->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $photographicEvidence->updated_at !!}</p>
</div>

