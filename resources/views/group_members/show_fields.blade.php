<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $groupMember->id !!}</p>
</div>

<!-- User Group Id Field -->
<div class="form-group">
    {!! Form::label('user_group_id', 'User Group Id:') !!}
    <p>{!! $groupMember->user_group_id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $groupMember->user_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $groupMember->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $groupMember->updated_at !!}</p>
</div>

