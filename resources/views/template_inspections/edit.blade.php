@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Template Inspection
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($templateInspection, ['route' => ['templateInspections.update', $templateInspection->id], 'method' => 'patch']) !!}

                        @include('template_inspections.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection