<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $typeFormItem->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $typeFormItem->title !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $typeFormItem->description !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $typeFormItem->type !!}</p>
</div>

<!-- Icon Name Field -->
<div class="form-group">
    {!! Form::label('icon_name', 'Icon Name:') !!}
    <p>{!! $typeFormItem->icon_name !!}</p>
</div>

<!-- Rules Field -->
<div class="form-group">
    {!! Form::label('rules', 'Rules:') !!}
    <p>{!! $typeFormItem->rules !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $typeFormItem->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $typeFormItem->updated_at !!}</p>
</div>

