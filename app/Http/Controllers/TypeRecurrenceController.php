<?php

namespace App\Http\Controllers;

use App\DataTables\TypeRecurrenceDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTypeRecurrenceRequest;
use App\Http\Requests\UpdateTypeRecurrenceRequest;
use App\Repositories\TypeRecurrenceRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TypeRecurrenceController extends AppBaseController
{
    /** @var  TypeRecurrenceRepository */
    private $typeRecurrenceRepository;

    public function __construct(TypeRecurrenceRepository $typeRecurrenceRepo)
    {
        $this->typeRecurrenceRepository = $typeRecurrenceRepo;
    }

    /**
     * Display a listing of the TypeRecurrence.
     *
     * @param TypeRecurrenceDataTable $typeRecurrenceDataTable
     * @return Response
     */
    public function index(TypeRecurrenceDataTable $typeRecurrenceDataTable)
    {
        return $typeRecurrenceDataTable->render('type_recurrences.index');
    }

    /**
     * Show the form for creating a new TypeRecurrence.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_recurrences.create');
    }

    /**
     * Store a newly created TypeRecurrence in storage.
     *
     * @param CreateTypeRecurrenceRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeRecurrenceRequest $request)
    {
        $input = $request->all();

        $typeRecurrence = $this->typeRecurrenceRepository->create($input);

        Flash::success('Type Recurrence saved successfully.');

        return redirect(route('typeRecurrences.index'));
    }

    /**
     * Display the specified TypeRecurrence.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeRecurrence = $this->typeRecurrenceRepository->findWithoutFail($id);

        if (empty($typeRecurrence)) {
            Flash::error('Type Recurrence not found');

            return redirect(route('typeRecurrences.index'));
        }

        return view('type_recurrences.show')->with('typeRecurrence', $typeRecurrence);
    }

    /**
     * Show the form for editing the specified TypeRecurrence.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeRecurrence = $this->typeRecurrenceRepository->findWithoutFail($id);

        if (empty($typeRecurrence)) {
            Flash::error('Type Recurrence not found');

            return redirect(route('typeRecurrences.index'));
        }

        return view('type_recurrences.edit')->with('typeRecurrence', $typeRecurrence);
    }

    /**
     * Update the specified TypeRecurrence in storage.
     *
     * @param  int              $id
     * @param UpdateTypeRecurrenceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeRecurrenceRequest $request)
    {
        $typeRecurrence = $this->typeRecurrenceRepository->findWithoutFail($id);

        if (empty($typeRecurrence)) {
            Flash::error('Type Recurrence not found');

            return redirect(route('typeRecurrences.index'));
        }

        $typeRecurrence = $this->typeRecurrenceRepository->update($request->all(), $id);

        Flash::success('Type Recurrence updated successfully.');

        return redirect(route('typeRecurrences.index'));
    }

    /**
     * Remove the specified TypeRecurrence from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeRecurrence = $this->typeRecurrenceRepository->findWithoutFail($id);

        if (empty($typeRecurrence)) {
            Flash::error('Type Recurrence not found');

            return redirect(route('typeRecurrences.index'));
        }

        $this->typeRecurrenceRepository->delete($id);

        Flash::success('Type Recurrence deleted successfully.');

        return redirect(route('typeRecurrences.index'));
    }
}
