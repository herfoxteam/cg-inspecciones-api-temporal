<?php

namespace App\Http\Controllers;

use App\DataTables\RecurrenceScheduledDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRecurrenceScheduledRequest;
use App\Http\Requests\UpdateRecurrenceScheduledRequest;
use App\Repositories\RecurrenceScheduledRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class RecurrenceScheduledController extends AppBaseController
{
    /** @var  RecurrenceScheduledRepository */
    private $recurrenceScheduledRepository;

    public function __construct(RecurrenceScheduledRepository $recurrenceScheduledRepo)
    {
        $this->recurrenceScheduledRepository = $recurrenceScheduledRepo;
    }

    /**
     * Display a listing of the RecurrenceScheduled.
     *
     * @param RecurrenceScheduledDataTable $recurrenceScheduledDataTable
     * @return Response
     */
    public function index(RecurrenceScheduledDataTable $recurrenceScheduledDataTable)
    {
        return $recurrenceScheduledDataTable->render('recurrence_scheduleds.index');
    }

    /**
     * Show the form for creating a new RecurrenceScheduled.
     *
     * @return Response
     */
    public function create()
    {
        return view('recurrence_scheduleds.create');
    }

    /**
     * Store a newly created RecurrenceScheduled in storage.
     *
     * @param CreateRecurrenceScheduledRequest $request
     *
     * @return Response
     */
    public function store(CreateRecurrenceScheduledRequest $request)
    {
        $input = $request->all();

        $recurrenceScheduled = $this->recurrenceScheduledRepository->create($input);

        Flash::success('Recurrence Scheduled saved successfully.');

        return redirect(route('recurrenceScheduleds.index'));
    }

    /**
     * Display the specified RecurrenceScheduled.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $recurrenceScheduled = $this->recurrenceScheduledRepository->findWithoutFail($id);

        if (empty($recurrenceScheduled)) {
            Flash::error('Recurrence Scheduled not found');

            return redirect(route('recurrenceScheduleds.index'));
        }

        return view('recurrence_scheduleds.show')->with('recurrenceScheduled', $recurrenceScheduled);
    }

    /**
     * Show the form for editing the specified RecurrenceScheduled.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $recurrenceScheduled = $this->recurrenceScheduledRepository->findWithoutFail($id);

        if (empty($recurrenceScheduled)) {
            Flash::error('Recurrence Scheduled not found');

            return redirect(route('recurrenceScheduleds.index'));
        }

        return view('recurrence_scheduleds.edit')->with('recurrenceScheduled', $recurrenceScheduled);
    }

    /**
     * Update the specified RecurrenceScheduled in storage.
     *
     * @param  int              $id
     * @param UpdateRecurrenceScheduledRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRecurrenceScheduledRequest $request)
    {
        $recurrenceScheduled = $this->recurrenceScheduledRepository->findWithoutFail($id);

        if (empty($recurrenceScheduled)) {
            Flash::error('Recurrence Scheduled not found');

            return redirect(route('recurrenceScheduleds.index'));
        }

        $recurrenceScheduled = $this->recurrenceScheduledRepository->update($request->all(), $id);

        Flash::success('Recurrence Scheduled updated successfully.');

        return redirect(route('recurrenceScheduleds.index'));
    }

    /**
     * Remove the specified RecurrenceScheduled from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $recurrenceScheduled = $this->recurrenceScheduledRepository->findWithoutFail($id);

        if (empty($recurrenceScheduled)) {
            Flash::error('Recurrence Scheduled not found');

            return redirect(route('recurrenceScheduleds.index'));
        }

        $this->recurrenceScheduledRepository->delete($id);

        Flash::success('Recurrence Scheduled deleted successfully.');

        return redirect(route('recurrenceScheduleds.index'));
    }
}
