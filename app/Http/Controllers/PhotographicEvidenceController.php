<?php

namespace App\Http\Controllers;

use App\DataTables\PhotographicEvidenceDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePhotographicEvidenceRequest;
use App\Http\Requests\UpdatePhotographicEvidenceRequest;
use App\Repositories\PhotographicEvidenceRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PhotographicEvidenceController extends AppBaseController
{
    /** @var  PhotographicEvidenceRepository */
    private $photographicEvidenceRepository;

    public function __construct(PhotographicEvidenceRepository $photographicEvidenceRepo)
    {
        $this->photographicEvidenceRepository = $photographicEvidenceRepo;
    }

    /**
     * Display a listing of the PhotographicEvidence.
     *
     * @param PhotographicEvidenceDataTable $photographicEvidenceDataTable
     * @return Response
     */
    public function index(PhotographicEvidenceDataTable $photographicEvidenceDataTable)
    {
        return $photographicEvidenceDataTable->render('photographic_evidences.index');
    }

    /**
     * Show the form for creating a new PhotographicEvidence.
     *
     * @return Response
     */
    public function create()
    {
        return view('photographic_evidences.create');
    }

    /**
     * Store a newly created PhotographicEvidence in storage.
     *
     * @param CreatePhotographicEvidenceRequest $request
     *
     * @return Response
     */
    public function store(CreatePhotographicEvidenceRequest $request)
    {
        $input = $request->all();

        $photographicEvidence = $this->photographicEvidenceRepository->create($input);

        Flash::success('Photographic Evidence saved successfully.');

        return redirect(route('photographicEvidences.index'));
    }

    /**
     * Display the specified PhotographicEvidence.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $photographicEvidence = $this->photographicEvidenceRepository->findWithoutFail($id);

        if (empty($photographicEvidence)) {
            Flash::error('Photographic Evidence not found');

            return redirect(route('photographicEvidences.index'));
        }

        return view('photographic_evidences.show')->with('photographicEvidence', $photographicEvidence);
    }

    /**
     * Show the form for editing the specified PhotographicEvidence.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $photographicEvidence = $this->photographicEvidenceRepository->findWithoutFail($id);

        if (empty($photographicEvidence)) {
            Flash::error('Photographic Evidence not found');

            return redirect(route('photographicEvidences.index'));
        }

        return view('photographic_evidences.edit')->with('photographicEvidence', $photographicEvidence);
    }

    /**
     * Update the specified PhotographicEvidence in storage.
     *
     * @param  int              $id
     * @param UpdatePhotographicEvidenceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePhotographicEvidenceRequest $request)
    {
        $photographicEvidence = $this->photographicEvidenceRepository->findWithoutFail($id);

        if (empty($photographicEvidence)) {
            Flash::error('Photographic Evidence not found');

            return redirect(route('photographicEvidences.index'));
        }

        $photographicEvidence = $this->photographicEvidenceRepository->update($request->all(), $id);

        Flash::success('Photographic Evidence updated successfully.');

        return redirect(route('photographicEvidences.index'));
    }

    /**
     * Remove the specified PhotographicEvidence from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $photographicEvidence = $this->photographicEvidenceRepository->findWithoutFail($id);

        if (empty($photographicEvidence)) {
            Flash::error('Photographic Evidence not found');

            return redirect(route('photographicEvidences.index'));
        }

        $this->photographicEvidenceRepository->delete($id);

        Flash::success('Photographic Evidence deleted successfully.');

        return redirect(route('photographicEvidences.index'));
    }
}
