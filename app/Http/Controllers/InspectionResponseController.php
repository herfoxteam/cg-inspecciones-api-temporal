<?php

namespace App\Http\Controllers;

use App\DataTables\InspectionResponseDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateInspectionResponseRequest;
use App\Http\Requests\UpdateInspectionResponseRequest;
use App\Repositories\InspectionResponseRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class InspectionResponseController extends AppBaseController
{
    /** @var  InspectionResponseRepository */
    private $inspectionResponseRepository;

    public function __construct(InspectionResponseRepository $inspectionResponseRepo)
    {
        $this->inspectionResponseRepository = $inspectionResponseRepo;
    }

    /**
     * Display a listing of the InspectionResponse.
     *
     * @param InspectionResponseDataTable $inspectionResponseDataTable
     * @return Response
     */
    public function index(InspectionResponseDataTable $inspectionResponseDataTable)
    {
        return $inspectionResponseDataTable->render('inspection_responses.index');
    }

    /**
     * Show the form for creating a new InspectionResponse.
     *
     * @return Response
     */
    public function create()
    {
        return view('inspection_responses.create');
    }

    /**
     * Store a newly created InspectionResponse in storage.
     *
     * @param CreateInspectionResponseRequest $request
     *
     * @return Response
     */
    public function store(CreateInspectionResponseRequest $request)
    {
        $input = $request->all();

        $inspectionResponse = $this->inspectionResponseRepository->create($input);

        Flash::success('Inspection Response saved successfully.');

        return redirect(route('inspectionResponses.index'));
    }

    /**
     * Display the specified InspectionResponse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $inspectionResponse = $this->inspectionResponseRepository->findWithoutFail($id);

        if (empty($inspectionResponse)) {
            Flash::error('Inspection Response not found');

            return redirect(route('inspectionResponses.index'));
        }

        return view('inspection_responses.show')->with('inspectionResponse', $inspectionResponse);
    }

    /**
     * Show the form for editing the specified InspectionResponse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $inspectionResponse = $this->inspectionResponseRepository->findWithoutFail($id);

        if (empty($inspectionResponse)) {
            Flash::error('Inspection Response not found');

            return redirect(route('inspectionResponses.index'));
        }

        return view('inspection_responses.edit')->with('inspectionResponse', $inspectionResponse);
    }

    /**
     * Update the specified InspectionResponse in storage.
     *
     * @param  int              $id
     * @param UpdateInspectionResponseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInspectionResponseRequest $request)
    {
        $inspectionResponse = $this->inspectionResponseRepository->findWithoutFail($id);

        if (empty($inspectionResponse)) {
            Flash::error('Inspection Response not found');

            return redirect(route('inspectionResponses.index'));
        }

        $inspectionResponse = $this->inspectionResponseRepository->update($request->all(), $id);

        Flash::success('Inspection Response updated successfully.');

        return redirect(route('inspectionResponses.index'));
    }

    /**
     * Remove the specified InspectionResponse from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $inspectionResponse = $this->inspectionResponseRepository->findWithoutFail($id);

        if (empty($inspectionResponse)) {
            Flash::error('Inspection Response not found');

            return redirect(route('inspectionResponses.index'));
        }

        $this->inspectionResponseRepository->delete($id);

        Flash::success('Inspection Response deleted successfully.');

        return redirect(route('inspectionResponses.index'));
    }
}
