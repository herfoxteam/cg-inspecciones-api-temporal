<?php

namespace App\Http\Controllers;

use App\DataTables\PriorityActionPlanDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePriorityActionPlanRequest;
use App\Http\Requests\UpdatePriorityActionPlanRequest;
use App\Repositories\PriorityActionPlanRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class PriorityActionPlanController extends AppBaseController
{
    /** @var  PriorityActionPlanRepository */
    private $priorityActionPlanRepository;

    public function __construct(PriorityActionPlanRepository $priorityActionPlanRepo)
    {
        $this->priorityActionPlanRepository = $priorityActionPlanRepo;
    }

    /**
     * Display a listing of the PriorityActionPlan.
     *
     * @param PriorityActionPlanDataTable $priorityActionPlanDataTable
     * @return Response
     */
    public function index(PriorityActionPlanDataTable $priorityActionPlanDataTable)
    {
        return $priorityActionPlanDataTable->render('priority_action_plans.index');
    }

    /**
     * Show the form for creating a new PriorityActionPlan.
     *
     * @return Response
     */
    public function create()
    {
        return view('priority_action_plans.create');
    }

    /**
     * Store a newly created PriorityActionPlan in storage.
     *
     * @param CreatePriorityActionPlanRequest $request
     *
     * @return Response
     */
    public function store(CreatePriorityActionPlanRequest $request)
    {
        $input = $request->all();

        $priorityActionPlan = $this->priorityActionPlanRepository->create($input);

        Flash::success('Priority Action Plan saved successfully.');

        return redirect(route('priorityActionPlans.index'));
    }

    /**
     * Display the specified PriorityActionPlan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $priorityActionPlan = $this->priorityActionPlanRepository->findWithoutFail($id);

        if (empty($priorityActionPlan)) {
            Flash::error('Priority Action Plan not found');

            return redirect(route('priorityActionPlans.index'));
        }

        return view('priority_action_plans.show')->with('priorityActionPlan', $priorityActionPlan);
    }

    /**
     * Show the form for editing the specified PriorityActionPlan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $priorityActionPlan = $this->priorityActionPlanRepository->findWithoutFail($id);

        if (empty($priorityActionPlan)) {
            Flash::error('Priority Action Plan not found');

            return redirect(route('priorityActionPlans.index'));
        }

        return view('priority_action_plans.edit')->with('priorityActionPlan', $priorityActionPlan);
    }

    /**
     * Update the specified PriorityActionPlan in storage.
     *
     * @param  int              $id
     * @param UpdatePriorityActionPlanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePriorityActionPlanRequest $request)
    {
        $priorityActionPlan = $this->priorityActionPlanRepository->findWithoutFail($id);

        if (empty($priorityActionPlan)) {
            Flash::error('Priority Action Plan not found');

            return redirect(route('priorityActionPlans.index'));
        }

        $priorityActionPlan = $this->priorityActionPlanRepository->update($request->all(), $id);

        Flash::success('Priority Action Plan updated successfully.');

        return redirect(route('priorityActionPlans.index'));
    }

    /**
     * Remove the specified PriorityActionPlan from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $priorityActionPlan = $this->priorityActionPlanRepository->findWithoutFail($id);

        if (empty($priorityActionPlan)) {
            Flash::error('Priority Action Plan not found');

            return redirect(route('priorityActionPlans.index'));
        }

        $this->priorityActionPlanRepository->delete($id);

        Flash::success('Priority Action Plan deleted successfully.');

        return redirect(route('priorityActionPlans.index'));
    }
}
