<?php

namespace App\Http\Controllers;

use App\DataTables\InspectionFormDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateInspectionFormRequest;
use App\Http\Requests\UpdateInspectionFormRequest;
use App\Repositories\InspectionFormRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class InspectionFormController extends AppBaseController
{
    /** @var  InspectionFormRepository */
    private $inspectionFormRepository;

    public function __construct(InspectionFormRepository $inspectionFormRepo)
    {
        $this->inspectionFormRepository = $inspectionFormRepo;
    }

    /**
     * Display a listing of the InspectionForm.
     *
     * @param InspectionFormDataTable $inspectionFormDataTable
     * @return Response
     */
    public function index(InspectionFormDataTable $inspectionFormDataTable)
    {
        return $inspectionFormDataTable->render('inspection_forms.index');
    }

    /**
     * Show the form for creating a new InspectionForm.
     *
     * @return Response
     */
    public function create()
    {
        return view('inspection_forms.create');
    }

    /**
     * Store a newly created InspectionForm in storage.
     *
     * @param CreateInspectionFormRequest $request
     *
     * @return Response
     */
    public function store(CreateInspectionFormRequest $request)
    {
        $input = $request->all();

        $inspectionForm = $this->inspectionFormRepository->create($input);

        Flash::success('Inspection Form saved successfully.');

        return redirect(route('inspectionForms.index'));
    }

    /**
     * Display the specified InspectionForm.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $inspectionForm = $this->inspectionFormRepository->findWithoutFail($id);

        if (empty($inspectionForm)) {
            Flash::error('Inspection Form not found');

            return redirect(route('inspectionForms.index'));
        }

        return view('inspection_forms.show')->with('inspectionForm', $inspectionForm);
    }

    /**
     * Show the form for editing the specified InspectionForm.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $inspectionForm = $this->inspectionFormRepository->findWithoutFail($id);

        if (empty($inspectionForm)) {
            Flash::error('Inspection Form not found');

            return redirect(route('inspectionForms.index'));
        }

        return view('inspection_forms.edit')->with('inspectionForm', $inspectionForm);
    }

    /**
     * Update the specified InspectionForm in storage.
     *
     * @param  int              $id
     * @param UpdateInspectionFormRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInspectionFormRequest $request)
    {
        $inspectionForm = $this->inspectionFormRepository->findWithoutFail($id);

        if (empty($inspectionForm)) {
            Flash::error('Inspection Form not found');

            return redirect(route('inspectionForms.index'));
        }

        $inspectionForm = $this->inspectionFormRepository->update($request->all(), $id);

        Flash::success('Inspection Form updated successfully.');

        return redirect(route('inspectionForms.index'));
    }

    /**
     * Remove the specified InspectionForm from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $inspectionForm = $this->inspectionFormRepository->findWithoutFail($id);

        if (empty($inspectionForm)) {
            Flash::error('Inspection Form not found');

            return redirect(route('inspectionForms.index'));
        }

        $this->inspectionFormRepository->delete($id);

        Flash::success('Inspection Form deleted successfully.');

        return redirect(route('inspectionForms.index'));
    }
}
