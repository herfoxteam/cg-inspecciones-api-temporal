<?php

namespace App\Http\Controllers;

use App\DataTables\StatusTemplateInspectionDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStatusTemplateInspectionRequest;
use App\Http\Requests\UpdateStatusTemplateInspectionRequest;
use App\Repositories\StatusTemplateInspectionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class StatusTemplateInspectionController extends AppBaseController
{
    /** @var  StatusTemplateInspectionRepository */
    private $statusTemplateInspectionRepository;

    public function __construct(StatusTemplateInspectionRepository $statusTemplateInspectionRepo)
    {
        $this->statusTemplateInspectionRepository = $statusTemplateInspectionRepo;
    }

    /**
     * Display a listing of the StatusTemplateInspection.
     *
     * @param StatusTemplateInspectionDataTable $statusTemplateInspectionDataTable
     * @return Response
     */
    public function index(StatusTemplateInspectionDataTable $statusTemplateInspectionDataTable)
    {
        return $statusTemplateInspectionDataTable->render('status_template_inspections.index');
    }

    /**
     * Show the form for creating a new StatusTemplateInspection.
     *
     * @return Response
     */
    public function create()
    {
        return view('status_template_inspections.create');
    }

    /**
     * Store a newly created StatusTemplateInspection in storage.
     *
     * @param CreateStatusTemplateInspectionRequest $request
     *
     * @return Response
     */
    public function store(CreateStatusTemplateInspectionRequest $request)
    {
        $input = $request->all();

        $statusTemplateInspection = $this->statusTemplateInspectionRepository->create($input);

        Flash::success('Status Template Inspection saved successfully.');

        return redirect(route('statusTemplateInspections.index'));
    }

    /**
     * Display the specified StatusTemplateInspection.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $statusTemplateInspection = $this->statusTemplateInspectionRepository->findWithoutFail($id);

        if (empty($statusTemplateInspection)) {
            Flash::error('Status Template Inspection not found');

            return redirect(route('statusTemplateInspections.index'));
        }

        return view('status_template_inspections.show')->with('statusTemplateInspection', $statusTemplateInspection);
    }

    /**
     * Show the form for editing the specified StatusTemplateInspection.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $statusTemplateInspection = $this->statusTemplateInspectionRepository->findWithoutFail($id);

        if (empty($statusTemplateInspection)) {
            Flash::error('Status Template Inspection not found');

            return redirect(route('statusTemplateInspections.index'));
        }

        return view('status_template_inspections.edit')->with('statusTemplateInspection', $statusTemplateInspection);
    }

    /**
     * Update the specified StatusTemplateInspection in storage.
     *
     * @param  int              $id
     * @param UpdateStatusTemplateInspectionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStatusTemplateInspectionRequest $request)
    {
        $statusTemplateInspection = $this->statusTemplateInspectionRepository->findWithoutFail($id);

        if (empty($statusTemplateInspection)) {
            Flash::error('Status Template Inspection not found');

            return redirect(route('statusTemplateInspections.index'));
        }

        $statusTemplateInspection = $this->statusTemplateInspectionRepository->update($request->all(), $id);

        Flash::success('Status Template Inspection updated successfully.');

        return redirect(route('statusTemplateInspections.index'));
    }

    /**
     * Remove the specified StatusTemplateInspection from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $statusTemplateInspection = $this->statusTemplateInspectionRepository->findWithoutFail($id);

        if (empty($statusTemplateInspection)) {
            Flash::error('Status Template Inspection not found');

            return redirect(route('statusTemplateInspections.index'));
        }

        $this->statusTemplateInspectionRepository->delete($id);

        Flash::success('Status Template Inspection deleted successfully.');

        return redirect(route('statusTemplateInspections.index'));
    }
}
