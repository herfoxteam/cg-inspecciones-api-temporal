<?php

namespace App\Http\Controllers;

use App\DataTables\OptionFormItemDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOptionFormItemRequest;
use App\Http\Requests\UpdateOptionFormItemRequest;
use App\Repositories\OptionFormItemRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OptionFormItemController extends AppBaseController
{
    /** @var  OptionFormItemRepository */
    private $optionFormItemRepository;

    public function __construct(OptionFormItemRepository $optionFormItemRepo)
    {
        $this->optionFormItemRepository = $optionFormItemRepo;
    }

    /**
     * Display a listing of the OptionFormItem.
     *
     * @param OptionFormItemDataTable $optionFormItemDataTable
     * @return Response
     */
    public function index(OptionFormItemDataTable $optionFormItemDataTable)
    {
        return $optionFormItemDataTable->render('option_form_items.index');
    }

    /**
     * Show the form for creating a new OptionFormItem.
     *
     * @return Response
     */
    public function create()
    {
        return view('option_form_items.create');
    }

    /**
     * Store a newly created OptionFormItem in storage.
     *
     * @param CreateOptionFormItemRequest $request
     *
     * @return Response
     */
    public function store(CreateOptionFormItemRequest $request)
    {
        $input = $request->all();

        $optionFormItem = $this->optionFormItemRepository->create($input);

        Flash::success('Option Form Item saved successfully.');

        return redirect(route('optionFormItems.index'));
    }

    /**
     * Display the specified OptionFormItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $optionFormItem = $this->optionFormItemRepository->findWithoutFail($id);

        if (empty($optionFormItem)) {
            Flash::error('Option Form Item not found');

            return redirect(route('optionFormItems.index'));
        }

        return view('option_form_items.show')->with('optionFormItem', $optionFormItem);
    }

    /**
     * Show the form for editing the specified OptionFormItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $optionFormItem = $this->optionFormItemRepository->findWithoutFail($id);

        if (empty($optionFormItem)) {
            Flash::error('Option Form Item not found');

            return redirect(route('optionFormItems.index'));
        }

        return view('option_form_items.edit')->with('optionFormItem', $optionFormItem);
    }

    /**
     * Update the specified OptionFormItem in storage.
     *
     * @param  int              $id
     * @param UpdateOptionFormItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOptionFormItemRequest $request)
    {
        $optionFormItem = $this->optionFormItemRepository->findWithoutFail($id);

        if (empty($optionFormItem)) {
            Flash::error('Option Form Item not found');

            return redirect(route('optionFormItems.index'));
        }

        $optionFormItem = $this->optionFormItemRepository->update($request->all(), $id);

        Flash::success('Option Form Item updated successfully.');

        return redirect(route('optionFormItems.index'));
    }

    /**
     * Remove the specified OptionFormItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $optionFormItem = $this->optionFormItemRepository->findWithoutFail($id);

        if (empty($optionFormItem)) {
            Flash::error('Option Form Item not found');

            return redirect(route('optionFormItems.index'));
        }

        $this->optionFormItemRepository->delete($id);

        Flash::success('Option Form Item deleted successfully.');

        return redirect(route('optionFormItems.index'));
    }
}
