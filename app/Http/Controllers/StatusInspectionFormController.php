<?php

namespace App\Http\Controllers;

use App\DataTables\StatusInspectionFormDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStatusInspectionFormRequest;
use App\Http\Requests\UpdateStatusInspectionFormRequest;
use App\Repositories\StatusInspectionFormRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class StatusInspectionFormController extends AppBaseController
{
    /** @var  StatusInspectionFormRepository */
    private $statusInspectionFormRepository;

    public function __construct(StatusInspectionFormRepository $statusInspectionFormRepo)
    {
        $this->statusInspectionFormRepository = $statusInspectionFormRepo;
    }

    /**
     * Display a listing of the StatusInspectionForm.
     *
     * @param StatusInspectionFormDataTable $statusInspectionFormDataTable
     * @return Response
     */
    public function index(StatusInspectionFormDataTable $statusInspectionFormDataTable)
    {
        return $statusInspectionFormDataTable->render('status_inspection_forms.index');
    }

    /**
     * Show the form for creating a new StatusInspectionForm.
     *
     * @return Response
     */
    public function create()
    {
        return view('status_inspection_forms.create');
    }

    /**
     * Store a newly created StatusInspectionForm in storage.
     *
     * @param CreateStatusInspectionFormRequest $request
     *
     * @return Response
     */
    public function store(CreateStatusInspectionFormRequest $request)
    {
        $input = $request->all();

        $statusInspectionForm = $this->statusInspectionFormRepository->create($input);

        Flash::success('Status Inspection Form saved successfully.');

        return redirect(route('statusInspectionForms.index'));
    }

    /**
     * Display the specified StatusInspectionForm.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $statusInspectionForm = $this->statusInspectionFormRepository->findWithoutFail($id);

        if (empty($statusInspectionForm)) {
            Flash::error('Status Inspection Form not found');

            return redirect(route('statusInspectionForms.index'));
        }

        return view('status_inspection_forms.show')->with('statusInspectionForm', $statusInspectionForm);
    }

    /**
     * Show the form for editing the specified StatusInspectionForm.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $statusInspectionForm = $this->statusInspectionFormRepository->findWithoutFail($id);

        if (empty($statusInspectionForm)) {
            Flash::error('Status Inspection Form not found');

            return redirect(route('statusInspectionForms.index'));
        }

        return view('status_inspection_forms.edit')->with('statusInspectionForm', $statusInspectionForm);
    }

    /**
     * Update the specified StatusInspectionForm in storage.
     *
     * @param  int              $id
     * @param UpdateStatusInspectionFormRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStatusInspectionFormRequest $request)
    {
        $statusInspectionForm = $this->statusInspectionFormRepository->findWithoutFail($id);

        if (empty($statusInspectionForm)) {
            Flash::error('Status Inspection Form not found');

            return redirect(route('statusInspectionForms.index'));
        }

        $statusInspectionForm = $this->statusInspectionFormRepository->update($request->all(), $id);

        Flash::success('Status Inspection Form updated successfully.');

        return redirect(route('statusInspectionForms.index'));
    }

    /**
     * Remove the specified StatusInspectionForm from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $statusInspectionForm = $this->statusInspectionFormRepository->findWithoutFail($id);

        if (empty($statusInspectionForm)) {
            Flash::error('Status Inspection Form not found');

            return redirect(route('statusInspectionForms.index'));
        }

        $this->statusInspectionFormRepository->delete($id);

        Flash::success('Status Inspection Form deleted successfully.');

        return redirect(route('statusInspectionForms.index'));
    }
}
