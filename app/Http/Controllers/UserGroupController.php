<?php

namespace App\Http\Controllers;

use App\DataTables\UserGroupDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateUserGroupRequest;
use App\Http\Requests\UpdateUserGroupRequest;
use App\Repositories\UserGroupRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class UserGroupController extends AppBaseController
{
    /** @var  UserGroupRepository */
    private $userGroupRepository;

    public function __construct(UserGroupRepository $userGroupRepo)
    {
        $this->userGroupRepository = $userGroupRepo;
    }

    /**
     * Display a listing of the UserGroup.
     *
     * @param UserGroupDataTable $userGroupDataTable
     * @return Response
     */
    public function index(UserGroupDataTable $userGroupDataTable)
    {
        return $userGroupDataTable->render('user_groups.index');
    }

    /**
     * Show the form for creating a new UserGroup.
     *
     * @return Response
     */
    public function create()
    {
        return view('user_groups.create');
    }

    /**
     * Store a newly created UserGroup in storage.
     *
     * @param CreateUserGroupRequest $request
     *
     * @return Response
     */
    public function store(CreateUserGroupRequest $request)
    {
        $input = $request->all();

        $userGroup = $this->userGroupRepository->create($input);

        Flash::success('User Group saved successfully.');

        return redirect(route('userGroups.index'));
    }

    /**
     * Display the specified UserGroup.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userGroup = $this->userGroupRepository->findWithoutFail($id);

        if (empty($userGroup)) {
            Flash::error('User Group not found');

            return redirect(route('userGroups.index'));
        }

        return view('user_groups.show')->with('userGroup', $userGroup);
    }

    /**
     * Show the form for editing the specified UserGroup.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userGroup = $this->userGroupRepository->findWithoutFail($id);

        if (empty($userGroup)) {
            Flash::error('User Group not found');

            return redirect(route('userGroups.index'));
        }

        return view('user_groups.edit')->with('userGroup', $userGroup);
    }

    /**
     * Update the specified UserGroup in storage.
     *
     * @param  int              $id
     * @param UpdateUserGroupRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserGroupRequest $request)
    {
        $userGroup = $this->userGroupRepository->findWithoutFail($id);

        if (empty($userGroup)) {
            Flash::error('User Group not found');

            return redirect(route('userGroups.index'));
        }

        $userGroup = $this->userGroupRepository->update($request->all(), $id);

        Flash::success('User Group updated successfully.');

        return redirect(route('userGroups.index'));
    }

    /**
     * Remove the specified UserGroup from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userGroup = $this->userGroupRepository->findWithoutFail($id);

        if (empty($userGroup)) {
            Flash::error('User Group not found');

            return redirect(route('userGroups.index'));
        }

        $this->userGroupRepository->delete($id);

        Flash::success('User Group deleted successfully.');

        return redirect(route('userGroups.index'));
    }
}
