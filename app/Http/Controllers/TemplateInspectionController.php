<?php

namespace App\Http\Controllers;

use App\DataTables\TemplateInspectionDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTemplateInspectionRequest;
use App\Http\Requests\UpdateTemplateInspectionRequest;
use App\Repositories\TemplateInspectionRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TemplateInspectionController extends AppBaseController
{
    /** @var  TemplateInspectionRepository */
    private $templateInspectionRepository;

    public function __construct(TemplateInspectionRepository $templateInspectionRepo)
    {
        $this->templateInspectionRepository = $templateInspectionRepo;
    }

    /**
     * Display a listing of the TemplateInspection.
     *
     * @param TemplateInspectionDataTable $templateInspectionDataTable
     * @return Response
     */
    public function index(TemplateInspectionDataTable $templateInspectionDataTable)
    {
        return $templateInspectionDataTable->render('template_inspections.index');
    }

    /**
     * Show the form for creating a new TemplateInspection.
     *
     * @return Response
     */
    public function create()
    {
        return view('template_inspections.create');
    }

    /**
     * Store a newly created TemplateInspection in storage.
     *
     * @param CreateTemplateInspectionRequest $request
     *
     * @return Response
     */
    public function store(CreateTemplateInspectionRequest $request)
    {
        $input = $request->all();

        $templateInspection = $this->templateInspectionRepository->create($input);

        Flash::success('Template Inspection saved successfully.');

        return redirect(route('templateInspections.index'));
    }

    /**
     * Display the specified TemplateInspection.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $templateInspection = $this->templateInspectionRepository->findWithoutFail($id);

        if (empty($templateInspection)) {
            Flash::error('Template Inspection not found');

            return redirect(route('templateInspections.index'));
        }

        return view('template_inspections.show')->with('templateInspection', $templateInspection);
    }

    /**
     * Show the form for editing the specified TemplateInspection.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $templateInspection = $this->templateInspectionRepository->findWithoutFail($id);

        if (empty($templateInspection)) {
            Flash::error('Template Inspection not found');

            return redirect(route('templateInspections.index'));
        }

        return view('template_inspections.edit')->with('templateInspection', $templateInspection);
    }

    /**
     * Update the specified TemplateInspection in storage.
     *
     * @param  int              $id
     * @param UpdateTemplateInspectionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTemplateInspectionRequest $request)
    {
        $templateInspection = $this->templateInspectionRepository->findWithoutFail($id);

        if (empty($templateInspection)) {
            Flash::error('Template Inspection not found');

            return redirect(route('templateInspections.index'));
        }

        $templateInspection = $this->templateInspectionRepository->update($request->all(), $id);

        Flash::success('Template Inspection updated successfully.');

        return redirect(route('templateInspections.index'));
    }

    /**
     * Remove the specified TemplateInspection from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $templateInspection = $this->templateInspectionRepository->findWithoutFail($id);

        if (empty($templateInspection)) {
            Flash::error('Template Inspection not found');

            return redirect(route('templateInspections.index'));
        }

        $this->templateInspectionRepository->delete($id);

        Flash::success('Template Inspection deleted successfully.');

        return redirect(route('templateInspections.index'));
    }
}
