<?php

namespace App\Http\Controllers;

use App\DataTables\StatusCategoryFormDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStatusCategoryFormRequest;
use App\Http\Requests\UpdateStatusCategoryFormRequest;
use App\Repositories\StatusCategoryFormRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class StatusCategoryFormController extends AppBaseController
{
    /** @var  StatusCategoryFormRepository */
    private $statusCategoryFormRepository;

    public function __construct(StatusCategoryFormRepository $statusCategoryFormRepo)
    {
        $this->statusCategoryFormRepository = $statusCategoryFormRepo;
    }

    /**
     * Display a listing of the StatusCategoryForm.
     *
     * @param StatusCategoryFormDataTable $statusCategoryFormDataTable
     * @return Response
     */
    public function index(StatusCategoryFormDataTable $statusCategoryFormDataTable)
    {
        return $statusCategoryFormDataTable->render('status_category_forms.index');
    }

    /**
     * Show the form for creating a new StatusCategoryForm.
     *
     * @return Response
     */
    public function create()
    {
        return view('status_category_forms.create');
    }

    /**
     * Store a newly created StatusCategoryForm in storage.
     *
     * @param CreateStatusCategoryFormRequest $request
     *
     * @return Response
     */
    public function store(CreateStatusCategoryFormRequest $request)
    {
        $input = $request->all();

        $statusCategoryForm = $this->statusCategoryFormRepository->create($input);

        Flash::success('Status Category Form saved successfully.');

        return redirect(route('statusCategoryForms.index'));
    }

    /**
     * Display the specified StatusCategoryForm.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $statusCategoryForm = $this->statusCategoryFormRepository->findWithoutFail($id);

        if (empty($statusCategoryForm)) {
            Flash::error('Status Category Form not found');

            return redirect(route('statusCategoryForms.index'));
        }

        return view('status_category_forms.show')->with('statusCategoryForm', $statusCategoryForm);
    }

    /**
     * Show the form for editing the specified StatusCategoryForm.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $statusCategoryForm = $this->statusCategoryFormRepository->findWithoutFail($id);

        if (empty($statusCategoryForm)) {
            Flash::error('Status Category Form not found');

            return redirect(route('statusCategoryForms.index'));
        }

        return view('status_category_forms.edit')->with('statusCategoryForm', $statusCategoryForm);
    }

    /**
     * Update the specified StatusCategoryForm in storage.
     *
     * @param  int              $id
     * @param UpdateStatusCategoryFormRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStatusCategoryFormRequest $request)
    {
        $statusCategoryForm = $this->statusCategoryFormRepository->findWithoutFail($id);

        if (empty($statusCategoryForm)) {
            Flash::error('Status Category Form not found');

            return redirect(route('statusCategoryForms.index'));
        }

        $statusCategoryForm = $this->statusCategoryFormRepository->update($request->all(), $id);

        Flash::success('Status Category Form updated successfully.');

        return redirect(route('statusCategoryForms.index'));
    }

    /**
     * Remove the specified StatusCategoryForm from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $statusCategoryForm = $this->statusCategoryFormRepository->findWithoutFail($id);

        if (empty($statusCategoryForm)) {
            Flash::error('Status Category Form not found');

            return redirect(route('statusCategoryForms.index'));
        }

        $this->statusCategoryFormRepository->delete($id);

        Flash::success('Status Category Form deleted successfully.');

        return redirect(route('statusCategoryForms.index'));
    }
}
