<?php

namespace App\Http\Controllers;

use App\DataTables\CategoryInspectionFormDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCategoryInspectionFormRequest;
use App\Http\Requests\UpdateCategoryInspectionFormRequest;
use App\Repositories\CategoryInspectionFormRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CategoryInspectionFormController extends AppBaseController
{
    /** @var  CategoryInspectionFormRepository */
    private $categoryInspectionFormRepository;

    public function __construct(CategoryInspectionFormRepository $categoryInspectionFormRepo)
    {
        $this->categoryInspectionFormRepository = $categoryInspectionFormRepo;
    }

    /**
     * Display a listing of the CategoryInspectionForm.
     *
     * @param CategoryInspectionFormDataTable $categoryInspectionFormDataTable
     * @return Response
     */
    public function index(CategoryInspectionFormDataTable $categoryInspectionFormDataTable)
    {
        return $categoryInspectionFormDataTable->render('category_inspection_forms.index');
    }

    /**
     * Show the form for creating a new CategoryInspectionForm.
     *
     * @return Response
     */
    public function create()
    {
        return view('category_inspection_forms.create');
    }

    /**
     * Store a newly created CategoryInspectionForm in storage.
     *
     * @param CreateCategoryInspectionFormRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryInspectionFormRequest $request)
    {
        $input = $request->all();

        $categoryInspectionForm = $this->categoryInspectionFormRepository->create($input);

        Flash::success('Category Inspection Form saved successfully.');

        return redirect(route('categoryInspectionForms.index'));
    }

    /**
     * Display the specified CategoryInspectionForm.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $categoryInspectionForm = $this->categoryInspectionFormRepository->findWithoutFail($id);

        if (empty($categoryInspectionForm)) {
            Flash::error('Category Inspection Form not found');

            return redirect(route('categoryInspectionForms.index'));
        }

        return view('category_inspection_forms.show')->with('categoryInspectionForm', $categoryInspectionForm);
    }

    /**
     * Show the form for editing the specified CategoryInspectionForm.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $categoryInspectionForm = $this->categoryInspectionFormRepository->findWithoutFail($id);

        if (empty($categoryInspectionForm)) {
            Flash::error('Category Inspection Form not found');

            return redirect(route('categoryInspectionForms.index'));
        }

        return view('category_inspection_forms.edit')->with('categoryInspectionForm', $categoryInspectionForm);
    }

    /**
     * Update the specified CategoryInspectionForm in storage.
     *
     * @param  int              $id
     * @param UpdateCategoryInspectionFormRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryInspectionFormRequest $request)
    {
        $categoryInspectionForm = $this->categoryInspectionFormRepository->findWithoutFail($id);

        if (empty($categoryInspectionForm)) {
            Flash::error('Category Inspection Form not found');

            return redirect(route('categoryInspectionForms.index'));
        }

        $categoryInspectionForm = $this->categoryInspectionFormRepository->update($request->all(), $id);

        Flash::success('Category Inspection Form updated successfully.');

        return redirect(route('categoryInspectionForms.index'));
    }

    /**
     * Remove the specified CategoryInspectionForm from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $categoryInspectionForm = $this->categoryInspectionFormRepository->findWithoutFail($id);

        if (empty($categoryInspectionForm)) {
            Flash::error('Category Inspection Form not found');

            return redirect(route('categoryInspectionForms.index'));
        }

        $this->categoryInspectionFormRepository->delete($id);

        Flash::success('Category Inspection Form deleted successfully.');

        return redirect(route('categoryInspectionForms.index'));
    }
}
