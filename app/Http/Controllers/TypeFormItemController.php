<?php

namespace App\Http\Controllers;

use App\DataTables\TypeFormItemDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTypeFormItemRequest;
use App\Http\Requests\UpdateTypeFormItemRequest;
use App\Repositories\TypeFormItemRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TypeFormItemController extends AppBaseController
{
    /** @var  TypeFormItemRepository */
    private $typeFormItemRepository;

    public function __construct(TypeFormItemRepository $typeFormItemRepo)
    {
        $this->typeFormItemRepository = $typeFormItemRepo;
    }

    /**
     * Display a listing of the TypeFormItem.
     *
     * @param TypeFormItemDataTable $typeFormItemDataTable
     * @return Response
     */
    public function index(TypeFormItemDataTable $typeFormItemDataTable)
    {
        return $typeFormItemDataTable->render('type_form_items.index');
    }

    /**
     * Show the form for creating a new TypeFormItem.
     *
     * @return Response
     */
    public function create()
    {
        return view('type_form_items.create');
    }

    /**
     * Store a newly created TypeFormItem in storage.
     *
     * @param CreateTypeFormItemRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeFormItemRequest $request)
    {
        $input = $request->all();

        $typeFormItem = $this->typeFormItemRepository->create($input);

        Flash::success('Type Form Item saved successfully.');

        return redirect(route('typeFormItems.index'));
    }

    /**
     * Display the specified TypeFormItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $typeFormItem = $this->typeFormItemRepository->findWithoutFail($id);

        if (empty($typeFormItem)) {
            Flash::error('Type Form Item not found');

            return redirect(route('typeFormItems.index'));
        }

        return view('type_form_items.show')->with('typeFormItem', $typeFormItem);
    }

    /**
     * Show the form for editing the specified TypeFormItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $typeFormItem = $this->typeFormItemRepository->findWithoutFail($id);

        if (empty($typeFormItem)) {
            Flash::error('Type Form Item not found');

            return redirect(route('typeFormItems.index'));
        }

        return view('type_form_items.edit')->with('typeFormItem', $typeFormItem);
    }

    /**
     * Update the specified TypeFormItem in storage.
     *
     * @param  int              $id
     * @param UpdateTypeFormItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeFormItemRequest $request)
    {
        $typeFormItem = $this->typeFormItemRepository->findWithoutFail($id);

        if (empty($typeFormItem)) {
            Flash::error('Type Form Item not found');

            return redirect(route('typeFormItems.index'));
        }

        $typeFormItem = $this->typeFormItemRepository->update($request->all(), $id);

        Flash::success('Type Form Item updated successfully.');

        return redirect(route('typeFormItems.index'));
    }

    /**
     * Remove the specified TypeFormItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $typeFormItem = $this->typeFormItemRepository->findWithoutFail($id);

        if (empty($typeFormItem)) {
            Flash::error('Type Form Item not found');

            return redirect(route('typeFormItems.index'));
        }

        $this->typeFormItemRepository->delete($id);

        Flash::success('Type Form Item deleted successfully.');

        return redirect(route('typeFormItems.index'));
    }
}
