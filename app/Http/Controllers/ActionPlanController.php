<?php

namespace App\Http\Controllers;

use App\DataTables\ActionPlanDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateActionPlanRequest;
use App\Http\Requests\UpdateActionPlanRequest;
use App\Repositories\ActionPlanRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ActionPlanController extends AppBaseController
{
    /** @var  ActionPlanRepository */
    private $actionPlanRepository;

    public function __construct(ActionPlanRepository $actionPlanRepo)
    {
        $this->actionPlanRepository = $actionPlanRepo;
    }

    /**
     * Display a listing of the ActionPlan.
     *
     * @param ActionPlanDataTable $actionPlanDataTable
     * @return Response
     */
    public function index(ActionPlanDataTable $actionPlanDataTable)
    {
        return $actionPlanDataTable->render('action_plans.index');
    }

    /**
     * Show the form for creating a new ActionPlan.
     *
     * @return Response
     */
    public function create()
    {
        return view('action_plans.create');
    }

    /**
     * Store a newly created ActionPlan in storage.
     *
     * @param CreateActionPlanRequest $request
     *
     * @return Response
     */
    public function store(CreateActionPlanRequest $request)
    {
        $input = $request->all();

        $actionPlan = $this->actionPlanRepository->create($input);

        Flash::success('Action Plan saved successfully.');

        return redirect(route('actionPlans.index'));
    }

    /**
     * Display the specified ActionPlan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $actionPlan = $this->actionPlanRepository->findWithoutFail($id);

        if (empty($actionPlan)) {
            Flash::error('Action Plan not found');

            return redirect(route('actionPlans.index'));
        }

        return view('action_plans.show')->with('actionPlan', $actionPlan);
    }

    /**
     * Show the form for editing the specified ActionPlan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $actionPlan = $this->actionPlanRepository->findWithoutFail($id);

        if (empty($actionPlan)) {
            Flash::error('Action Plan not found');

            return redirect(route('actionPlans.index'));
        }

        return view('action_plans.edit')->with('actionPlan', $actionPlan);
    }

    /**
     * Update the specified ActionPlan in storage.
     *
     * @param  int              $id
     * @param UpdateActionPlanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActionPlanRequest $request)
    {
        $actionPlan = $this->actionPlanRepository->findWithoutFail($id);

        if (empty($actionPlan)) {
            Flash::error('Action Plan not found');

            return redirect(route('actionPlans.index'));
        }

        $actionPlan = $this->actionPlanRepository->update($request->all(), $id);

        Flash::success('Action Plan updated successfully.');

        return redirect(route('actionPlans.index'));
    }

    /**
     * Remove the specified ActionPlan from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $actionPlan = $this->actionPlanRepository->findWithoutFail($id);

        if (empty($actionPlan)) {
            Flash::error('Action Plan not found');

            return redirect(route('actionPlans.index'));
        }

        $this->actionPlanRepository->delete($id);

        Flash::success('Action Plan deleted successfully.');

        return redirect(route('actionPlans.index'));
    }
}
