<?php

namespace App\Http\Controllers;

use App\DataTables\CustomDateScheduledDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCustomDateScheduledRequest;
use App\Http\Requests\UpdateCustomDateScheduledRequest;
use App\Repositories\CustomDateScheduledRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CustomDateScheduledController extends AppBaseController
{
    /** @var  CustomDateScheduledRepository */
    private $customDateScheduledRepository;

    public function __construct(CustomDateScheduledRepository $customDateScheduledRepo)
    {
        $this->customDateScheduledRepository = $customDateScheduledRepo;
    }

    /**
     * Display a listing of the CustomDateScheduled.
     *
     * @param CustomDateScheduledDataTable $customDateScheduledDataTable
     * @return Response
     */
    public function index(CustomDateScheduledDataTable $customDateScheduledDataTable)
    {
        return $customDateScheduledDataTable->render('custom_date_scheduleds.index');
    }

    /**
     * Show the form for creating a new CustomDateScheduled.
     *
     * @return Response
     */
    public function create()
    {
        return view('custom_date_scheduleds.create');
    }

    /**
     * Store a newly created CustomDateScheduled in storage.
     *
     * @param CreateCustomDateScheduledRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomDateScheduledRequest $request)
    {
        $input = $request->all();

        $customDateScheduled = $this->customDateScheduledRepository->create($input);

        Flash::success('Custom Date Scheduled saved successfully.');

        return redirect(route('customDateScheduleds.index'));
    }

    /**
     * Display the specified CustomDateScheduled.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customDateScheduled = $this->customDateScheduledRepository->findWithoutFail($id);

        if (empty($customDateScheduled)) {
            Flash::error('Custom Date Scheduled not found');

            return redirect(route('customDateScheduleds.index'));
        }

        return view('custom_date_scheduleds.show')->with('customDateScheduled', $customDateScheduled);
    }

    /**
     * Show the form for editing the specified CustomDateScheduled.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customDateScheduled = $this->customDateScheduledRepository->findWithoutFail($id);

        if (empty($customDateScheduled)) {
            Flash::error('Custom Date Scheduled not found');

            return redirect(route('customDateScheduleds.index'));
        }

        return view('custom_date_scheduleds.edit')->with('customDateScheduled', $customDateScheduled);
    }

    /**
     * Update the specified CustomDateScheduled in storage.
     *
     * @param  int              $id
     * @param UpdateCustomDateScheduledRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomDateScheduledRequest $request)
    {
        $customDateScheduled = $this->customDateScheduledRepository->findWithoutFail($id);

        if (empty($customDateScheduled)) {
            Flash::error('Custom Date Scheduled not found');

            return redirect(route('customDateScheduleds.index'));
        }

        $customDateScheduled = $this->customDateScheduledRepository->update($request->all(), $id);

        Flash::success('Custom Date Scheduled updated successfully.');

        return redirect(route('customDateScheduleds.index'));
    }

    /**
     * Remove the specified CustomDateScheduled from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customDateScheduled = $this->customDateScheduledRepository->findWithoutFail($id);

        if (empty($customDateScheduled)) {
            Flash::error('Custom Date Scheduled not found');

            return redirect(route('customDateScheduleds.index'));
        }

        $this->customDateScheduledRepository->delete($id);

        Flash::success('Custom Date Scheduled deleted successfully.');

        return redirect(route('customDateScheduleds.index'));
    }
}
