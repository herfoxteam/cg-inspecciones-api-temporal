<?php

namespace App\Http\Controllers;

use App\DataTables\ObservationFormItemDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateObservationFormItemRequest;
use App\Http\Requests\UpdateObservationFormItemRequest;
use App\Repositories\ObservationFormItemRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ObservationFormItemController extends AppBaseController
{
    /** @var  ObservationFormItemRepository */
    private $observationFormItemRepository;

    public function __construct(ObservationFormItemRepository $observationFormItemRepo)
    {
        $this->observationFormItemRepository = $observationFormItemRepo;
    }

    /**
     * Display a listing of the ObservationFormItem.
     *
     * @param ObservationFormItemDataTable $observationFormItemDataTable
     * @return Response
     */
    public function index(ObservationFormItemDataTable $observationFormItemDataTable)
    {
        return $observationFormItemDataTable->render('observation_form_items.index');
    }

    /**
     * Show the form for creating a new ObservationFormItem.
     *
     * @return Response
     */
    public function create()
    {
        return view('observation_form_items.create');
    }

    /**
     * Store a newly created ObservationFormItem in storage.
     *
     * @param CreateObservationFormItemRequest $request
     *
     * @return Response
     */
    public function store(CreateObservationFormItemRequest $request)
    {
        $input = $request->all();

        $observationFormItem = $this->observationFormItemRepository->create($input);

        Flash::success('Observation Form Item saved successfully.');

        return redirect(route('observationFormItems.index'));
    }

    /**
     * Display the specified ObservationFormItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $observationFormItem = $this->observationFormItemRepository->findWithoutFail($id);

        if (empty($observationFormItem)) {
            Flash::error('Observation Form Item not found');

            return redirect(route('observationFormItems.index'));
        }

        return view('observation_form_items.show')->with('observationFormItem', $observationFormItem);
    }

    /**
     * Show the form for editing the specified ObservationFormItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $observationFormItem = $this->observationFormItemRepository->findWithoutFail($id);

        if (empty($observationFormItem)) {
            Flash::error('Observation Form Item not found');

            return redirect(route('observationFormItems.index'));
        }

        return view('observation_form_items.edit')->with('observationFormItem', $observationFormItem);
    }

    /**
     * Update the specified ObservationFormItem in storage.
     *
     * @param  int              $id
     * @param UpdateObservationFormItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObservationFormItemRequest $request)
    {
        $observationFormItem = $this->observationFormItemRepository->findWithoutFail($id);

        if (empty($observationFormItem)) {
            Flash::error('Observation Form Item not found');

            return redirect(route('observationFormItems.index'));
        }

        $observationFormItem = $this->observationFormItemRepository->update($request->all(), $id);

        Flash::success('Observation Form Item updated successfully.');

        return redirect(route('observationFormItems.index'));
    }

    /**
     * Remove the specified ObservationFormItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $observationFormItem = $this->observationFormItemRepository->findWithoutFail($id);

        if (empty($observationFormItem)) {
            Flash::error('Observation Form Item not found');

            return redirect(route('observationFormItems.index'));
        }

        $this->observationFormItemRepository->delete($id);

        Flash::success('Observation Form Item deleted successfully.');

        return redirect(route('observationFormItems.index'));
    }
}
