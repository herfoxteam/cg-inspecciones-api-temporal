<?php

namespace App\Http\Controllers;

use App\DataTables\InspectionFormItemDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateInspectionFormItemRequest;
use App\Http\Requests\UpdateInspectionFormItemRequest;
use App\Repositories\InspectionFormItemRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class InspectionFormItemController extends AppBaseController
{
    /** @var  InspectionFormItemRepository */
    private $inspectionFormItemRepository;

    public function __construct(InspectionFormItemRepository $inspectionFormItemRepo)
    {
        $this->inspectionFormItemRepository = $inspectionFormItemRepo;
    }

    /**
     * Display a listing of the InspectionFormItem.
     *
     * @param InspectionFormItemDataTable $inspectionFormItemDataTable
     * @return Response
     */
    public function index(InspectionFormItemDataTable $inspectionFormItemDataTable)
    {
        return $inspectionFormItemDataTable->render('inspection_form_items.index');
    }

    /**
     * Show the form for creating a new InspectionFormItem.
     *
     * @return Response
     */
    public function create()
    {
        return view('inspection_form_items.create');
    }

    /**
     * Store a newly created InspectionFormItem in storage.
     *
     * @param CreateInspectionFormItemRequest $request
     *
     * @return Response
     */
    public function store(CreateInspectionFormItemRequest $request)
    {
        $input = $request->all();

        $inspectionFormItem = $this->inspectionFormItemRepository->create($input);

        Flash::success('Inspection Form Item saved successfully.');

        return redirect(route('inspectionFormItems.index'));
    }

    /**
     * Display the specified InspectionFormItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $inspectionFormItem = $this->inspectionFormItemRepository->findWithoutFail($id);

        if (empty($inspectionFormItem)) {
            Flash::error('Inspection Form Item not found');

            return redirect(route('inspectionFormItems.index'));
        }

        return view('inspection_form_items.show')->with('inspectionFormItem', $inspectionFormItem);
    }

    /**
     * Show the form for editing the specified InspectionFormItem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $inspectionFormItem = $this->inspectionFormItemRepository->findWithoutFail($id);

        if (empty($inspectionFormItem)) {
            Flash::error('Inspection Form Item not found');

            return redirect(route('inspectionFormItems.index'));
        }

        return view('inspection_form_items.edit')->with('inspectionFormItem', $inspectionFormItem);
    }

    /**
     * Update the specified InspectionFormItem in storage.
     *
     * @param  int              $id
     * @param UpdateInspectionFormItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInspectionFormItemRequest $request)
    {
        $inspectionFormItem = $this->inspectionFormItemRepository->findWithoutFail($id);

        if (empty($inspectionFormItem)) {
            Flash::error('Inspection Form Item not found');

            return redirect(route('inspectionFormItems.index'));
        }

        $inspectionFormItem = $this->inspectionFormItemRepository->update($request->all(), $id);

        Flash::success('Inspection Form Item updated successfully.');

        return redirect(route('inspectionFormItems.index'));
    }

    /**
     * Remove the specified InspectionFormItem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $inspectionFormItem = $this->inspectionFormItemRepository->findWithoutFail($id);

        if (empty($inspectionFormItem)) {
            Flash::error('Inspection Form Item not found');

            return redirect(route('inspectionFormItems.index'));
        }

        $this->inspectionFormItemRepository->delete($id);

        Flash::success('Inspection Form Item deleted successfully.');

        return redirect(route('inspectionFormItems.index'));
    }
}
