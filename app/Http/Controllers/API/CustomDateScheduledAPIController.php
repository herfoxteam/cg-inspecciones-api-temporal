<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomDateScheduledAPIRequest;
use App\Http\Requests\API\UpdateCustomDateScheduledAPIRequest;
use App\Models\CustomDateScheduled;
use App\Repositories\CustomDateScheduledRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CustomDateScheduledController
 * @package App\Http\Controllers\API
 */

class CustomDateScheduledAPIController extends AppBaseController
{
    /** @var  CustomDateScheduledRepository */
    private $customDateScheduledRepository;

    public function __construct(CustomDateScheduledRepository $customDateScheduledRepo)
    {
        $this->customDateScheduledRepository = $customDateScheduledRepo;
    }

    /**
     * Display a listing of the CustomDateScheduled.
     * GET|HEAD /customDateScheduleds
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->customDateScheduledRepository->pushCriteria(new RequestCriteria($request));
        $this->customDateScheduledRepository->pushCriteria(new LimitOffsetCriteria($request));
        $customDateScheduleds = $this->customDateScheduledRepository->all();

        return $this->sendResponse($customDateScheduleds->toArray(), 'Custom Date Scheduleds retrieved successfully');
    }

    /**
     * Store a newly created CustomDateScheduled in storage.
     * POST /customDateScheduleds
     *
     * @param CreateCustomDateScheduledAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomDateScheduledAPIRequest $request)
    {
        $input = $request->all();

        $customDateScheduleds = $this->customDateScheduledRepository->create($input);

        return $this->sendResponse($customDateScheduleds->toArray(), 'Custom Date Scheduled saved successfully');
    }

    /**
     * Display the specified CustomDateScheduled.
     * GET|HEAD /customDateScheduleds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CustomDateScheduled $customDateScheduled */
        $customDateScheduled = $this->customDateScheduledRepository->findWithoutFail($id);

        if (empty($customDateScheduled)) {
            return $this->sendError('Custom Date Scheduled not found');
        }

        return $this->sendResponse($customDateScheduled->toArray(), 'Custom Date Scheduled retrieved successfully');
    }

    /**
     * Update the specified CustomDateScheduled in storage.
     * PUT/PATCH /customDateScheduleds/{id}
     *
     * @param  int $id
     * @param UpdateCustomDateScheduledAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomDateScheduledAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomDateScheduled $customDateScheduled */
        $customDateScheduled = $this->customDateScheduledRepository->findWithoutFail($id);

        if (empty($customDateScheduled)) {
            return $this->sendError('Custom Date Scheduled not found');
        }

        $customDateScheduled = $this->customDateScheduledRepository->update($input, $id);

        return $this->sendResponse($customDateScheduled->toArray(), 'CustomDateScheduled updated successfully');
    }

    /**
     * Remove the specified CustomDateScheduled from storage.
     * DELETE /customDateScheduleds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CustomDateScheduled $customDateScheduled */
        $customDateScheduled = $this->customDateScheduledRepository->findWithoutFail($id);

        if (empty($customDateScheduled)) {
            return $this->sendError('Custom Date Scheduled not found');
        }

        $customDateScheduled->delete();

        return $this->sendResponse($id, 'Custom Date Scheduled deleted successfully');
    }
}
