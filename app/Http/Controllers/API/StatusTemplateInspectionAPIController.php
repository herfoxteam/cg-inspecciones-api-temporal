<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStatusTemplateInspectionAPIRequest;
use App\Http\Requests\API\UpdateStatusTemplateInspectionAPIRequest;
use App\Models\StatusTemplateInspection;
use App\Repositories\StatusTemplateInspectionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StatusTemplateInspectionController
 * @package App\Http\Controllers\API
 */

class StatusTemplateInspectionAPIController extends AppBaseController
{
    /** @var  StatusTemplateInspectionRepository */
    private $statusTemplateInspectionRepository;

    public function __construct(StatusTemplateInspectionRepository $statusTemplateInspectionRepo)
    {
        $this->statusTemplateInspectionRepository = $statusTemplateInspectionRepo;
    }

    /**
     * Display a listing of the StatusTemplateInspection.
     * GET|HEAD /statusTemplateInspections
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->statusTemplateInspectionRepository->pushCriteria(new RequestCriteria($request));
        $this->statusTemplateInspectionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $statusTemplateInspections = $this->statusTemplateInspectionRepository->all();

        return $this->sendResponse($statusTemplateInspections->toArray(), 'Status Template Inspections retrieved successfully');
    }

    /**
     * Store a newly created StatusTemplateInspection in storage.
     * POST /statusTemplateInspections
     *
     * @param CreateStatusTemplateInspectionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStatusTemplateInspectionAPIRequest $request)
    {
        $input = $request->all();

        $statusTemplateInspections = $this->statusTemplateInspectionRepository->create($input);

        return $this->sendResponse($statusTemplateInspections->toArray(), 'Status Template Inspection saved successfully');
    }

    /**
     * Display the specified StatusTemplateInspection.
     * GET|HEAD /statusTemplateInspections/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StatusTemplateInspection $statusTemplateInspection */
        $statusTemplateInspection = $this->statusTemplateInspectionRepository->findWithoutFail($id);

        if (empty($statusTemplateInspection)) {
            return $this->sendError('Status Template Inspection not found');
        }

        return $this->sendResponse($statusTemplateInspection->toArray(), 'Status Template Inspection retrieved successfully');
    }

    /**
     * Update the specified StatusTemplateInspection in storage.
     * PUT/PATCH /statusTemplateInspections/{id}
     *
     * @param  int $id
     * @param UpdateStatusTemplateInspectionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStatusTemplateInspectionAPIRequest $request)
    {
        $input = $request->all();

        /** @var StatusTemplateInspection $statusTemplateInspection */
        $statusTemplateInspection = $this->statusTemplateInspectionRepository->findWithoutFail($id);

        if (empty($statusTemplateInspection)) {
            return $this->sendError('Status Template Inspection not found');
        }

        $statusTemplateInspection = $this->statusTemplateInspectionRepository->update($input, $id);

        return $this->sendResponse($statusTemplateInspection->toArray(), 'StatusTemplateInspection updated successfully');
    }

    /**
     * Remove the specified StatusTemplateInspection from storage.
     * DELETE /statusTemplateInspections/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StatusTemplateInspection $statusTemplateInspection */
        $statusTemplateInspection = $this->statusTemplateInspectionRepository->findWithoutFail($id);

        if (empty($statusTemplateInspection)) {
            return $this->sendError('Status Template Inspection not found');
        }

        $statusTemplateInspection->delete();

        return $this->sendResponse($id, 'Status Template Inspection deleted successfully');
    }
}
