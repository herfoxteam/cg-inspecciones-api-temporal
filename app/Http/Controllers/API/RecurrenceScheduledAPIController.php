<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRecurrenceScheduledAPIRequest;
use App\Http\Requests\API\UpdateRecurrenceScheduledAPIRequest;
use App\Models\RecurrenceScheduled;
use App\Repositories\RecurrenceScheduledRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RecurrenceScheduledController
 * @package App\Http\Controllers\API
 */

class RecurrenceScheduledAPIController extends AppBaseController
{
    /** @var  RecurrenceScheduledRepository */
    private $recurrenceScheduledRepository;

    public function __construct(RecurrenceScheduledRepository $recurrenceScheduledRepo)
    {
        $this->recurrenceScheduledRepository = $recurrenceScheduledRepo;
    }

    /**
     * Display a listing of the RecurrenceScheduled.
     * GET|HEAD /recurrenceScheduleds
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->recurrenceScheduledRepository->pushCriteria(new RequestCriteria($request));
        $this->recurrenceScheduledRepository->pushCriteria(new LimitOffsetCriteria($request));
        $recurrenceScheduleds = $this->recurrenceScheduledRepository->all();

        return $this->sendResponse($recurrenceScheduleds->toArray(), 'Recurrence Scheduleds retrieved successfully');
    }

    /**
     * Store a newly created RecurrenceScheduled in storage.
     * POST /recurrenceScheduleds
     *
     * @param CreateRecurrenceScheduledAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRecurrenceScheduledAPIRequest $request)
    {
        $input = $request->all();

        $recurrenceScheduleds = $this->recurrenceScheduledRepository->create($input);

        return $this->sendResponse($recurrenceScheduleds->toArray(), 'Recurrence Scheduled saved successfully');
    }

    /**
     * Display the specified RecurrenceScheduled.
     * GET|HEAD /recurrenceScheduleds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RecurrenceScheduled $recurrenceScheduled */
        $recurrenceScheduled = $this->recurrenceScheduledRepository->findWithoutFail($id);

        if (empty($recurrenceScheduled)) {
            return $this->sendError('Recurrence Scheduled not found');
        }

        return $this->sendResponse($recurrenceScheduled->toArray(), 'Recurrence Scheduled retrieved successfully');
    }

    /**
     * Update the specified RecurrenceScheduled in storage.
     * PUT/PATCH /recurrenceScheduleds/{id}
     *
     * @param  int $id
     * @param UpdateRecurrenceScheduledAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRecurrenceScheduledAPIRequest $request)
    {
        $input = $request->all();

        /** @var RecurrenceScheduled $recurrenceScheduled */
        $recurrenceScheduled = $this->recurrenceScheduledRepository->findWithoutFail($id);

        if (empty($recurrenceScheduled)) {
            return $this->sendError('Recurrence Scheduled not found');
        }

        $recurrenceScheduled = $this->recurrenceScheduledRepository->update($input, $id);

        return $this->sendResponse($recurrenceScheduled->toArray(), 'RecurrenceScheduled updated successfully');
    }

    /**
     * Remove the specified RecurrenceScheduled from storage.
     * DELETE /recurrenceScheduleds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RecurrenceScheduled $recurrenceScheduled */
        $recurrenceScheduled = $this->recurrenceScheduledRepository->findWithoutFail($id);

        if (empty($recurrenceScheduled)) {
            return $this->sendError('Recurrence Scheduled not found');
        }

        $recurrenceScheduled->delete();

        return $this->sendResponse($id, 'Recurrence Scheduled deleted successfully');
    }
}
