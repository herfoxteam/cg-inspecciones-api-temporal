<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserGroupAPIRequest;
use App\Http\Requests\API\UpdateUserGroupAPIRequest;
use App\Models\UserGroup;
use App\Repositories\UserGroupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class UserGroupController
 * @package App\Http\Controllers\API
 */

class UserGroupAPIController extends AppBaseController
{
    /** @var  UserGroupRepository */
    private $userGroupRepository;

    public function __construct(UserGroupRepository $userGroupRepo)
    {
        $this->userGroupRepository = $userGroupRepo;
    }

    /**
     * Display a listing of the UserGroup.
     * GET|HEAD /userGroups
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userGroupRepository->pushCriteria(new RequestCriteria($request));
        $this->userGroupRepository->pushCriteria(new LimitOffsetCriteria($request));
        $userGroups = $this->userGroupRepository->all();

        return $this->sendResponse($userGroups->toArray(), 'User Groups retrieved successfully');
    }

    /**
     * Store a newly created UserGroup in storage.
     * POST /userGroups
     *
     * @param CreateUserGroupAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateUserGroupAPIRequest $request)
    {
        $input = $request->all();

        $userGroups = $this->userGroupRepository->create($input);

        return $this->sendResponse($userGroups->toArray(), 'User Group saved successfully');
    }

    /**
     * Display the specified UserGroup.
     * GET|HEAD /userGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var UserGroup $userGroup */
        $userGroup = $this->userGroupRepository->findWithoutFail($id);

        if (empty($userGroup)) {
            return $this->sendError('User Group not found');
        }

        return $this->sendResponse($userGroup->toArray(), 'User Group retrieved successfully');
    }

    /**
     * Update the specified UserGroup in storage.
     * PUT/PATCH /userGroups/{id}
     *
     * @param  int $id
     * @param UpdateUserGroupAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserGroupAPIRequest $request)
    {
        $input = $request->all();

        /** @var UserGroup $userGroup */
        $userGroup = $this->userGroupRepository->findWithoutFail($id);

        if (empty($userGroup)) {
            return $this->sendError('User Group not found');
        }

        $userGroup = $this->userGroupRepository->update($input, $id);

        return $this->sendResponse($userGroup->toArray(), 'UserGroup updated successfully');
    }

    /**
     * Remove the specified UserGroup from storage.
     * DELETE /userGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var UserGroup $userGroup */
        $userGroup = $this->userGroupRepository->findWithoutFail($id);

        if (empty($userGroup)) {
            return $this->sendError('User Group not found');
        }

        $userGroup->delete();

        return $this->sendResponse($id, 'User Group deleted successfully');
    }
}
