<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateActionPlanAPIRequest;
use App\Http\Requests\API\UpdateActionPlanAPIRequest;
use App\Models\ActionPlan;
use App\Repositories\ActionPlanRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ActionPlanController
 * @package App\Http\Controllers\API
 */

class ActionPlanAPIController extends AppBaseController
{
    /** @var  ActionPlanRepository */
    private $actionPlanRepository;

    public function __construct(ActionPlanRepository $actionPlanRepo)
    {
        $this->actionPlanRepository = $actionPlanRepo;
    }

    /**
     * Display a listing of the ActionPlan.
     * GET|HEAD /actionPlans
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->actionPlanRepository->pushCriteria(new RequestCriteria($request));
        $this->actionPlanRepository->pushCriteria(new LimitOffsetCriteria($request));
        $actionPlans = $this->actionPlanRepository->all();

        return $this->sendResponse($actionPlans->toArray(), 'Action Plans retrieved successfully');
    }

    /**
     * Store a newly created ActionPlan in storage.
     * POST /actionPlans
     *
     * @param CreateActionPlanAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateActionPlanAPIRequest $request)
    {
        $input = $request->all();

        $actionPlans = $this->actionPlanRepository->create($input);

        return $this->sendResponse($actionPlans->toArray(), 'Action Plan saved successfully');
    }

    /**
     * Display the specified ActionPlan.
     * GET|HEAD /actionPlans/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ActionPlan $actionPlan */
        $actionPlan = $this->actionPlanRepository->findWithoutFail($id);

        if (empty($actionPlan)) {
            return $this->sendError('Action Plan not found');
        }

        return $this->sendResponse($actionPlan->toArray(), 'Action Plan retrieved successfully');
    }

    /**
     * Update the specified ActionPlan in storage.
     * PUT/PATCH /actionPlans/{id}
     *
     * @param  int $id
     * @param UpdateActionPlanAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActionPlanAPIRequest $request)
    {
        $input = $request->all();

        /** @var ActionPlan $actionPlan */
        $actionPlan = $this->actionPlanRepository->findWithoutFail($id);

        if (empty($actionPlan)) {
            return $this->sendError('Action Plan not found');
        }

        $actionPlan = $this->actionPlanRepository->update($input, $id);

        return $this->sendResponse($actionPlan->toArray(), 'ActionPlan updated successfully');
    }

    /**
     * Remove the specified ActionPlan from storage.
     * DELETE /actionPlans/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ActionPlan $actionPlan */
        $actionPlan = $this->actionPlanRepository->findWithoutFail($id);

        if (empty($actionPlan)) {
            return $this->sendError('Action Plan not found');
        }

        $actionPlan->delete();

        return $this->sendResponse($id, 'Action Plan deleted successfully');
    }
}
