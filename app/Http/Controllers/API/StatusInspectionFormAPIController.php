<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStatusInspectionFormAPIRequest;
use App\Http\Requests\API\UpdateStatusInspectionFormAPIRequest;
use App\Models\StatusInspectionForm;
use App\Repositories\StatusInspectionFormRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StatusInspectionFormController
 * @package App\Http\Controllers\API
 */

class StatusInspectionFormAPIController extends AppBaseController
{
    /** @var  StatusInspectionFormRepository */
    private $statusInspectionFormRepository;

    public function __construct(StatusInspectionFormRepository $statusInspectionFormRepo)
    {
        $this->statusInspectionFormRepository = $statusInspectionFormRepo;
    }

    /**
     * Display a listing of the StatusInspectionForm.
     * GET|HEAD /statusInspectionForms
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->statusInspectionFormRepository->pushCriteria(new RequestCriteria($request));
        $this->statusInspectionFormRepository->pushCriteria(new LimitOffsetCriteria($request));
        $statusInspectionForms = $this->statusInspectionFormRepository->all();

        return $this->sendResponse($statusInspectionForms->toArray(), 'Status Inspection Forms retrieved successfully');
    }

    /**
     * Store a newly created StatusInspectionForm in storage.
     * POST /statusInspectionForms
     *
     * @param CreateStatusInspectionFormAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStatusInspectionFormAPIRequest $request)
    {
        $input = $request->all();

        $statusInspectionForms = $this->statusInspectionFormRepository->create($input);

        return $this->sendResponse($statusInspectionForms->toArray(), 'Status Inspection Form saved successfully');
    }

    /**
     * Display the specified StatusInspectionForm.
     * GET|HEAD /statusInspectionForms/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StatusInspectionForm $statusInspectionForm */
        $statusInspectionForm = $this->statusInspectionFormRepository->findWithoutFail($id);

        if (empty($statusInspectionForm)) {
            return $this->sendError('Status Inspection Form not found');
        }

        return $this->sendResponse($statusInspectionForm->toArray(), 'Status Inspection Form retrieved successfully');
    }

    /**
     * Update the specified StatusInspectionForm in storage.
     * PUT/PATCH /statusInspectionForms/{id}
     *
     * @param  int $id
     * @param UpdateStatusInspectionFormAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStatusInspectionFormAPIRequest $request)
    {
        $input = $request->all();

        /** @var StatusInspectionForm $statusInspectionForm */
        $statusInspectionForm = $this->statusInspectionFormRepository->findWithoutFail($id);

        if (empty($statusInspectionForm)) {
            return $this->sendError('Status Inspection Form not found');
        }

        $statusInspectionForm = $this->statusInspectionFormRepository->update($input, $id);

        return $this->sendResponse($statusInspectionForm->toArray(), 'StatusInspectionForm updated successfully');
    }

    /**
     * Remove the specified StatusInspectionForm from storage.
     * DELETE /statusInspectionForms/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StatusInspectionForm $statusInspectionForm */
        $statusInspectionForm = $this->statusInspectionFormRepository->findWithoutFail($id);

        if (empty($statusInspectionForm)) {
            return $this->sendError('Status Inspection Form not found');
        }

        $statusInspectionForm->delete();

        return $this->sendResponse($id, 'Status Inspection Form deleted successfully');
    }
}
