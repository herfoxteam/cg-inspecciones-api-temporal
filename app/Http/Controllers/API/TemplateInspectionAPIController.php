<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTemplateInspectionAPIRequest;
use App\Http\Requests\API\UpdateTemplateInspectionAPIRequest;
use App\Models\TemplateInspection;
use App\Repositories\TemplateInspectionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TemplateInspectionController
 * @package App\Http\Controllers\API
 */

class TemplateInspectionAPIController extends AppBaseController
{
    /** @var  TemplateInspectionRepository */
    private $templateInspectionRepository;

    public function __construct(TemplateInspectionRepository $templateInspectionRepo)
    {
        $this->templateInspectionRepository = $templateInspectionRepo;
    }

    /**
     * Display a listing of the TemplateInspection.
     * GET|HEAD /templateInspections
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->templateInspectionRepository->pushCriteria(new RequestCriteria($request));
        $this->templateInspectionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $templateInspections = $this->templateInspectionRepository->all();

        return $this->sendResponse($templateInspections->toArray(), 'Template Inspections retrieved successfully');
    }

    /**
     * Store a newly created TemplateInspection in storage.
     * POST /templateInspections
     *
     * @param CreateTemplateInspectionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTemplateInspectionAPIRequest $request)
    {
        $input = $request->all();

        $templateInspections = $this->templateInspectionRepository->create($input);

        return $this->sendResponse($templateInspections->toArray(), 'Template Inspection saved successfully');
    }

    /**
     * Display the specified TemplateInspection.
     * GET|HEAD /templateInspections/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TemplateInspection $templateInspection */
        $templateInspection = $this->templateInspectionRepository->findWithoutFail($id);

        if (empty($templateInspection)) {
            return $this->sendError('Template Inspection not found');
        }

        return $this->sendResponse($templateInspection->toArray(), 'Template Inspection retrieved successfully');
    }

    /**
     * Update the specified TemplateInspection in storage.
     * PUT/PATCH /templateInspections/{id}
     *
     * @param  int $id
     * @param UpdateTemplateInspectionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTemplateInspectionAPIRequest $request)
    {
        $input = $request->all();

        /** @var TemplateInspection $templateInspection */
        $templateInspection = $this->templateInspectionRepository->findWithoutFail($id);

        if (empty($templateInspection)) {
            return $this->sendError('Template Inspection not found');
        }

        $templateInspection = $this->templateInspectionRepository->update($input, $id);

        return $this->sendResponse($templateInspection->toArray(), 'TemplateInspection updated successfully');
    }

    /**
     * Remove the specified TemplateInspection from storage.
     * DELETE /templateInspections/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TemplateInspection $templateInspection */
        $templateInspection = $this->templateInspectionRepository->findWithoutFail($id);

        if (empty($templateInspection)) {
            return $this->sendError('Template Inspection not found');
        }

        $templateInspection->delete();

        return $this->sendResponse($id, 'Template Inspection deleted successfully');
    }
}
