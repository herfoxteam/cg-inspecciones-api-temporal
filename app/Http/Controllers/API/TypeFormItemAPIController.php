<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTypeFormItemAPIRequest;
use App\Http\Requests\API\UpdateTypeFormItemAPIRequest;
use App\Models\TypeFormItem;
use App\Repositories\TypeFormItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TypeFormItemController
 * @package App\Http\Controllers\API
 */

class TypeFormItemAPIController extends AppBaseController
{
    /** @var  TypeFormItemRepository */
    private $typeFormItemRepository;

    public function __construct(TypeFormItemRepository $typeFormItemRepo)
    {
        $this->typeFormItemRepository = $typeFormItemRepo;
    }

    /**
     * Display a listing of the TypeFormItem.
     * GET|HEAD /typeFormItems
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->typeFormItemRepository->pushCriteria(new RequestCriteria($request));
        $this->typeFormItemRepository->pushCriteria(new LimitOffsetCriteria($request));
        $typeFormItems = $this->typeFormItemRepository->all();

        return $this->sendResponse($typeFormItems->toArray(), 'Type Form Items retrieved successfully');
    }

    /**
     * Store a newly created TypeFormItem in storage.
     * POST /typeFormItems
     *
     * @param CreateTypeFormItemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeFormItemAPIRequest $request)
    {
        $input = $request->all();

        $typeFormItems = $this->typeFormItemRepository->create($input);

        return $this->sendResponse($typeFormItems->toArray(), 'Type Form Item saved successfully');
    }

    /**
     * Display the specified TypeFormItem.
     * GET|HEAD /typeFormItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TypeFormItem $typeFormItem */
        $typeFormItem = $this->typeFormItemRepository->findWithoutFail($id);

        if (empty($typeFormItem)) {
            return $this->sendError('Type Form Item not found');
        }

        return $this->sendResponse($typeFormItem->toArray(), 'Type Form Item retrieved successfully');
    }

    /**
     * Update the specified TypeFormItem in storage.
     * PUT/PATCH /typeFormItems/{id}
     *
     * @param  int $id
     * @param UpdateTypeFormItemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeFormItemAPIRequest $request)
    {
        $input = $request->all();

        /** @var TypeFormItem $typeFormItem */
        $typeFormItem = $this->typeFormItemRepository->findWithoutFail($id);

        if (empty($typeFormItem)) {
            return $this->sendError('Type Form Item not found');
        }

        $typeFormItem = $this->typeFormItemRepository->update($input, $id);

        return $this->sendResponse($typeFormItem->toArray(), 'TypeFormItem updated successfully');
    }

    /**
     * Remove the specified TypeFormItem from storage.
     * DELETE /typeFormItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TypeFormItem $typeFormItem */
        $typeFormItem = $this->typeFormItemRepository->findWithoutFail($id);

        if (empty($typeFormItem)) {
            return $this->sendError('Type Form Item not found');
        }

        $typeFormItem->delete();

        return $this->sendResponse($id, 'Type Form Item deleted successfully');
    }
}
