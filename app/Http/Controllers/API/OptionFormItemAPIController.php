<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOptionFormItemAPIRequest;
use App\Http\Requests\API\UpdateOptionFormItemAPIRequest;
use App\Models\OptionFormItem;
use App\Repositories\OptionFormItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class OptionFormItemController
 * @package App\Http\Controllers\API
 */

class OptionFormItemAPIController extends AppBaseController
{
    /** @var  OptionFormItemRepository */
    private $optionFormItemRepository;

    public function __construct(OptionFormItemRepository $optionFormItemRepo)
    {
        $this->optionFormItemRepository = $optionFormItemRepo;
    }

    /**
     * Display a listing of the OptionFormItem.
     * GET|HEAD /optionFormItems
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->optionFormItemRepository->pushCriteria(new RequestCriteria($request));
        $this->optionFormItemRepository->pushCriteria(new LimitOffsetCriteria($request));
        $optionFormItems = $this->optionFormItemRepository->all();

        return $this->sendResponse($optionFormItems->toArray(), 'Option Form Items retrieved successfully');
    }

    /**
     * Store a newly created OptionFormItem in storage.
     * POST /optionFormItems
     *
     * @param CreateOptionFormItemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateOptionFormItemAPIRequest $request)
    {
        $input = $request->all();

        $optionFormItems = $this->optionFormItemRepository->create($input);

        return $this->sendResponse($optionFormItems->toArray(), 'Option Form Item saved successfully');
    }

    /**
     * Display the specified OptionFormItem.
     * GET|HEAD /optionFormItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var OptionFormItem $optionFormItem */
        $optionFormItem = $this->optionFormItemRepository->findWithoutFail($id);

        if (empty($optionFormItem)) {
            return $this->sendError('Option Form Item not found');
        }

        return $this->sendResponse($optionFormItem->toArray(), 'Option Form Item retrieved successfully');
    }

    /**
     * Update the specified OptionFormItem in storage.
     * PUT/PATCH /optionFormItems/{id}
     *
     * @param  int $id
     * @param UpdateOptionFormItemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOptionFormItemAPIRequest $request)
    {
        $input = $request->all();

        /** @var OptionFormItem $optionFormItem */
        $optionFormItem = $this->optionFormItemRepository->findWithoutFail($id);

        if (empty($optionFormItem)) {
            return $this->sendError('Option Form Item not found');
        }

        $optionFormItem = $this->optionFormItemRepository->update($input, $id);

        return $this->sendResponse($optionFormItem->toArray(), 'OptionFormItem updated successfully');
    }

    /**
     * Remove the specified OptionFormItem from storage.
     * DELETE /optionFormItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var OptionFormItem $optionFormItem */
        $optionFormItem = $this->optionFormItemRepository->findWithoutFail($id);

        if (empty($optionFormItem)) {
            return $this->sendError('Option Form Item not found');
        }

        $optionFormItem->delete();

        return $this->sendResponse($id, 'Option Form Item deleted successfully');
    }
}
