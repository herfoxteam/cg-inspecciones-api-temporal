<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePhotographicEvidenceAPIRequest;
use App\Http\Requests\API\UpdatePhotographicEvidenceAPIRequest;
use App\Models\PhotographicEvidence;
use App\Repositories\PhotographicEvidenceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PhotographicEvidenceController
 * @package App\Http\Controllers\API
 */

class PhotographicEvidenceAPIController extends AppBaseController
{
    /** @var  PhotographicEvidenceRepository */
    private $photographicEvidenceRepository;

    public function __construct(PhotographicEvidenceRepository $photographicEvidenceRepo)
    {
        $this->photographicEvidenceRepository = $photographicEvidenceRepo;
    }

    /**
     * Display a listing of the PhotographicEvidence.
     * GET|HEAD /photographicEvidences
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->photographicEvidenceRepository->pushCriteria(new RequestCriteria($request));
        $this->photographicEvidenceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $photographicEvidences = $this->photographicEvidenceRepository->all();

        return $this->sendResponse($photographicEvidences->toArray(), 'Photographic Evidences retrieved successfully');
    }

    /**
     * Store a newly created PhotographicEvidence in storage.
     * POST /photographicEvidences
     *
     * @param CreatePhotographicEvidenceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePhotographicEvidenceAPIRequest $request)
    {
        $input = $request->all();

        $photographicEvidences = $this->photographicEvidenceRepository->create($input);

        return $this->sendResponse($photographicEvidences->toArray(), 'Photographic Evidence saved successfully');
    }

    /**
     * Display the specified PhotographicEvidence.
     * GET|HEAD /photographicEvidences/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PhotographicEvidence $photographicEvidence */
        $photographicEvidence = $this->photographicEvidenceRepository->findWithoutFail($id);

        if (empty($photographicEvidence)) {
            return $this->sendError('Photographic Evidence not found');
        }

        return $this->sendResponse($photographicEvidence->toArray(), 'Photographic Evidence retrieved successfully');
    }

    /**
     * Update the specified PhotographicEvidence in storage.
     * PUT/PATCH /photographicEvidences/{id}
     *
     * @param  int $id
     * @param UpdatePhotographicEvidenceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePhotographicEvidenceAPIRequest $request)
    {
        $input = $request->all();

        /** @var PhotographicEvidence $photographicEvidence */
        $photographicEvidence = $this->photographicEvidenceRepository->findWithoutFail($id);

        if (empty($photographicEvidence)) {
            return $this->sendError('Photographic Evidence not found');
        }

        $photographicEvidence = $this->photographicEvidenceRepository->update($input, $id);

        return $this->sendResponse($photographicEvidence->toArray(), 'PhotographicEvidence updated successfully');
    }

    /**
     * Remove the specified PhotographicEvidence from storage.
     * DELETE /photographicEvidences/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PhotographicEvidence $photographicEvidence */
        $photographicEvidence = $this->photographicEvidenceRepository->findWithoutFail($id);

        if (empty($photographicEvidence)) {
            return $this->sendError('Photographic Evidence not found');
        }

        $photographicEvidence->delete();

        return $this->sendResponse($id, 'Photographic Evidence deleted successfully');
    }
}
