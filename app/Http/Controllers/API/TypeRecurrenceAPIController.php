<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTypeRecurrenceAPIRequest;
use App\Http\Requests\API\UpdateTypeRecurrenceAPIRequest;
use App\Models\TypeRecurrence;
use App\Repositories\TypeRecurrenceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TypeRecurrenceController
 * @package App\Http\Controllers\API
 */

class TypeRecurrenceAPIController extends AppBaseController
{
    /** @var  TypeRecurrenceRepository */
    private $typeRecurrenceRepository;

    public function __construct(TypeRecurrenceRepository $typeRecurrenceRepo)
    {
        $this->typeRecurrenceRepository = $typeRecurrenceRepo;
    }

    /**
     * Display a listing of the TypeRecurrence.
     * GET|HEAD /typeRecurrences
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->typeRecurrenceRepository->pushCriteria(new RequestCriteria($request));
        $this->typeRecurrenceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $typeRecurrences = $this->typeRecurrenceRepository->all();

        return $this->sendResponse($typeRecurrences->toArray(), 'Type Recurrences retrieved successfully');
    }

    /**
     * Store a newly created TypeRecurrence in storage.
     * POST /typeRecurrences
     *
     * @param CreateTypeRecurrenceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeRecurrenceAPIRequest $request)
    {
        $input = $request->all();

        $typeRecurrences = $this->typeRecurrenceRepository->create($input);

        return $this->sendResponse($typeRecurrences->toArray(), 'Type Recurrence saved successfully');
    }

    /**
     * Display the specified TypeRecurrence.
     * GET|HEAD /typeRecurrences/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TypeRecurrence $typeRecurrence */
        $typeRecurrence = $this->typeRecurrenceRepository->findWithoutFail($id);

        if (empty($typeRecurrence)) {
            return $this->sendError('Type Recurrence not found');
        }

        return $this->sendResponse($typeRecurrence->toArray(), 'Type Recurrence retrieved successfully');
    }

    /**
     * Update the specified TypeRecurrence in storage.
     * PUT/PATCH /typeRecurrences/{id}
     *
     * @param  int $id
     * @param UpdateTypeRecurrenceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeRecurrenceAPIRequest $request)
    {
        $input = $request->all();

        /** @var TypeRecurrence $typeRecurrence */
        $typeRecurrence = $this->typeRecurrenceRepository->findWithoutFail($id);

        if (empty($typeRecurrence)) {
            return $this->sendError('Type Recurrence not found');
        }

        $typeRecurrence = $this->typeRecurrenceRepository->update($input, $id);

        return $this->sendResponse($typeRecurrence->toArray(), 'TypeRecurrence updated successfully');
    }

    /**
     * Remove the specified TypeRecurrence from storage.
     * DELETE /typeRecurrences/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TypeRecurrence $typeRecurrence */
        $typeRecurrence = $this->typeRecurrenceRepository->findWithoutFail($id);

        if (empty($typeRecurrence)) {
            return $this->sendError('Type Recurrence not found');
        }

        $typeRecurrence->delete();

        return $this->sendResponse($id, 'Type Recurrence deleted successfully');
    }
}
