<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePriorityActionPlanAPIRequest;
use App\Http\Requests\API\UpdatePriorityActionPlanAPIRequest;
use App\Models\PriorityActionPlan;
use App\Repositories\PriorityActionPlanRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PriorityActionPlanController
 * @package App\Http\Controllers\API
 */

class PriorityActionPlanAPIController extends AppBaseController
{
    /** @var  PriorityActionPlanRepository */
    private $priorityActionPlanRepository;

    public function __construct(PriorityActionPlanRepository $priorityActionPlanRepo)
    {
        $this->priorityActionPlanRepository = $priorityActionPlanRepo;
    }

    /**
     * Display a listing of the PriorityActionPlan.
     * GET|HEAD /priorityActionPlans
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->priorityActionPlanRepository->pushCriteria(new RequestCriteria($request));
        $this->priorityActionPlanRepository->pushCriteria(new LimitOffsetCriteria($request));
        $priorityActionPlans = $this->priorityActionPlanRepository->all();

        return $this->sendResponse($priorityActionPlans->toArray(), 'Priority Action Plans retrieved successfully');
    }

    /**
     * Store a newly created PriorityActionPlan in storage.
     * POST /priorityActionPlans
     *
     * @param CreatePriorityActionPlanAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePriorityActionPlanAPIRequest $request)
    {
        $input = $request->all();

        $priorityActionPlans = $this->priorityActionPlanRepository->create($input);

        return $this->sendResponse($priorityActionPlans->toArray(), 'Priority Action Plan saved successfully');
    }

    /**
     * Display the specified PriorityActionPlan.
     * GET|HEAD /priorityActionPlans/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PriorityActionPlan $priorityActionPlan */
        $priorityActionPlan = $this->priorityActionPlanRepository->findWithoutFail($id);

        if (empty($priorityActionPlan)) {
            return $this->sendError('Priority Action Plan not found');
        }

        return $this->sendResponse($priorityActionPlan->toArray(), 'Priority Action Plan retrieved successfully');
    }

    /**
     * Update the specified PriorityActionPlan in storage.
     * PUT/PATCH /priorityActionPlans/{id}
     *
     * @param  int $id
     * @param UpdatePriorityActionPlanAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePriorityActionPlanAPIRequest $request)
    {
        $input = $request->all();

        /** @var PriorityActionPlan $priorityActionPlan */
        $priorityActionPlan = $this->priorityActionPlanRepository->findWithoutFail($id);

        if (empty($priorityActionPlan)) {
            return $this->sendError('Priority Action Plan not found');
        }

        $priorityActionPlan = $this->priorityActionPlanRepository->update($input, $id);

        return $this->sendResponse($priorityActionPlan->toArray(), 'PriorityActionPlan updated successfully');
    }

    /**
     * Remove the specified PriorityActionPlan from storage.
     * DELETE /priorityActionPlans/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PriorityActionPlan $priorityActionPlan */
        $priorityActionPlan = $this->priorityActionPlanRepository->findWithoutFail($id);

        if (empty($priorityActionPlan)) {
            return $this->sendError('Priority Action Plan not found');
        }

        $priorityActionPlan->delete();

        return $this->sendResponse($id, 'Priority Action Plan deleted successfully');
    }
}
