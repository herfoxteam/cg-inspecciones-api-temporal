<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInspectionFormAPIRequest;
use App\Http\Requests\API\UpdateInspectionFormAPIRequest;
use App\Models\InspectionForm;
use App\Repositories\InspectionFormRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class InspectionFormController
 * @package App\Http\Controllers\API
 */

class InspectionFormAPIController extends AppBaseController
{
    /** @var  InspectionFormRepository */
    private $inspectionFormRepository;

    public function __construct(InspectionFormRepository $inspectionFormRepo)
    {
        $this->inspectionFormRepository = $inspectionFormRepo;
    }

    /**
     * Display a listing of the InspectionForm.
     * GET|HEAD /inspectionForms
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->inspectionFormRepository->pushCriteria(new RequestCriteria($request));
        $this->inspectionFormRepository->pushCriteria(new LimitOffsetCriteria($request));
        $inspectionForms = $this->inspectionFormRepository->all();

        return $this->sendResponse($inspectionForms->toArray(), 'Inspection Forms retrieved successfully');
    }

    /**
     * Store a newly created InspectionForm in storage.
     * POST /inspectionForms
     *
     * @param CreateInspectionFormAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInspectionFormAPIRequest $request)
    {
        $input = $request->all();

        $inspectionForms = $this->inspectionFormRepository->create($input);

        return $this->sendResponse($inspectionForms->toArray(), 'Inspection Form saved successfully');
    }

    /**
     * Display the specified InspectionForm.
     * GET|HEAD /inspectionForms/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var InspectionForm $inspectionForm */
        $inspectionForm = $this->inspectionFormRepository->findWithoutFail($id);

        if (empty($inspectionForm)) {
            return $this->sendError('Inspection Form not found');
        }

        return $this->sendResponse($inspectionForm->toArray(), 'Inspection Form retrieved successfully');
    }

    /**
     * Update the specified InspectionForm in storage.
     * PUT/PATCH /inspectionForms/{id}
     *
     * @param  int $id
     * @param UpdateInspectionFormAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInspectionFormAPIRequest $request)
    {
        $input = $request->all();

        /** @var InspectionForm $inspectionForm */
        $inspectionForm = $this->inspectionFormRepository->findWithoutFail($id);

        if (empty($inspectionForm)) {
            return $this->sendError('Inspection Form not found');
        }

        $inspectionForm = $this->inspectionFormRepository->update($input, $id);

        return $this->sendResponse($inspectionForm->toArray(), 'InspectionForm updated successfully');
    }

    /**
     * Remove the specified InspectionForm from storage.
     * DELETE /inspectionForms/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var InspectionForm $inspectionForm */
        $inspectionForm = $this->inspectionFormRepository->findWithoutFail($id);

        if (empty($inspectionForm)) {
            return $this->sendError('Inspection Form not found');
        }

        $inspectionForm->delete();

        return $this->sendResponse($id, 'Inspection Form deleted successfully');
    }
}
