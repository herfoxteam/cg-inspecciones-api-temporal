<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateObservationFormItemAPIRequest;
use App\Http\Requests\API\UpdateObservationFormItemAPIRequest;
use App\Models\ObservationFormItem;
use App\Repositories\ObservationFormItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ObservationFormItemController
 * @package App\Http\Controllers\API
 */

class ObservationFormItemAPIController extends AppBaseController
{
    /** @var  ObservationFormItemRepository */
    private $observationFormItemRepository;

    public function __construct(ObservationFormItemRepository $observationFormItemRepo)
    {
        $this->observationFormItemRepository = $observationFormItemRepo;
    }

    /**
     * Display a listing of the ObservationFormItem.
     * GET|HEAD /observationFormItems
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->observationFormItemRepository->pushCriteria(new RequestCriteria($request));
        $this->observationFormItemRepository->pushCriteria(new LimitOffsetCriteria($request));
        $observationFormItems = $this->observationFormItemRepository->all();

        return $this->sendResponse($observationFormItems->toArray(), 'Observation Form Items retrieved successfully');
    }

    /**
     * Store a newly created ObservationFormItem in storage.
     * POST /observationFormItems
     *
     * @param CreateObservationFormItemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateObservationFormItemAPIRequest $request)
    {
        $input = $request->all();

        $observationFormItems = $this->observationFormItemRepository->create($input);

        return $this->sendResponse($observationFormItems->toArray(), 'Observation Form Item saved successfully');
    }

    /**
     * Display the specified ObservationFormItem.
     * GET|HEAD /observationFormItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ObservationFormItem $observationFormItem */
        $observationFormItem = $this->observationFormItemRepository->findWithoutFail($id);

        if (empty($observationFormItem)) {
            return $this->sendError('Observation Form Item not found');
        }

        return $this->sendResponse($observationFormItem->toArray(), 'Observation Form Item retrieved successfully');
    }

    /**
     * Update the specified ObservationFormItem in storage.
     * PUT/PATCH /observationFormItems/{id}
     *
     * @param  int $id
     * @param UpdateObservationFormItemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObservationFormItemAPIRequest $request)
    {
        $input = $request->all();

        /** @var ObservationFormItem $observationFormItem */
        $observationFormItem = $this->observationFormItemRepository->findWithoutFail($id);

        if (empty($observationFormItem)) {
            return $this->sendError('Observation Form Item not found');
        }

        $observationFormItem = $this->observationFormItemRepository->update($input, $id);

        return $this->sendResponse($observationFormItem->toArray(), 'ObservationFormItem updated successfully');
    }

    /**
     * Remove the specified ObservationFormItem from storage.
     * DELETE /observationFormItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ObservationFormItem $observationFormItem */
        $observationFormItem = $this->observationFormItemRepository->findWithoutFail($id);

        if (empty($observationFormItem)) {
            return $this->sendError('Observation Form Item not found');
        }

        $observationFormItem->delete();

        return $this->sendResponse($id, 'Observation Form Item deleted successfully');
    }
}
