<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStatusCategoryFormAPIRequest;
use App\Http\Requests\API\UpdateStatusCategoryFormAPIRequest;
use App\Models\StatusCategoryForm;
use App\Repositories\StatusCategoryFormRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StatusCategoryFormController
 * @package App\Http\Controllers\API
 */

class StatusCategoryFormAPIController extends AppBaseController
{
    /** @var  StatusCategoryFormRepository */
    private $statusCategoryFormRepository;

    public function __construct(StatusCategoryFormRepository $statusCategoryFormRepo)
    {
        $this->statusCategoryFormRepository = $statusCategoryFormRepo;
    }

    /**
     * Display a listing of the StatusCategoryForm.
     * GET|HEAD /statusCategoryForms
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->statusCategoryFormRepository->pushCriteria(new RequestCriteria($request));
        $this->statusCategoryFormRepository->pushCriteria(new LimitOffsetCriteria($request));
        $statusCategoryForms = $this->statusCategoryFormRepository->all();

        return $this->sendResponse($statusCategoryForms->toArray(), 'Status Category Forms retrieved successfully');
    }

    /**
     * Store a newly created StatusCategoryForm in storage.
     * POST /statusCategoryForms
     *
     * @param CreateStatusCategoryFormAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStatusCategoryFormAPIRequest $request)
    {
        $input = $request->all();

        $statusCategoryForms = $this->statusCategoryFormRepository->create($input);

        return $this->sendResponse($statusCategoryForms->toArray(), 'Status Category Form saved successfully');
    }

    /**
     * Display the specified StatusCategoryForm.
     * GET|HEAD /statusCategoryForms/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StatusCategoryForm $statusCategoryForm */
        $statusCategoryForm = $this->statusCategoryFormRepository->findWithoutFail($id);

        if (empty($statusCategoryForm)) {
            return $this->sendError('Status Category Form not found');
        }

        return $this->sendResponse($statusCategoryForm->toArray(), 'Status Category Form retrieved successfully');
    }

    /**
     * Update the specified StatusCategoryForm in storage.
     * PUT/PATCH /statusCategoryForms/{id}
     *
     * @param  int $id
     * @param UpdateStatusCategoryFormAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStatusCategoryFormAPIRequest $request)
    {
        $input = $request->all();

        /** @var StatusCategoryForm $statusCategoryForm */
        $statusCategoryForm = $this->statusCategoryFormRepository->findWithoutFail($id);

        if (empty($statusCategoryForm)) {
            return $this->sendError('Status Category Form not found');
        }

        $statusCategoryForm = $this->statusCategoryFormRepository->update($input, $id);

        return $this->sendResponse($statusCategoryForm->toArray(), 'StatusCategoryForm updated successfully');
    }

    /**
     * Remove the specified StatusCategoryForm from storage.
     * DELETE /statusCategoryForms/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StatusCategoryForm $statusCategoryForm */
        $statusCategoryForm = $this->statusCategoryFormRepository->findWithoutFail($id);

        if (empty($statusCategoryForm)) {
            return $this->sendError('Status Category Form not found');
        }

        $statusCategoryForm->delete();

        return $this->sendResponse($id, 'Status Category Form deleted successfully');
    }
}
