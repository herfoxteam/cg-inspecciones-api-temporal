<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateGroupMemberAPIRequest;
use App\Http\Requests\API\UpdateGroupMemberAPIRequest;
use App\Models\GroupMember;
use App\Repositories\GroupMemberRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class GroupMemberController
 * @package App\Http\Controllers\API
 */

class GroupMemberAPIController extends AppBaseController
{
    /** @var  GroupMemberRepository */
    private $groupMemberRepository;

    public function __construct(GroupMemberRepository $groupMemberRepo)
    {
        $this->groupMemberRepository = $groupMemberRepo;
    }

    /**
     * Display a listing of the GroupMember.
     * GET|HEAD /groupMembers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->groupMemberRepository->pushCriteria(new RequestCriteria($request));
        $this->groupMemberRepository->pushCriteria(new LimitOffsetCriteria($request));
        $groupMembers = $this->groupMemberRepository->all();

        return $this->sendResponse($groupMembers->toArray(), 'Group Members retrieved successfully');
    }

    /**
     * Store a newly created GroupMember in storage.
     * POST /groupMembers
     *
     * @param CreateGroupMemberAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateGroupMemberAPIRequest $request)
    {
        $input = $request->all();

        $groupMembers = $this->groupMemberRepository->create($input);

        return $this->sendResponse($groupMembers->toArray(), 'Group Member saved successfully');
    }

    /**
     * Display the specified GroupMember.
     * GET|HEAD /groupMembers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var GroupMember $groupMember */
        $groupMember = $this->groupMemberRepository->findWithoutFail($id);

        if (empty($groupMember)) {
            return $this->sendError('Group Member not found');
        }

        return $this->sendResponse($groupMember->toArray(), 'Group Member retrieved successfully');
    }

    /**
     * Update the specified GroupMember in storage.
     * PUT/PATCH /groupMembers/{id}
     *
     * @param  int $id
     * @param UpdateGroupMemberAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGroupMemberAPIRequest $request)
    {
        $input = $request->all();

        /** @var GroupMember $groupMember */
        $groupMember = $this->groupMemberRepository->findWithoutFail($id);

        if (empty($groupMember)) {
            return $this->sendError('Group Member not found');
        }

        $groupMember = $this->groupMemberRepository->update($input, $id);

        return $this->sendResponse($groupMember->toArray(), 'GroupMember updated successfully');
    }

    /**
     * Remove the specified GroupMember from storage.
     * DELETE /groupMembers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var GroupMember $groupMember */
        $groupMember = $this->groupMemberRepository->findWithoutFail($id);

        if (empty($groupMember)) {
            return $this->sendError('Group Member not found');
        }

        $groupMember->delete();

        return $this->sendResponse($id, 'Group Member deleted successfully');
    }
}
