<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCategoryInspectionFormAPIRequest;
use App\Http\Requests\API\UpdateCategoryInspectionFormAPIRequest;
use App\Models\CategoryInspectionForm;
use App\Repositories\CategoryInspectionFormRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CategoryInspectionFormController
 * @package App\Http\Controllers\API
 */

class CategoryInspectionFormAPIController extends AppBaseController
{
    /** @var  CategoryInspectionFormRepository */
    private $categoryInspectionFormRepository;

    public function __construct(CategoryInspectionFormRepository $categoryInspectionFormRepo)
    {
        $this->categoryInspectionFormRepository = $categoryInspectionFormRepo;
    }

    /**
     * Display a listing of the CategoryInspectionForm.
     * GET|HEAD /categoryInspectionForms
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->categoryInspectionFormRepository->pushCriteria(new RequestCriteria($request));
        $this->categoryInspectionFormRepository->pushCriteria(new LimitOffsetCriteria($request));
        $categoryInspectionForms = $this->categoryInspectionFormRepository->all();

        return $this->sendResponse($categoryInspectionForms->toArray(), 'Category Inspection Forms retrieved successfully');
    }

    /**
     * Store a newly created CategoryInspectionForm in storage.
     * POST /categoryInspectionForms
     *
     * @param CreateCategoryInspectionFormAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryInspectionFormAPIRequest $request)
    {
        $input = $request->all();

        $categoryInspectionForms = $this->categoryInspectionFormRepository->create($input);

        return $this->sendResponse($categoryInspectionForms->toArray(), 'Category Inspection Form saved successfully');
    }

    /**
     * Display the specified CategoryInspectionForm.
     * GET|HEAD /categoryInspectionForms/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CategoryInspectionForm $categoryInspectionForm */
        $categoryInspectionForm = $this->categoryInspectionFormRepository->findWithoutFail($id);

        if (empty($categoryInspectionForm)) {
            return $this->sendError('Category Inspection Form not found');
        }

        return $this->sendResponse($categoryInspectionForm->toArray(), 'Category Inspection Form retrieved successfully');
    }

    /**
     * Update the specified CategoryInspectionForm in storage.
     * PUT/PATCH /categoryInspectionForms/{id}
     *
     * @param  int $id
     * @param UpdateCategoryInspectionFormAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryInspectionFormAPIRequest $request)
    {
        $input = $request->all();

        /** @var CategoryInspectionForm $categoryInspectionForm */
        $categoryInspectionForm = $this->categoryInspectionFormRepository->findWithoutFail($id);

        if (empty($categoryInspectionForm)) {
            return $this->sendError('Category Inspection Form not found');
        }

        $categoryInspectionForm = $this->categoryInspectionFormRepository->update($input, $id);

        return $this->sendResponse($categoryInspectionForm->toArray(), 'CategoryInspectionForm updated successfully');
    }

    /**
     * Remove the specified CategoryInspectionForm from storage.
     * DELETE /categoryInspectionForms/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CategoryInspectionForm $categoryInspectionForm */
        $categoryInspectionForm = $this->categoryInspectionFormRepository->findWithoutFail($id);

        if (empty($categoryInspectionForm)) {
            return $this->sendError('Category Inspection Form not found');
        }

        $categoryInspectionForm->delete();

        return $this->sendResponse($id, 'Category Inspection Form deleted successfully');
    }
}
