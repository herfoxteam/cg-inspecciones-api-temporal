<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInspectionFormItemAPIRequest;
use App\Http\Requests\API\UpdateInspectionFormItemAPIRequest;
use App\Models\InspectionFormItem;
use App\Repositories\InspectionFormItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class InspectionFormItemController
 * @package App\Http\Controllers\API
 */

class InspectionFormItemAPIController extends AppBaseController
{
    /** @var  InspectionFormItemRepository */
    private $inspectionFormItemRepository;

    public function __construct(InspectionFormItemRepository $inspectionFormItemRepo)
    {
        $this->inspectionFormItemRepository = $inspectionFormItemRepo;
    }

    /**
     * Display a listing of the InspectionFormItem.
     * GET|HEAD /inspectionFormItems
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->inspectionFormItemRepository->pushCriteria(new RequestCriteria($request));
        $this->inspectionFormItemRepository->pushCriteria(new LimitOffsetCriteria($request));
        $inspectionFormItems = $this->inspectionFormItemRepository->all();

        return $this->sendResponse($inspectionFormItems->toArray(), 'Inspection Form Items retrieved successfully');
    }

    /**
     * Store a newly created InspectionFormItem in storage.
     * POST /inspectionFormItems
     *
     * @param CreateInspectionFormItemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInspectionFormItemAPIRequest $request)
    {
        $input = $request->all();

        $inspectionFormItems = $this->inspectionFormItemRepository->create($input);

        return $this->sendResponse($inspectionFormItems->toArray(), 'Inspection Form Item saved successfully');
    }

    /**
     * Display the specified InspectionFormItem.
     * GET|HEAD /inspectionFormItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var InspectionFormItem $inspectionFormItem */
        $inspectionFormItem = $this->inspectionFormItemRepository->findWithoutFail($id);

        if (empty($inspectionFormItem)) {
            return $this->sendError('Inspection Form Item not found');
        }

        return $this->sendResponse($inspectionFormItem->toArray(), 'Inspection Form Item retrieved successfully');
    }

    /**
     * Update the specified InspectionFormItem in storage.
     * PUT/PATCH /inspectionFormItems/{id}
     *
     * @param  int $id
     * @param UpdateInspectionFormItemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInspectionFormItemAPIRequest $request)
    {
        $input = $request->all();

        /** @var InspectionFormItem $inspectionFormItem */
        $inspectionFormItem = $this->inspectionFormItemRepository->findWithoutFail($id);

        if (empty($inspectionFormItem)) {
            return $this->sendError('Inspection Form Item not found');
        }

        $inspectionFormItem = $this->inspectionFormItemRepository->update($input, $id);

        return $this->sendResponse($inspectionFormItem->toArray(), 'InspectionFormItem updated successfully');
    }

    /**
     * Remove the specified InspectionFormItem from storage.
     * DELETE /inspectionFormItems/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var InspectionFormItem $inspectionFormItem */
        $inspectionFormItem = $this->inspectionFormItemRepository->findWithoutFail($id);

        if (empty($inspectionFormItem)) {
            return $this->sendError('Inspection Form Item not found');
        }

        $inspectionFormItem->delete();

        return $this->sendResponse($id, 'Inspection Form Item deleted successfully');
    }
}
