<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAvatarAPIRequest;
use App\Http\Requests\API\UpdateAvatarAPIRequest;
use App\Models\Avatar;
use App\Repositories\AvatarRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AvatarController
 * @package App\Http\Controllers\API
 */

class AvatarAPIController extends AppBaseController
{
    /** @var  AvatarRepository */
    private $avatarRepository;

    public function __construct(AvatarRepository $avatarRepo)
    {
        $this->avatarRepository = $avatarRepo;
    }

    /**
     * Display a listing of the Avatar.
     * GET|HEAD /avatars
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->avatarRepository->pushCriteria(new RequestCriteria($request));
        $this->avatarRepository->pushCriteria(new LimitOffsetCriteria($request));
        $avatars = $this->avatarRepository->all();

        return $this->sendResponse($avatars->toArray(), 'Avatars retrieved successfully');
    }

    /**
     * Store a newly created Avatar in storage.
     * POST /avatars
     *
     * @param CreateAvatarAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAvatarAPIRequest $request)
    {
        $input = $request->all();

        $avatars = $this->avatarRepository->create($input);

        return $this->sendResponse($avatars->toArray(), 'Avatar saved successfully');
    }

    /**
     * Display the specified Avatar.
     * GET|HEAD /avatars/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Avatar $avatar */
        $avatar = $this->avatarRepository->findWithoutFail($id);

        if (empty($avatar)) {
            return $this->sendError('Avatar not found');
        }

        return $this->sendResponse($avatar->toArray(), 'Avatar retrieved successfully');
    }

    /**
     * Update the specified Avatar in storage.
     * PUT/PATCH /avatars/{id}
     *
     * @param  int $id
     * @param UpdateAvatarAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAvatarAPIRequest $request)
    {
        $input = $request->all();

        /** @var Avatar $avatar */
        $avatar = $this->avatarRepository->findWithoutFail($id);

        if (empty($avatar)) {
            return $this->sendError('Avatar not found');
        }

        $avatar = $this->avatarRepository->update($input, $id);

        return $this->sendResponse($avatar->toArray(), 'Avatar updated successfully');
    }

    /**
     * Remove the specified Avatar from storage.
     * DELETE /avatars/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Avatar $avatar */
        $avatar = $this->avatarRepository->findWithoutFail($id);

        if (empty($avatar)) {
            return $this->sendError('Avatar not found');
        }

        $avatar->delete();

        return $this->sendResponse($id, 'Avatar deleted successfully');
    }
}
