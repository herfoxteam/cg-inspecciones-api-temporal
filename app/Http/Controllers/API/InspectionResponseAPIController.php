<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInspectionResponseAPIRequest;
use App\Http\Requests\API\UpdateInspectionResponseAPIRequest;
use App\Models\InspectionResponse;
use App\Repositories\InspectionResponseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class InspectionResponseController
 * @package App\Http\Controllers\API
 */

class InspectionResponseAPIController extends AppBaseController
{
    /** @var  InspectionResponseRepository */
    private $inspectionResponseRepository;

    public function __construct(InspectionResponseRepository $inspectionResponseRepo)
    {
        $this->inspectionResponseRepository = $inspectionResponseRepo;
    }

    /**
     * Display a listing of the InspectionResponse.
     * GET|HEAD /inspectionResponses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->inspectionResponseRepository->pushCriteria(new RequestCriteria($request));
        $this->inspectionResponseRepository->pushCriteria(new LimitOffsetCriteria($request));
        $inspectionResponses = $this->inspectionResponseRepository->all();

        return $this->sendResponse($inspectionResponses->toArray(), 'Inspection Responses retrieved successfully');
    }

    /**
     * Store a newly created InspectionResponse in storage.
     * POST /inspectionResponses
     *
     * @param CreateInspectionResponseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateInspectionResponseAPIRequest $request)
    {
        $input = $request->all();

        $inspectionResponses = $this->inspectionResponseRepository->create($input);

        return $this->sendResponse($inspectionResponses->toArray(), 'Inspection Response saved successfully');
    }

    /**
     * Display the specified InspectionResponse.
     * GET|HEAD /inspectionResponses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var InspectionResponse $inspectionResponse */
        $inspectionResponse = $this->inspectionResponseRepository->findWithoutFail($id);

        if (empty($inspectionResponse)) {
            return $this->sendError('Inspection Response not found');
        }

        return $this->sendResponse($inspectionResponse->toArray(), 'Inspection Response retrieved successfully');
    }

    /**
     * Update the specified InspectionResponse in storage.
     * PUT/PATCH /inspectionResponses/{id}
     *
     * @param  int $id
     * @param UpdateInspectionResponseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInspectionResponseAPIRequest $request)
    {
        $input = $request->all();

        /** @var InspectionResponse $inspectionResponse */
        $inspectionResponse = $this->inspectionResponseRepository->findWithoutFail($id);

        if (empty($inspectionResponse)) {
            return $this->sendError('Inspection Response not found');
        }

        $inspectionResponse = $this->inspectionResponseRepository->update($input, $id);

        return $this->sendResponse($inspectionResponse->toArray(), 'InspectionResponse updated successfully');
    }

    /**
     * Remove the specified InspectionResponse from storage.
     * DELETE /inspectionResponses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var InspectionResponse $inspectionResponse */
        $inspectionResponse = $this->inspectionResponseRepository->findWithoutFail($id);

        if (empty($inspectionResponse)) {
            return $this->sendError('Inspection Response not found');
        }

        $inspectionResponse->delete();

        return $this->sendResponse($id, 'Inspection Response deleted successfully');
    }
}
