<?php

namespace App\Http\Controllers;

use App\DataTables\AvatarDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAvatarRequest;
use App\Http\Requests\UpdateAvatarRequest;
use App\Repositories\AvatarRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AvatarController extends AppBaseController
{
    /** @var  AvatarRepository */
    private $avatarRepository;

    public function __construct(AvatarRepository $avatarRepo)
    {
        $this->avatarRepository = $avatarRepo;
    }

    /**
     * Display a listing of the Avatar.
     *
     * @param AvatarDataTable $avatarDataTable
     * @return Response
     */
    public function index(AvatarDataTable $avatarDataTable)
    {
        return $avatarDataTable->render('avatars.index');
    }

    /**
     * Show the form for creating a new Avatar.
     *
     * @return Response
     */
    public function create()
    {
        return view('avatars.create');
    }

    /**
     * Store a newly created Avatar in storage.
     *
     * @param CreateAvatarRequest $request
     *
     * @return Response
     */
    public function store(CreateAvatarRequest $request)
    {
        $input = $request->all();

        $avatar = $this->avatarRepository->create($input);

        Flash::success('Avatar saved successfully.');

        return redirect(route('avatars.index'));
    }

    /**
     * Display the specified Avatar.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $avatar = $this->avatarRepository->findWithoutFail($id);

        if (empty($avatar)) {
            Flash::error('Avatar not found');

            return redirect(route('avatars.index'));
        }

        return view('avatars.show')->with('avatar', $avatar);
    }

    /**
     * Show the form for editing the specified Avatar.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $avatar = $this->avatarRepository->findWithoutFail($id);

        if (empty($avatar)) {
            Flash::error('Avatar not found');

            return redirect(route('avatars.index'));
        }

        return view('avatars.edit')->with('avatar', $avatar);
    }

    /**
     * Update the specified Avatar in storage.
     *
     * @param  int              $id
     * @param UpdateAvatarRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAvatarRequest $request)
    {
        $avatar = $this->avatarRepository->findWithoutFail($id);

        if (empty($avatar)) {
            Flash::error('Avatar not found');

            return redirect(route('avatars.index'));
        }

        $avatar = $this->avatarRepository->update($request->all(), $id);

        Flash::success('Avatar updated successfully.');

        return redirect(route('avatars.index'));
    }

    /**
     * Remove the specified Avatar from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $avatar = $this->avatarRepository->findWithoutFail($id);

        if (empty($avatar)) {
            Flash::error('Avatar not found');

            return redirect(route('avatars.index'));
        }

        $this->avatarRepository->delete($id);

        Flash::success('Avatar deleted successfully.');

        return redirect(route('avatars.index'));
    }
}
