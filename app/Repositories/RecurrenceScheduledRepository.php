<?php

namespace App\Repositories;

use App\Models\RecurrenceScheduled;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RecurrenceScheduledRepository
 * @package App\Repositories
 * @version August 3, 2018, 2:57 pm -05
 *
 * @method RecurrenceScheduled findWithoutFail($id, $columns = ['*'])
 * @method RecurrenceScheduled find($id, $columns = ['*'])
 * @method RecurrenceScheduled first($columns = ['*'])
*/
class RecurrenceScheduledRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RecurrenceScheduled::class;
    }
}
