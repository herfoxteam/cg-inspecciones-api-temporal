<?php

namespace App\Repositories;

use App\Models\TypeFormItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TypeFormItemRepository
 * @package App\Repositories
 * @version August 2, 2018, 11:50 pm -05
 *
 * @method TypeFormItem findWithoutFail($id, $columns = ['*'])
 * @method TypeFormItem find($id, $columns = ['*'])
 * @method TypeFormItem first($columns = ['*'])
*/
class TypeFormItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeFormItem::class;
    }
}
