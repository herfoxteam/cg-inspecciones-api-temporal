<?php

namespace App\Repositories;

use App\Models\InspectionForm;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class InspectionFormRepository
 * @package App\Repositories
 * @version August 2, 2018, 4:58 pm -05
 *
 * @method InspectionForm findWithoutFail($id, $columns = ['*'])
 * @method InspectionForm find($id, $columns = ['*'])
 * @method InspectionForm first($columns = ['*'])
*/
class InspectionFormRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'hash',
        'template_code'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InspectionForm::class;
    }
}
