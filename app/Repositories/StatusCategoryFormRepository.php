<?php

namespace App\Repositories;

use App\Models\StatusCategoryForm;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StatusCategoryFormRepository
 * @package App\Repositories
 * @version August 2, 2018, 11:17 pm -05
 *
 * @method StatusCategoryForm findWithoutFail($id, $columns = ['*'])
 * @method StatusCategoryForm find($id, $columns = ['*'])
 * @method StatusCategoryForm first($columns = ['*'])
*/
class StatusCategoryFormRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StatusCategoryForm::class;
    }
}
