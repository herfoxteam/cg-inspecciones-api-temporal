<?php

namespace App\Repositories;

use App\Models\InspectionResponse;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class InspectionResponseRepository
 * @package App\Repositories
 * @version August 2, 2018, 5:29 pm -05
 *
 * @method InspectionResponse findWithoutFail($id, $columns = ['*'])
 * @method InspectionResponse find($id, $columns = ['*'])
 * @method InspectionResponse first($columns = ['*'])
*/
class InspectionResponseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InspectionResponse::class;
    }
}
