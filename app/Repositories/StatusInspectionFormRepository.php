<?php

namespace App\Repositories;

use App\Models\StatusInspectionForm;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StatusInspectionFormRepository
 * @package App\Repositories
 * @version August 2, 2018, 4:27 pm -05
 *
 * @method StatusInspectionForm findWithoutFail($id, $columns = ['*'])
 * @method StatusInspectionForm find($id, $columns = ['*'])
 * @method StatusInspectionForm first($columns = ['*'])
*/
class StatusInspectionFormRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StatusInspectionForm::class;
    }
}
