<?php

namespace App\Repositories;

use App\Models\ObservationFormItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ObservationFormItemRepository
 * @package App\Repositories
 * @version August 3, 2018, 12:17 pm -05
 *
 * @method ObservationFormItem findWithoutFail($id, $columns = ['*'])
 * @method ObservationFormItem find($id, $columns = ['*'])
 * @method ObservationFormItem first($columns = ['*'])
*/
class ObservationFormItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'inspection_forms_items_code',
        'description',
        'hash_form'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ObservationFormItem::class;
    }
}
