<?php

namespace App\Repositories;

use App\Models\CustomDateScheduled;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CustomDateScheduledRepository
 * @package App\Repositories
 * @version August 3, 2018, 2:27 pm -05
 *
 * @method CustomDateScheduled findWithoutFail($id, $columns = ['*'])
 * @method CustomDateScheduled find($id, $columns = ['*'])
 * @method CustomDateScheduled first($columns = ['*'])
*/
class CustomDateScheduledRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomDateScheduled::class;
    }
}
