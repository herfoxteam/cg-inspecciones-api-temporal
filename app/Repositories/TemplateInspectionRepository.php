<?php

namespace App\Repositories;

use App\Models\TemplateInspection;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TemplateInspectionRepository
 * @package App\Repositories
 * @version August 2, 2018, 4:10 pm -05
 *
 * @method TemplateInspection findWithoutFail($id, $columns = ['*'])
 * @method TemplateInspection find($id, $columns = ['*'])
 * @method TemplateInspection first($columns = ['*'])
*/
class TemplateInspectionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'code'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TemplateInspection::class;
    }
}
