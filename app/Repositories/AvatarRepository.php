<?php

namespace App\Repositories;

use App\Models\Avatar;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AvatarRepository
 * @package App\Repositories
 * @version August 2, 2018, 3:17 pm -05
 *
 * @method Avatar findWithoutFail($id, $columns = ['*'])
 * @method Avatar find($id, $columns = ['*'])
 * @method Avatar first($columns = ['*'])
*/
class AvatarRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Avatar::class;
    }
}
