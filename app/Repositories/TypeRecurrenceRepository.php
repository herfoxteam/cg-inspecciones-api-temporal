<?php

namespace App\Repositories;

use App\Models\TypeRecurrence;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TypeRecurrenceRepository
 * @package App\Repositories
 * @version August 3, 2018, 2:06 pm -05
 *
 * @method TypeRecurrence findWithoutFail($id, $columns = ['*'])
 * @method TypeRecurrence find($id, $columns = ['*'])
 * @method TypeRecurrence first($columns = ['*'])
*/
class TypeRecurrenceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'value',
        'label'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TypeRecurrence::class;
    }
}
