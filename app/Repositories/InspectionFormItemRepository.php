<?php

namespace App\Repositories;

use App\Models\InspectionFormItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class InspectionFormItemRepository
 * @package App\Repositories
 * @version August 3, 2018, 1:30 am -05
 *
 * @method InspectionFormItem findWithoutFail($id, $columns = ['*'])
 * @method InspectionFormItem find($id, $columns = ['*'])
 * @method InspectionFormItem first($columns = ['*'])
*/
class InspectionFormItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type_forms_items_id',
        'required',
        'published'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InspectionFormItem::class;
    }
}
