<?php

namespace App\Repositories;

use App\Models\UserGroup;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserGroupRepository
 * @package App\Repositories
 * @version August 2, 2018, 3:00 pm -05
 *
 * @method UserGroup findWithoutFail($id, $columns = ['*'])
 * @method UserGroup find($id, $columns = ['*'])
 * @method UserGroup first($columns = ['*'])
*/
class UserGroupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description',
        'administrator_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserGroup::class;
    }
}
