<?php

namespace App\Repositories;

use App\Models\GroupMember;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class GroupMemberRepository
 * @package App\Repositories
 * @version August 2, 2018, 3:09 pm -05
 *
 * @method GroupMember findWithoutFail($id, $columns = ['*'])
 * @method GroupMember find($id, $columns = ['*'])
 * @method GroupMember first($columns = ['*'])
*/
class GroupMemberRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_group_id',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GroupMember::class;
    }
}
