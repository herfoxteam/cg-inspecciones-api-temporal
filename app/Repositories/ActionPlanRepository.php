<?php

namespace App\Repositories;

use App\Models\ActionPlan;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ActionPlanRepository
 * @package App\Repositories
 * @version August 3, 2018, 11:36 am -05
 *
 * @method ActionPlan findWithoutFail($id, $columns = ['*'])
 * @method ActionPlan find($id, $columns = ['*'])
 * @method ActionPlan first($columns = ['*'])
*/
class ActionPlanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description',
        'hash_form'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ActionPlan::class;
    }
}
