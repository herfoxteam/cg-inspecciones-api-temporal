<?php

namespace App\Repositories;

use App\Models\OptionFormItem;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OptionFormItemRepository
 * @package App\Repositories
 * @version August 3, 2018, 12:37 pm -05
 *
 * @method OptionFormItem findWithoutFail($id, $columns = ['*'])
 * @method OptionFormItem find($id, $columns = ['*'])
 * @method OptionFormItem first($columns = ['*'])
*/
class OptionFormItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'form_code_item',
        'label'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OptionFormItem::class;
    }
}
