<?php

namespace App\Repositories;

use App\Models\StatusTemplateInspection;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StatusTemplateInspectionRepository
 * @package App\Repositories
 * @version August 2, 2018, 3:42 pm -05
 *
 * @method StatusTemplateInspection findWithoutFail($id, $columns = ['*'])
 * @method StatusTemplateInspection find($id, $columns = ['*'])
 * @method StatusTemplateInspection first($columns = ['*'])
*/
class StatusTemplateInspectionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StatusTemplateInspection::class;
    }
}
