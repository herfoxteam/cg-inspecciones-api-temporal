<?php

namespace App\Repositories;

use App\Models\CategoryInspectionForm;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CategoryInspectionFormRepository
 * @package App\Repositories
 * @version August 2, 2018, 8:08 pm -05
 *
 * @method CategoryInspectionForm findWithoutFail($id, $columns = ['*'])
 * @method CategoryInspectionForm find($id, $columns = ['*'])
 * @method CategoryInspectionForm first($columns = ['*'])
*/
class CategoryInspectionFormRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'status_categories_forms_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CategoryInspectionForm::class;
    }
}
