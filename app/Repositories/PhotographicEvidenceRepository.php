<?php

namespace App\Repositories;

use App\Models\PhotographicEvidence;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PhotographicEvidenceRepository
 * @package App\Repositories
 * @version August 3, 2018, 1:37 pm -05
 *
 * @method PhotographicEvidence findWithoutFail($id, $columns = ['*'])
 * @method PhotographicEvidence find($id, $columns = ['*'])
 * @method PhotographicEvidence first($columns = ['*'])
*/
class PhotographicEvidenceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'inspection_forms_items_code',
        'hash_form'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PhotographicEvidence::class;
    }
}
