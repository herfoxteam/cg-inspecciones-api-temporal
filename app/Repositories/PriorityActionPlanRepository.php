<?php

namespace App\Repositories;

use App\Models\PriorityActionPlan;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PriorityActionPlanRepository
 * @package App\Repositories
 * @version August 3, 2018, 10:36 am -05
 *
 * @method PriorityActionPlan findWithoutFail($id, $columns = ['*'])
 * @method PriorityActionPlan find($id, $columns = ['*'])
 * @method PriorityActionPlan first($columns = ['*'])
*/
class PriorityActionPlanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PriorityActionPlan::class;
    }
}
