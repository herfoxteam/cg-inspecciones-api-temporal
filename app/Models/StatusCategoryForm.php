<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class StatusCategoryForm
 * @package App\Models
 * @version August 2, 2018, 11:17 pm -05
 *
 * @property string title
 * @property string description
 */
class StatusCategoryForm extends Model
{

    public $table = 'status_category_forms';
    


    public $fillable = [
        'title',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'string|required'
    ];

    
}
