<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class PhotographicEvidence
 * @package App\Models
 * @version August 3, 2018, 1:37 pm -05
 *
 * @property string inspection_forms_items_code
 * @property string thumbnail
 * @property string original_file
 * @property string hash_form
 */
class PhotographicEvidence extends Model
{

    public $table = 'photographic_evidences';
    


    public $fillable = [
        'inspection_forms_items_code',
        'thumbnail',
        'original_file',
        'hash_form'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'inspection_forms_items_code' => 'string',
        'thumbnail' => 'string',
        'original_file' => 'string',
        'hash_form' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'inspection_forms_items_code' => 'string|required',
        'thumbnail' => 'string',
        'original_file' => 'string',
        'image' => 'file|required',
        'hash_form' => 'string|required'
    ];

    
}
