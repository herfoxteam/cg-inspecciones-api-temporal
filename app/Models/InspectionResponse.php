<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class InspectionResponse
 * @package App\Models
 * @version August 2, 2018, 5:29 pm -05
 *
 * @property string form_response
 * @property string inspections_forms_hash
 */
class InspectionResponse extends Model
{

    public $table = 'inspection_responses';
    


    public $fillable = [
        'form_response',
        'inspections_forms_hash'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'form_response' => 'string',
        'inspections_forms_hash' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'form_response' => 'required|json',
        'inspections_forms_hash' => 'required'
    ];

    
}
