<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ObservationFormItem
 * @package App\Models
 * @version August 3, 2018, 12:17 pm -05
 *
 * @property string inspection_forms_items_code
 * @property string description
 * @property string hash_form
 */
class ObservationFormItem extends Model
{

    public $table = 'observations_forms_item';
    


    public $fillable = [
        'inspection_forms_items_code',
        'description',
        'hash_form'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'inspection_forms_items_code' => 'string',
        'description' => 'string',
        'hash_form' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'inspection_forms_items_code' => 'required|string',
        'description' => 'required',
        'hash_form' => 'required|string'
    ];

    
}
