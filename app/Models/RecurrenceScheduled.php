<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class RecurrenceScheduled
 * @package App\Models
 * @version August 3, 2018, 2:57 pm -05
 *
 * @property integer type_recurrence_id
 * @property boolean active
 * @property integer user_id
 * @property integer template_id
 * @property string|\Carbon\Carbon start_date
 */
class RecurrenceScheduled extends Model
{

    public $table = 'recurrence_schedules';
    


    public $fillable = [
        'type_recurrence_id',
        'active',
        'user_id',
        'template_id',
        'start_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'type_recurrence_id' => 'integer',
        'active' => 'boolean',
        'user_id' => 'integer',
        'template_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type_recurrence_id' => 'required|integer',
        'active' => 'required',
        'user_id' => 'required|integer',
        'template_id' => 'required'
    ];

    
}
