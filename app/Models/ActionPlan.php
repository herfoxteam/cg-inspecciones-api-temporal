<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ActionPlan
 * @package App\Models
 * @version August 3, 2018, 11:36 am -05
 *
 * @property string description
 * @property string|\Carbon\Carbon date_time_end
 * @property integer assigned_user_id
 * @property integer priorities_id
 * @property string inspections_form_item_code
 * @property boolean closed
 * @property string hash_form
 */
class ActionPlan extends Model
{

    public $table = 'action_plans';
    


    public $fillable = [
        'description',
        'date_time_end',
        'assigned_user_id',
        'priorities_id',
        'inspections_form_item_code',
        'closed',
        'hash_form'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'description' => 'string',
        'assigned_user_id' => 'integer',
        'priorities_id' => 'integer',
        'inspections_form_item_code' => 'string',
        'closed' => 'boolean',
        'hash_form' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'description' => 'required',
        'date_time_end' => 'required',
        'assigned_user_id' => 'required|integer',
        'priorities_id' => 'required|integer',
        'inspections_form_item_code' => 'required',
        'closed' => 'required|boolean',
        'hash_form' => 'required'
    ];

    
}
