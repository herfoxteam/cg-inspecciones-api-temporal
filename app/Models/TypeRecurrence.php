<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class TypeRecurrence
 * @package App\Models
 * @version August 3, 2018, 2:06 pm -05
 *
 * @property string value
 * @property string label
 */
class TypeRecurrence extends Model
{

    public $table = 'types_recurrences';
    


    public $fillable = [
        'value',
        'label'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'value' => 'string',
        'label' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'value' => 'required|string',
        'label' => 'required|string'
    ];

    
}
