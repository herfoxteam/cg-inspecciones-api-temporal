<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class TypeFormItem
 * @package App\Models
 * @version August 2, 2018, 11:50 pm -05
 *
 * @property string title
 * @property string description
 * @property string type
 * @property string icon_name
 * @property string rules
 */
class TypeFormItem extends Model
{

    public $table = 'types_form_items';
    


    public $fillable = [
        'title',
        'description',
        'type',
        'icon_name',
        'rules'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'type' => 'string',
        'icon_name' => 'string',
        'rules' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'string|required|max:40|min:3',
        'type' => 'required|max:50|string',
        'icon_name' => 'string|required',
        'rules' => 'json'
    ];

    
}
