<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class CategoryInspectionForm
 * @package App\Models
 * @version August 2, 2018, 8:08 pm -05
 *
 * @property integer location_value
 * @property string label_value
 * @property string icon_class
 * @property string template_code
 * @property integer status_categories_forms_id
 * @property string hash_category
 */
class CategoryInspectionForm extends Model
{

    public $table = 'categories_inspections_forms';
    


    public $fillable = [
        'location_value',
        'label_value',
        'icon_class',
        'template_code',
        'status_categories_forms_id',
        'hash_category'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'location_value' => 'integer',
        'label_value' => 'string',
        'icon_class' => 'string',
        'template_code' => 'string',
        'status_categories_forms_id' => 'integer',
        'hash_category' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'location_value' => 'integer|required',
        'label_value' => 'required',
        'icon_class' => 'string|min:3|max:30',
        'template_code' => 'string|required',
        'status_categories_forms_id' => 'integer|required',
        'hash_category' => 'string|max:10|min:10|required'
    ];

    
}
