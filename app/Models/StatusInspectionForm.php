<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class StatusInspectionForm
 * @package App\Models
 * @version August 2, 2018, 4:27 pm -05
 *
 * @property string title
 * @property string description
 */
class StatusInspectionForm extends Model
{

    public $table = 'status_inspection_forms';
    


    public $fillable = [
        'title',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'string|max:50|required'
    ];

    
}
