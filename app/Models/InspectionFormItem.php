<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class InspectionFormItem
 * @package App\Models
 * @version August 3, 2018, 1:30 am -05
 *
 * @property string category_hash
 * @property string label
 * @property integer type_forms_items_id
 * @property string cod_item
 * @property integer required
 * @property integer published
 */
class InspectionFormItem extends Model
{

    public $table = 'inspection_form_items';
    


    public $fillable = [
        'category_hash',
        'label',
        'type_forms_items_id',
        'cod_item',
        'required',
        'published'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'category_hash' => 'string',
        'label' => 'string',
        'type_forms_items_id' => 'integer',
        'cod_item' => 'string',
        'required' => 'integer',
        'published' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'category_hash' => 'required',
        'label' => 'required',
        'type_forms_items_id' => 'required|integer',
        'cod_item' => 'required|max:10|string',
        'required' => 'integer|required',
        'published' => 'integer|required'
    ];

    
}
