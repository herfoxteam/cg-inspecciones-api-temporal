<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class GroupMember
 * @package App\Models
 * @version August 2, 2018, 3:09 pm -05
 *
 * @property integer user_group_id
 * @property integer user_id
 */
class GroupMember extends Model
{

    public $table = 'group_members';
    


    public $fillable = [
        'user_group_id',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_group_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_group_id' => 'integer|required',
        'user_id' => 'integer|required'
    ];

    
}
