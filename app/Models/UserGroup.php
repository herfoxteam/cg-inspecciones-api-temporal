<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class UserGroup
 * @package App\Models
 * @version August 2, 2018, 3:00 pm -05
 *
 * @property string title
 * @property string description
 * @property integer administrator_id
 */
class UserGroup extends Model
{

    public $table = 'user_groups';
    


    public $fillable = [
        'title',
        'description',
        'administrator_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'administrator_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'string|required|max:60',
        'description' => 'string|max:500',
        'administrator_id' => 'integer|required'
    ];

    
}
