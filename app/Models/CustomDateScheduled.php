<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class CustomDateScheduled
 * @package App\Models
 * @version August 3, 2018, 2:27 pm -05
 *
 * @property string|\Carbon\Carbon custom_date
 * @property integer recurrence_schedules_id
 */
class CustomDateScheduled extends Model
{

    public $table = 'custom_dates_scheduled';
    


    public $fillable = [
        'custom_date',
        'recurrence_schedules_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'recurrence_schedules_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'custom_date' => 'required',
        'recurrence_schedules_id' => 'string'
    ];

    
}
