<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class OptionFormItem
 * @package App\Models
 * @version August 3, 2018, 12:37 pm -05
 *
 * @property string form_code_item
 * @property string label
 * @property string value
 */
class OptionFormItem extends Model
{

    public $table = 'options_form_items';
    


    public $fillable = [
        'form_code_item',
        'label',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'form_code_item' => 'string',
        'label' => 'string',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'form_code_item' => 'required,string',
        'label' => 'required',
        'value' => 'required'
    ];

    
}
