<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class StatusTemplateInspection
 * @package App\Models
 * @version August 2, 2018, 3:42 pm -05
 *
 * @property string title
 * @property string description
 */
class StatusTemplateInspection extends Model
{

    public $table = 'status_template_inspections';
    


    public $fillable = [
        'title',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|max:50|string'
    ];

    
}
