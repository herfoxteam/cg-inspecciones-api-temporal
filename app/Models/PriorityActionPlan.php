<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class PriorityActionPlan
 * @package App\Models
 * @version August 3, 2018, 10:36 am -05
 *
 * @property string title
 * @property string description
 * @property string icon_class
 */
class PriorityActionPlan extends Model
{

    public $table = 'priorities_actions_plans';
    


    public $fillable = [
        'title',
        'description',
        'icon_class'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'icon_class' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'string|required|min:3'
    ];

    
}
