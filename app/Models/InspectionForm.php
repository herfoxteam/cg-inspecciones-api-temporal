<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class InspectionForm
 * @package App\Models
 * @version August 2, 2018, 4:58 pm -05
 *
 * @property string hash
 * @property string template_code
 * @property integer user_id
 * @property integer status_inspection_form_id
 */
class InspectionForm extends Model
{

    public $table = 'inspection_forms';
    


    public $fillable = [
        'hash',
        'template_code',
        'user_id',
        'status_inspection_form_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'hash' => 'string',
        'template_code' => 'string',
        'user_id' => 'integer',
        'status_inspection_form_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'hash' => 'string|min:15|max:15|required',
        'template_code' => 'string|required',
        'user_id' => 'integer|required',
        'status_inspection_form_id' => 'integer|required'
    ];

    
}
