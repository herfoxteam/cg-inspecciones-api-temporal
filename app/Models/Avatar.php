<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Avatar
 * @package App\Models
 * @version August 2, 2018, 3:17 pm -05
 *
 * @property integer user_id
 * @property string thumbnail
 * @property string original
 */
class Avatar extends Model
{

    public $table = 'avatars';
    


    public $fillable = [
        'user_id',
        'thumbnail',
        'original'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'thumbnail' => 'string',
        'original' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'integer|required',
        'thumbnail' => 'string|required',
        'original' => 'string|required'
    ];

    
}
