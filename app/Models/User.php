<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class User
 * @package App\Models
 * @version August 2, 2018, 2:44 pm -05
 *
 * @property integer role_id
 * @property string username
 * @property string email
 * @property string uid
 */
class User extends Model
{

    public $table = 'users';
    


    public $fillable = [
        'role_id',
        'username',
        'email',
        'uid'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'role_id' => 'integer',
        'username' => 'string',
        'email' => 'string',
        'uid' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'string|required|max:70',
        'role_id' => 'required|integer',
        'username' => 'string|required|max:30',
        'email' => 'required|unique:users,email|max:70',
        'uid' => 'required|max:70|string'
    ];

    
}
