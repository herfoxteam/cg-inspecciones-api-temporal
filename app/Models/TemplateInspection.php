<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class TemplateInspection
 * @package App\Models
 * @version August 2, 2018, 4:10 pm -05
 *
 * @property string title
 * @property string description
 * @property integer user_id
 * @property integer status_template_id
 * @property string code
 */
class TemplateInspection extends Model
{

    public $table = 'template_inspections';
    


    public $fillable = [
        'title',
        'description',
        'user_id',
        'status_template_id',
        'code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'user_id' => 'integer',
        'status_template_id' => 'integer',
        'code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'string|required',
        'user_id' => 'string|required',
        'status_template_id' => 'integer|required',
        'code' => 'required|string|max:10'
    ];

    
}
