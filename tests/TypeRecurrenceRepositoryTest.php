<?php

use App\Models\TypeRecurrence;
use App\Repositories\TypeRecurrenceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TypeRecurrenceRepositoryTest extends TestCase
{
    use MakeTypeRecurrenceTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TypeRecurrenceRepository
     */
    protected $typeRecurrenceRepo;

    public function setUp()
    {
        parent::setUp();
        $this->typeRecurrenceRepo = App::make(TypeRecurrenceRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTypeRecurrence()
    {
        $typeRecurrence = $this->fakeTypeRecurrenceData();
        $createdTypeRecurrence = $this->typeRecurrenceRepo->create($typeRecurrence);
        $createdTypeRecurrence = $createdTypeRecurrence->toArray();
        $this->assertArrayHasKey('id', $createdTypeRecurrence);
        $this->assertNotNull($createdTypeRecurrence['id'], 'Created TypeRecurrence must have id specified');
        $this->assertNotNull(TypeRecurrence::find($createdTypeRecurrence['id']), 'TypeRecurrence with given id must be in DB');
        $this->assertModelData($typeRecurrence, $createdTypeRecurrence);
    }

    /**
     * @test read
     */
    public function testReadTypeRecurrence()
    {
        $typeRecurrence = $this->makeTypeRecurrence();
        $dbTypeRecurrence = $this->typeRecurrenceRepo->find($typeRecurrence->id);
        $dbTypeRecurrence = $dbTypeRecurrence->toArray();
        $this->assertModelData($typeRecurrence->toArray(), $dbTypeRecurrence);
    }

    /**
     * @test update
     */
    public function testUpdateTypeRecurrence()
    {
        $typeRecurrence = $this->makeTypeRecurrence();
        $fakeTypeRecurrence = $this->fakeTypeRecurrenceData();
        $updatedTypeRecurrence = $this->typeRecurrenceRepo->update($fakeTypeRecurrence, $typeRecurrence->id);
        $this->assertModelData($fakeTypeRecurrence, $updatedTypeRecurrence->toArray());
        $dbTypeRecurrence = $this->typeRecurrenceRepo->find($typeRecurrence->id);
        $this->assertModelData($fakeTypeRecurrence, $dbTypeRecurrence->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTypeRecurrence()
    {
        $typeRecurrence = $this->makeTypeRecurrence();
        $resp = $this->typeRecurrenceRepo->delete($typeRecurrence->id);
        $this->assertTrue($resp);
        $this->assertNull(TypeRecurrence::find($typeRecurrence->id), 'TypeRecurrence should not exist in DB');
    }
}
