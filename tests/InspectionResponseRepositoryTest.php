<?php

use App\Models\InspectionResponse;
use App\Repositories\InspectionResponseRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InspectionResponseRepositoryTest extends TestCase
{
    use MakeInspectionResponseTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var InspectionResponseRepository
     */
    protected $inspectionResponseRepo;

    public function setUp()
    {
        parent::setUp();
        $this->inspectionResponseRepo = App::make(InspectionResponseRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateInspectionResponse()
    {
        $inspectionResponse = $this->fakeInspectionResponseData();
        $createdInspectionResponse = $this->inspectionResponseRepo->create($inspectionResponse);
        $createdInspectionResponse = $createdInspectionResponse->toArray();
        $this->assertArrayHasKey('id', $createdInspectionResponse);
        $this->assertNotNull($createdInspectionResponse['id'], 'Created InspectionResponse must have id specified');
        $this->assertNotNull(InspectionResponse::find($createdInspectionResponse['id']), 'InspectionResponse with given id must be in DB');
        $this->assertModelData($inspectionResponse, $createdInspectionResponse);
    }

    /**
     * @test read
     */
    public function testReadInspectionResponse()
    {
        $inspectionResponse = $this->makeInspectionResponse();
        $dbInspectionResponse = $this->inspectionResponseRepo->find($inspectionResponse->id);
        $dbInspectionResponse = $dbInspectionResponse->toArray();
        $this->assertModelData($inspectionResponse->toArray(), $dbInspectionResponse);
    }

    /**
     * @test update
     */
    public function testUpdateInspectionResponse()
    {
        $inspectionResponse = $this->makeInspectionResponse();
        $fakeInspectionResponse = $this->fakeInspectionResponseData();
        $updatedInspectionResponse = $this->inspectionResponseRepo->update($fakeInspectionResponse, $inspectionResponse->id);
        $this->assertModelData($fakeInspectionResponse, $updatedInspectionResponse->toArray());
        $dbInspectionResponse = $this->inspectionResponseRepo->find($inspectionResponse->id);
        $this->assertModelData($fakeInspectionResponse, $dbInspectionResponse->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteInspectionResponse()
    {
        $inspectionResponse = $this->makeInspectionResponse();
        $resp = $this->inspectionResponseRepo->delete($inspectionResponse->id);
        $this->assertTrue($resp);
        $this->assertNull(InspectionResponse::find($inspectionResponse->id), 'InspectionResponse should not exist in DB');
    }
}
