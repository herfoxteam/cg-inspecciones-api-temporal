<?php

use App\Models\InspectionFormItem;
use App\Repositories\InspectionFormItemRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InspectionFormItemRepositoryTest extends TestCase
{
    use MakeInspectionFormItemTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var InspectionFormItemRepository
     */
    protected $inspectionFormItemRepo;

    public function setUp()
    {
        parent::setUp();
        $this->inspectionFormItemRepo = App::make(InspectionFormItemRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateInspectionFormItem()
    {
        $inspectionFormItem = $this->fakeInspectionFormItemData();
        $createdInspectionFormItem = $this->inspectionFormItemRepo->create($inspectionFormItem);
        $createdInspectionFormItem = $createdInspectionFormItem->toArray();
        $this->assertArrayHasKey('id', $createdInspectionFormItem);
        $this->assertNotNull($createdInspectionFormItem['id'], 'Created InspectionFormItem must have id specified');
        $this->assertNotNull(InspectionFormItem::find($createdInspectionFormItem['id']), 'InspectionFormItem with given id must be in DB');
        $this->assertModelData($inspectionFormItem, $createdInspectionFormItem);
    }

    /**
     * @test read
     */
    public function testReadInspectionFormItem()
    {
        $inspectionFormItem = $this->makeInspectionFormItem();
        $dbInspectionFormItem = $this->inspectionFormItemRepo->find($inspectionFormItem->id);
        $dbInspectionFormItem = $dbInspectionFormItem->toArray();
        $this->assertModelData($inspectionFormItem->toArray(), $dbInspectionFormItem);
    }

    /**
     * @test update
     */
    public function testUpdateInspectionFormItem()
    {
        $inspectionFormItem = $this->makeInspectionFormItem();
        $fakeInspectionFormItem = $this->fakeInspectionFormItemData();
        $updatedInspectionFormItem = $this->inspectionFormItemRepo->update($fakeInspectionFormItem, $inspectionFormItem->id);
        $this->assertModelData($fakeInspectionFormItem, $updatedInspectionFormItem->toArray());
        $dbInspectionFormItem = $this->inspectionFormItemRepo->find($inspectionFormItem->id);
        $this->assertModelData($fakeInspectionFormItem, $dbInspectionFormItem->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteInspectionFormItem()
    {
        $inspectionFormItem = $this->makeInspectionFormItem();
        $resp = $this->inspectionFormItemRepo->delete($inspectionFormItem->id);
        $this->assertTrue($resp);
        $this->assertNull(InspectionFormItem::find($inspectionFormItem->id), 'InspectionFormItem should not exist in DB');
    }
}
