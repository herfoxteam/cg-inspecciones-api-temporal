<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OptionFormItemApiTest extends TestCase
{
    use MakeOptionFormItemTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateOptionFormItem()
    {
        $optionFormItem = $this->fakeOptionFormItemData();
        $this->json('POST', '/api/v1/optionFormItems', $optionFormItem);

        $this->assertApiResponse($optionFormItem);
    }

    /**
     * @test
     */
    public function testReadOptionFormItem()
    {
        $optionFormItem = $this->makeOptionFormItem();
        $this->json('GET', '/api/v1/optionFormItems/'.$optionFormItem->id);

        $this->assertApiResponse($optionFormItem->toArray());
    }

    /**
     * @test
     */
    public function testUpdateOptionFormItem()
    {
        $optionFormItem = $this->makeOptionFormItem();
        $editedOptionFormItem = $this->fakeOptionFormItemData();

        $this->json('PUT', '/api/v1/optionFormItems/'.$optionFormItem->id, $editedOptionFormItem);

        $this->assertApiResponse($editedOptionFormItem);
    }

    /**
     * @test
     */
    public function testDeleteOptionFormItem()
    {
        $optionFormItem = $this->makeOptionFormItem();
        $this->json('DELETE', '/api/v1/optionFormItems/'.$optionFormItem->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/optionFormItems/'.$optionFormItem->id);

        $this->assertResponseStatus(404);
    }
}
