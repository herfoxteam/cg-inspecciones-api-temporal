<?php

use App\Models\PriorityActionPlan;
use App\Repositories\PriorityActionPlanRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PriorityActionPlanRepositoryTest extends TestCase
{
    use MakePriorityActionPlanTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PriorityActionPlanRepository
     */
    protected $priorityActionPlanRepo;

    public function setUp()
    {
        parent::setUp();
        $this->priorityActionPlanRepo = App::make(PriorityActionPlanRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePriorityActionPlan()
    {
        $priorityActionPlan = $this->fakePriorityActionPlanData();
        $createdPriorityActionPlan = $this->priorityActionPlanRepo->create($priorityActionPlan);
        $createdPriorityActionPlan = $createdPriorityActionPlan->toArray();
        $this->assertArrayHasKey('id', $createdPriorityActionPlan);
        $this->assertNotNull($createdPriorityActionPlan['id'], 'Created PriorityActionPlan must have id specified');
        $this->assertNotNull(PriorityActionPlan::find($createdPriorityActionPlan['id']), 'PriorityActionPlan with given id must be in DB');
        $this->assertModelData($priorityActionPlan, $createdPriorityActionPlan);
    }

    /**
     * @test read
     */
    public function testReadPriorityActionPlan()
    {
        $priorityActionPlan = $this->makePriorityActionPlan();
        $dbPriorityActionPlan = $this->priorityActionPlanRepo->find($priorityActionPlan->id);
        $dbPriorityActionPlan = $dbPriorityActionPlan->toArray();
        $this->assertModelData($priorityActionPlan->toArray(), $dbPriorityActionPlan);
    }

    /**
     * @test update
     */
    public function testUpdatePriorityActionPlan()
    {
        $priorityActionPlan = $this->makePriorityActionPlan();
        $fakePriorityActionPlan = $this->fakePriorityActionPlanData();
        $updatedPriorityActionPlan = $this->priorityActionPlanRepo->update($fakePriorityActionPlan, $priorityActionPlan->id);
        $this->assertModelData($fakePriorityActionPlan, $updatedPriorityActionPlan->toArray());
        $dbPriorityActionPlan = $this->priorityActionPlanRepo->find($priorityActionPlan->id);
        $this->assertModelData($fakePriorityActionPlan, $dbPriorityActionPlan->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePriorityActionPlan()
    {
        $priorityActionPlan = $this->makePriorityActionPlan();
        $resp = $this->priorityActionPlanRepo->delete($priorityActionPlan->id);
        $this->assertTrue($resp);
        $this->assertNull(PriorityActionPlan::find($priorityActionPlan->id), 'PriorityActionPlan should not exist in DB');
    }
}
