<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TemplateInspectionApiTest extends TestCase
{
    use MakeTemplateInspectionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTemplateInspection()
    {
        $templateInspection = $this->fakeTemplateInspectionData();
        $this->json('POST', '/api/v1/templateInspections', $templateInspection);

        $this->assertApiResponse($templateInspection);
    }

    /**
     * @test
     */
    public function testReadTemplateInspection()
    {
        $templateInspection = $this->makeTemplateInspection();
        $this->json('GET', '/api/v1/templateInspections/'.$templateInspection->id);

        $this->assertApiResponse($templateInspection->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTemplateInspection()
    {
        $templateInspection = $this->makeTemplateInspection();
        $editedTemplateInspection = $this->fakeTemplateInspectionData();

        $this->json('PUT', '/api/v1/templateInspections/'.$templateInspection->id, $editedTemplateInspection);

        $this->assertApiResponse($editedTemplateInspection);
    }

    /**
     * @test
     */
    public function testDeleteTemplateInspection()
    {
        $templateInspection = $this->makeTemplateInspection();
        $this->json('DELETE', '/api/v1/templateInspections/'.$templateInspection->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/templateInspections/'.$templateInspection->id);

        $this->assertResponseStatus(404);
    }
}
