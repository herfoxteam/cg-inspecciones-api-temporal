<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InspectionFormItemApiTest extends TestCase
{
    use MakeInspectionFormItemTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateInspectionFormItem()
    {
        $inspectionFormItem = $this->fakeInspectionFormItemData();
        $this->json('POST', '/api/v1/inspectionFormItems', $inspectionFormItem);

        $this->assertApiResponse($inspectionFormItem);
    }

    /**
     * @test
     */
    public function testReadInspectionFormItem()
    {
        $inspectionFormItem = $this->makeInspectionFormItem();
        $this->json('GET', '/api/v1/inspectionFormItems/'.$inspectionFormItem->id);

        $this->assertApiResponse($inspectionFormItem->toArray());
    }

    /**
     * @test
     */
    public function testUpdateInspectionFormItem()
    {
        $inspectionFormItem = $this->makeInspectionFormItem();
        $editedInspectionFormItem = $this->fakeInspectionFormItemData();

        $this->json('PUT', '/api/v1/inspectionFormItems/'.$inspectionFormItem->id, $editedInspectionFormItem);

        $this->assertApiResponse($editedInspectionFormItem);
    }

    /**
     * @test
     */
    public function testDeleteInspectionFormItem()
    {
        $inspectionFormItem = $this->makeInspectionFormItem();
        $this->json('DELETE', '/api/v1/inspectionFormItems/'.$inspectionFormItem->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/inspectionFormItems/'.$inspectionFormItem->id);

        $this->assertResponseStatus(404);
    }
}
