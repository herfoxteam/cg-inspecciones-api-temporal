<?php

use App\Models\CustomDateScheduled;
use App\Repositories\CustomDateScheduledRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CustomDateScheduledRepositoryTest extends TestCase
{
    use MakeCustomDateScheduledTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CustomDateScheduledRepository
     */
    protected $customDateScheduledRepo;

    public function setUp()
    {
        parent::setUp();
        $this->customDateScheduledRepo = App::make(CustomDateScheduledRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCustomDateScheduled()
    {
        $customDateScheduled = $this->fakeCustomDateScheduledData();
        $createdCustomDateScheduled = $this->customDateScheduledRepo->create($customDateScheduled);
        $createdCustomDateScheduled = $createdCustomDateScheduled->toArray();
        $this->assertArrayHasKey('id', $createdCustomDateScheduled);
        $this->assertNotNull($createdCustomDateScheduled['id'], 'Created CustomDateScheduled must have id specified');
        $this->assertNotNull(CustomDateScheduled::find($createdCustomDateScheduled['id']), 'CustomDateScheduled with given id must be in DB');
        $this->assertModelData($customDateScheduled, $createdCustomDateScheduled);
    }

    /**
     * @test read
     */
    public function testReadCustomDateScheduled()
    {
        $customDateScheduled = $this->makeCustomDateScheduled();
        $dbCustomDateScheduled = $this->customDateScheduledRepo->find($customDateScheduled->id);
        $dbCustomDateScheduled = $dbCustomDateScheduled->toArray();
        $this->assertModelData($customDateScheduled->toArray(), $dbCustomDateScheduled);
    }

    /**
     * @test update
     */
    public function testUpdateCustomDateScheduled()
    {
        $customDateScheduled = $this->makeCustomDateScheduled();
        $fakeCustomDateScheduled = $this->fakeCustomDateScheduledData();
        $updatedCustomDateScheduled = $this->customDateScheduledRepo->update($fakeCustomDateScheduled, $customDateScheduled->id);
        $this->assertModelData($fakeCustomDateScheduled, $updatedCustomDateScheduled->toArray());
        $dbCustomDateScheduled = $this->customDateScheduledRepo->find($customDateScheduled->id);
        $this->assertModelData($fakeCustomDateScheduled, $dbCustomDateScheduled->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCustomDateScheduled()
    {
        $customDateScheduled = $this->makeCustomDateScheduled();
        $resp = $this->customDateScheduledRepo->delete($customDateScheduled->id);
        $this->assertTrue($resp);
        $this->assertNull(CustomDateScheduled::find($customDateScheduled->id), 'CustomDateScheduled should not exist in DB');
    }
}
