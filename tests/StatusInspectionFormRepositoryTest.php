<?php

use App\Models\StatusInspectionForm;
use App\Repositories\StatusInspectionFormRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StatusInspectionFormRepositoryTest extends TestCase
{
    use MakeStatusInspectionFormTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StatusInspectionFormRepository
     */
    protected $statusInspectionFormRepo;

    public function setUp()
    {
        parent::setUp();
        $this->statusInspectionFormRepo = App::make(StatusInspectionFormRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStatusInspectionForm()
    {
        $statusInspectionForm = $this->fakeStatusInspectionFormData();
        $createdStatusInspectionForm = $this->statusInspectionFormRepo->create($statusInspectionForm);
        $createdStatusInspectionForm = $createdStatusInspectionForm->toArray();
        $this->assertArrayHasKey('id', $createdStatusInspectionForm);
        $this->assertNotNull($createdStatusInspectionForm['id'], 'Created StatusInspectionForm must have id specified');
        $this->assertNotNull(StatusInspectionForm::find($createdStatusInspectionForm['id']), 'StatusInspectionForm with given id must be in DB');
        $this->assertModelData($statusInspectionForm, $createdStatusInspectionForm);
    }

    /**
     * @test read
     */
    public function testReadStatusInspectionForm()
    {
        $statusInspectionForm = $this->makeStatusInspectionForm();
        $dbStatusInspectionForm = $this->statusInspectionFormRepo->find($statusInspectionForm->id);
        $dbStatusInspectionForm = $dbStatusInspectionForm->toArray();
        $this->assertModelData($statusInspectionForm->toArray(), $dbStatusInspectionForm);
    }

    /**
     * @test update
     */
    public function testUpdateStatusInspectionForm()
    {
        $statusInspectionForm = $this->makeStatusInspectionForm();
        $fakeStatusInspectionForm = $this->fakeStatusInspectionFormData();
        $updatedStatusInspectionForm = $this->statusInspectionFormRepo->update($fakeStatusInspectionForm, $statusInspectionForm->id);
        $this->assertModelData($fakeStatusInspectionForm, $updatedStatusInspectionForm->toArray());
        $dbStatusInspectionForm = $this->statusInspectionFormRepo->find($statusInspectionForm->id);
        $this->assertModelData($fakeStatusInspectionForm, $dbStatusInspectionForm->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStatusInspectionForm()
    {
        $statusInspectionForm = $this->makeStatusInspectionForm();
        $resp = $this->statusInspectionFormRepo->delete($statusInspectionForm->id);
        $this->assertTrue($resp);
        $this->assertNull(StatusInspectionForm::find($statusInspectionForm->id), 'StatusInspectionForm should not exist in DB');
    }
}
