<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AvatarApiTest extends TestCase
{
    use MakeAvatarTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAvatar()
    {
        $avatar = $this->fakeAvatarData();
        $this->json('POST', '/api/v1/avatars', $avatar);

        $this->assertApiResponse($avatar);
    }

    /**
     * @test
     */
    public function testReadAvatar()
    {
        $avatar = $this->makeAvatar();
        $this->json('GET', '/api/v1/avatars/'.$avatar->id);

        $this->assertApiResponse($avatar->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAvatar()
    {
        $avatar = $this->makeAvatar();
        $editedAvatar = $this->fakeAvatarData();

        $this->json('PUT', '/api/v1/avatars/'.$avatar->id, $editedAvatar);

        $this->assertApiResponse($editedAvatar);
    }

    /**
     * @test
     */
    public function testDeleteAvatar()
    {
        $avatar = $this->makeAvatar();
        $this->json('DELETE', '/api/v1/avatars/'.$avatar->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/avatars/'.$avatar->id);

        $this->assertResponseStatus(404);
    }
}
