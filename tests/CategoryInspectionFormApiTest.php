<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoryInspectionFormApiTest extends TestCase
{
    use MakeCategoryInspectionFormTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCategoryInspectionForm()
    {
        $categoryInspectionForm = $this->fakeCategoryInspectionFormData();
        $this->json('POST', '/api/v1/categoryInspectionForms', $categoryInspectionForm);

        $this->assertApiResponse($categoryInspectionForm);
    }

    /**
     * @test
     */
    public function testReadCategoryInspectionForm()
    {
        $categoryInspectionForm = $this->makeCategoryInspectionForm();
        $this->json('GET', '/api/v1/categoryInspectionForms/'.$categoryInspectionForm->id);

        $this->assertApiResponse($categoryInspectionForm->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCategoryInspectionForm()
    {
        $categoryInspectionForm = $this->makeCategoryInspectionForm();
        $editedCategoryInspectionForm = $this->fakeCategoryInspectionFormData();

        $this->json('PUT', '/api/v1/categoryInspectionForms/'.$categoryInspectionForm->id, $editedCategoryInspectionForm);

        $this->assertApiResponse($editedCategoryInspectionForm);
    }

    /**
     * @test
     */
    public function testDeleteCategoryInspectionForm()
    {
        $categoryInspectionForm = $this->makeCategoryInspectionForm();
        $this->json('DELETE', '/api/v1/categoryInspectionForms/'.$categoryInspectionForm->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/categoryInspectionForms/'.$categoryInspectionForm->id);

        $this->assertResponseStatus(404);
    }
}
