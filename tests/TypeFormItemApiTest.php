<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TypeFormItemApiTest extends TestCase
{
    use MakeTypeFormItemTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTypeFormItem()
    {
        $typeFormItem = $this->fakeTypeFormItemData();
        $this->json('POST', '/api/v1/typeFormItems', $typeFormItem);

        $this->assertApiResponse($typeFormItem);
    }

    /**
     * @test
     */
    public function testReadTypeFormItem()
    {
        $typeFormItem = $this->makeTypeFormItem();
        $this->json('GET', '/api/v1/typeFormItems/'.$typeFormItem->id);

        $this->assertApiResponse($typeFormItem->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTypeFormItem()
    {
        $typeFormItem = $this->makeTypeFormItem();
        $editedTypeFormItem = $this->fakeTypeFormItemData();

        $this->json('PUT', '/api/v1/typeFormItems/'.$typeFormItem->id, $editedTypeFormItem);

        $this->assertApiResponse($editedTypeFormItem);
    }

    /**
     * @test
     */
    public function testDeleteTypeFormItem()
    {
        $typeFormItem = $this->makeTypeFormItem();
        $this->json('DELETE', '/api/v1/typeFormItems/'.$typeFormItem->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/typeFormItems/'.$typeFormItem->id);

        $this->assertResponseStatus(404);
    }
}
