<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CustomDateScheduledApiTest extends TestCase
{
    use MakeCustomDateScheduledTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCustomDateScheduled()
    {
        $customDateScheduled = $this->fakeCustomDateScheduledData();
        $this->json('POST', '/api/v1/customDateScheduleds', $customDateScheduled);

        $this->assertApiResponse($customDateScheduled);
    }

    /**
     * @test
     */
    public function testReadCustomDateScheduled()
    {
        $customDateScheduled = $this->makeCustomDateScheduled();
        $this->json('GET', '/api/v1/customDateScheduleds/'.$customDateScheduled->id);

        $this->assertApiResponse($customDateScheduled->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCustomDateScheduled()
    {
        $customDateScheduled = $this->makeCustomDateScheduled();
        $editedCustomDateScheduled = $this->fakeCustomDateScheduledData();

        $this->json('PUT', '/api/v1/customDateScheduleds/'.$customDateScheduled->id, $editedCustomDateScheduled);

        $this->assertApiResponse($editedCustomDateScheduled);
    }

    /**
     * @test
     */
    public function testDeleteCustomDateScheduled()
    {
        $customDateScheduled = $this->makeCustomDateScheduled();
        $this->json('DELETE', '/api/v1/customDateScheduleds/'.$customDateScheduled->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/customDateScheduleds/'.$customDateScheduled->id);

        $this->assertResponseStatus(404);
    }
}
