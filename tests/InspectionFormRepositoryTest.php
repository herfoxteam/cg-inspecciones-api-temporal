<?php

use App\Models\InspectionForm;
use App\Repositories\InspectionFormRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InspectionFormRepositoryTest extends TestCase
{
    use MakeInspectionFormTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var InspectionFormRepository
     */
    protected $inspectionFormRepo;

    public function setUp()
    {
        parent::setUp();
        $this->inspectionFormRepo = App::make(InspectionFormRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateInspectionForm()
    {
        $inspectionForm = $this->fakeInspectionFormData();
        $createdInspectionForm = $this->inspectionFormRepo->create($inspectionForm);
        $createdInspectionForm = $createdInspectionForm->toArray();
        $this->assertArrayHasKey('id', $createdInspectionForm);
        $this->assertNotNull($createdInspectionForm['id'], 'Created InspectionForm must have id specified');
        $this->assertNotNull(InspectionForm::find($createdInspectionForm['id']), 'InspectionForm with given id must be in DB');
        $this->assertModelData($inspectionForm, $createdInspectionForm);
    }

    /**
     * @test read
     */
    public function testReadInspectionForm()
    {
        $inspectionForm = $this->makeInspectionForm();
        $dbInspectionForm = $this->inspectionFormRepo->find($inspectionForm->id);
        $dbInspectionForm = $dbInspectionForm->toArray();
        $this->assertModelData($inspectionForm->toArray(), $dbInspectionForm);
    }

    /**
     * @test update
     */
    public function testUpdateInspectionForm()
    {
        $inspectionForm = $this->makeInspectionForm();
        $fakeInspectionForm = $this->fakeInspectionFormData();
        $updatedInspectionForm = $this->inspectionFormRepo->update($fakeInspectionForm, $inspectionForm->id);
        $this->assertModelData($fakeInspectionForm, $updatedInspectionForm->toArray());
        $dbInspectionForm = $this->inspectionFormRepo->find($inspectionForm->id);
        $this->assertModelData($fakeInspectionForm, $dbInspectionForm->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteInspectionForm()
    {
        $inspectionForm = $this->makeInspectionForm();
        $resp = $this->inspectionFormRepo->delete($inspectionForm->id);
        $this->assertTrue($resp);
        $this->assertNull(InspectionForm::find($inspectionForm->id), 'InspectionForm should not exist in DB');
    }
}
