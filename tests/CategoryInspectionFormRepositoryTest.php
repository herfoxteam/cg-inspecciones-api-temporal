<?php

use App\Models\CategoryInspectionForm;
use App\Repositories\CategoryInspectionFormRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoryInspectionFormRepositoryTest extends TestCase
{
    use MakeCategoryInspectionFormTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CategoryInspectionFormRepository
     */
    protected $categoryInspectionFormRepo;

    public function setUp()
    {
        parent::setUp();
        $this->categoryInspectionFormRepo = App::make(CategoryInspectionFormRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCategoryInspectionForm()
    {
        $categoryInspectionForm = $this->fakeCategoryInspectionFormData();
        $createdCategoryInspectionForm = $this->categoryInspectionFormRepo->create($categoryInspectionForm);
        $createdCategoryInspectionForm = $createdCategoryInspectionForm->toArray();
        $this->assertArrayHasKey('id', $createdCategoryInspectionForm);
        $this->assertNotNull($createdCategoryInspectionForm['id'], 'Created CategoryInspectionForm must have id specified');
        $this->assertNotNull(CategoryInspectionForm::find($createdCategoryInspectionForm['id']), 'CategoryInspectionForm with given id must be in DB');
        $this->assertModelData($categoryInspectionForm, $createdCategoryInspectionForm);
    }

    /**
     * @test read
     */
    public function testReadCategoryInspectionForm()
    {
        $categoryInspectionForm = $this->makeCategoryInspectionForm();
        $dbCategoryInspectionForm = $this->categoryInspectionFormRepo->find($categoryInspectionForm->id);
        $dbCategoryInspectionForm = $dbCategoryInspectionForm->toArray();
        $this->assertModelData($categoryInspectionForm->toArray(), $dbCategoryInspectionForm);
    }

    /**
     * @test update
     */
    public function testUpdateCategoryInspectionForm()
    {
        $categoryInspectionForm = $this->makeCategoryInspectionForm();
        $fakeCategoryInspectionForm = $this->fakeCategoryInspectionFormData();
        $updatedCategoryInspectionForm = $this->categoryInspectionFormRepo->update($fakeCategoryInspectionForm, $categoryInspectionForm->id);
        $this->assertModelData($fakeCategoryInspectionForm, $updatedCategoryInspectionForm->toArray());
        $dbCategoryInspectionForm = $this->categoryInspectionFormRepo->find($categoryInspectionForm->id);
        $this->assertModelData($fakeCategoryInspectionForm, $dbCategoryInspectionForm->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCategoryInspectionForm()
    {
        $categoryInspectionForm = $this->makeCategoryInspectionForm();
        $resp = $this->categoryInspectionFormRepo->delete($categoryInspectionForm->id);
        $this->assertTrue($resp);
        $this->assertNull(CategoryInspectionForm::find($categoryInspectionForm->id), 'CategoryInspectionForm should not exist in DB');
    }
}
