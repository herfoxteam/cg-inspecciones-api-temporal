<?php

use App\Models\StatusCategoryForm;
use App\Repositories\StatusCategoryFormRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StatusCategoryFormRepositoryTest extends TestCase
{
    use MakeStatusCategoryFormTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StatusCategoryFormRepository
     */
    protected $statusCategoryFormRepo;

    public function setUp()
    {
        parent::setUp();
        $this->statusCategoryFormRepo = App::make(StatusCategoryFormRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStatusCategoryForm()
    {
        $statusCategoryForm = $this->fakeStatusCategoryFormData();
        $createdStatusCategoryForm = $this->statusCategoryFormRepo->create($statusCategoryForm);
        $createdStatusCategoryForm = $createdStatusCategoryForm->toArray();
        $this->assertArrayHasKey('id', $createdStatusCategoryForm);
        $this->assertNotNull($createdStatusCategoryForm['id'], 'Created StatusCategoryForm must have id specified');
        $this->assertNotNull(StatusCategoryForm::find($createdStatusCategoryForm['id']), 'StatusCategoryForm with given id must be in DB');
        $this->assertModelData($statusCategoryForm, $createdStatusCategoryForm);
    }

    /**
     * @test read
     */
    public function testReadStatusCategoryForm()
    {
        $statusCategoryForm = $this->makeStatusCategoryForm();
        $dbStatusCategoryForm = $this->statusCategoryFormRepo->find($statusCategoryForm->id);
        $dbStatusCategoryForm = $dbStatusCategoryForm->toArray();
        $this->assertModelData($statusCategoryForm->toArray(), $dbStatusCategoryForm);
    }

    /**
     * @test update
     */
    public function testUpdateStatusCategoryForm()
    {
        $statusCategoryForm = $this->makeStatusCategoryForm();
        $fakeStatusCategoryForm = $this->fakeStatusCategoryFormData();
        $updatedStatusCategoryForm = $this->statusCategoryFormRepo->update($fakeStatusCategoryForm, $statusCategoryForm->id);
        $this->assertModelData($fakeStatusCategoryForm, $updatedStatusCategoryForm->toArray());
        $dbStatusCategoryForm = $this->statusCategoryFormRepo->find($statusCategoryForm->id);
        $this->assertModelData($fakeStatusCategoryForm, $dbStatusCategoryForm->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStatusCategoryForm()
    {
        $statusCategoryForm = $this->makeStatusCategoryForm();
        $resp = $this->statusCategoryFormRepo->delete($statusCategoryForm->id);
        $this->assertTrue($resp);
        $this->assertNull(StatusCategoryForm::find($statusCategoryForm->id), 'StatusCategoryForm should not exist in DB');
    }
}
