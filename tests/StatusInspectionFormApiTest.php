<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StatusInspectionFormApiTest extends TestCase
{
    use MakeStatusInspectionFormTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStatusInspectionForm()
    {
        $statusInspectionForm = $this->fakeStatusInspectionFormData();
        $this->json('POST', '/api/v1/statusInspectionForms', $statusInspectionForm);

        $this->assertApiResponse($statusInspectionForm);
    }

    /**
     * @test
     */
    public function testReadStatusInspectionForm()
    {
        $statusInspectionForm = $this->makeStatusInspectionForm();
        $this->json('GET', '/api/v1/statusInspectionForms/'.$statusInspectionForm->id);

        $this->assertApiResponse($statusInspectionForm->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStatusInspectionForm()
    {
        $statusInspectionForm = $this->makeStatusInspectionForm();
        $editedStatusInspectionForm = $this->fakeStatusInspectionFormData();

        $this->json('PUT', '/api/v1/statusInspectionForms/'.$statusInspectionForm->id, $editedStatusInspectionForm);

        $this->assertApiResponse($editedStatusInspectionForm);
    }

    /**
     * @test
     */
    public function testDeleteStatusInspectionForm()
    {
        $statusInspectionForm = $this->makeStatusInspectionForm();
        $this->json('DELETE', '/api/v1/statusInspectionForms/'.$statusInspectionForm->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/statusInspectionForms/'.$statusInspectionForm->id);

        $this->assertResponseStatus(404);
    }
}
