<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ObservationFormItemApiTest extends TestCase
{
    use MakeObservationFormItemTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateObservationFormItem()
    {
        $observationFormItem = $this->fakeObservationFormItemData();
        $this->json('POST', '/api/v1/observationFormItems', $observationFormItem);

        $this->assertApiResponse($observationFormItem);
    }

    /**
     * @test
     */
    public function testReadObservationFormItem()
    {
        $observationFormItem = $this->makeObservationFormItem();
        $this->json('GET', '/api/v1/observationFormItems/'.$observationFormItem->id);

        $this->assertApiResponse($observationFormItem->toArray());
    }

    /**
     * @test
     */
    public function testUpdateObservationFormItem()
    {
        $observationFormItem = $this->makeObservationFormItem();
        $editedObservationFormItem = $this->fakeObservationFormItemData();

        $this->json('PUT', '/api/v1/observationFormItems/'.$observationFormItem->id, $editedObservationFormItem);

        $this->assertApiResponse($editedObservationFormItem);
    }

    /**
     * @test
     */
    public function testDeleteObservationFormItem()
    {
        $observationFormItem = $this->makeObservationFormItem();
        $this->json('DELETE', '/api/v1/observationFormItems/'.$observationFormItem->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/observationFormItems/'.$observationFormItem->id);

        $this->assertResponseStatus(404);
    }
}
