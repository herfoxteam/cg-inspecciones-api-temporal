<?php

use App\Models\OptionFormItem;
use App\Repositories\OptionFormItemRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OptionFormItemRepositoryTest extends TestCase
{
    use MakeOptionFormItemTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var OptionFormItemRepository
     */
    protected $optionFormItemRepo;

    public function setUp()
    {
        parent::setUp();
        $this->optionFormItemRepo = App::make(OptionFormItemRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateOptionFormItem()
    {
        $optionFormItem = $this->fakeOptionFormItemData();
        $createdOptionFormItem = $this->optionFormItemRepo->create($optionFormItem);
        $createdOptionFormItem = $createdOptionFormItem->toArray();
        $this->assertArrayHasKey('id', $createdOptionFormItem);
        $this->assertNotNull($createdOptionFormItem['id'], 'Created OptionFormItem must have id specified');
        $this->assertNotNull(OptionFormItem::find($createdOptionFormItem['id']), 'OptionFormItem with given id must be in DB');
        $this->assertModelData($optionFormItem, $createdOptionFormItem);
    }

    /**
     * @test read
     */
    public function testReadOptionFormItem()
    {
        $optionFormItem = $this->makeOptionFormItem();
        $dbOptionFormItem = $this->optionFormItemRepo->find($optionFormItem->id);
        $dbOptionFormItem = $dbOptionFormItem->toArray();
        $this->assertModelData($optionFormItem->toArray(), $dbOptionFormItem);
    }

    /**
     * @test update
     */
    public function testUpdateOptionFormItem()
    {
        $optionFormItem = $this->makeOptionFormItem();
        $fakeOptionFormItem = $this->fakeOptionFormItemData();
        $updatedOptionFormItem = $this->optionFormItemRepo->update($fakeOptionFormItem, $optionFormItem->id);
        $this->assertModelData($fakeOptionFormItem, $updatedOptionFormItem->toArray());
        $dbOptionFormItem = $this->optionFormItemRepo->find($optionFormItem->id);
        $this->assertModelData($fakeOptionFormItem, $dbOptionFormItem->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteOptionFormItem()
    {
        $optionFormItem = $this->makeOptionFormItem();
        $resp = $this->optionFormItemRepo->delete($optionFormItem->id);
        $this->assertTrue($resp);
        $this->assertNull(OptionFormItem::find($optionFormItem->id), 'OptionFormItem should not exist in DB');
    }
}
