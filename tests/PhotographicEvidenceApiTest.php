<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PhotographicEvidenceApiTest extends TestCase
{
    use MakePhotographicEvidenceTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePhotographicEvidence()
    {
        $photographicEvidence = $this->fakePhotographicEvidenceData();
        $this->json('POST', '/api/v1/photographicEvidences', $photographicEvidence);

        $this->assertApiResponse($photographicEvidence);
    }

    /**
     * @test
     */
    public function testReadPhotographicEvidence()
    {
        $photographicEvidence = $this->makePhotographicEvidence();
        $this->json('GET', '/api/v1/photographicEvidences/'.$photographicEvidence->id);

        $this->assertApiResponse($photographicEvidence->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePhotographicEvidence()
    {
        $photographicEvidence = $this->makePhotographicEvidence();
        $editedPhotographicEvidence = $this->fakePhotographicEvidenceData();

        $this->json('PUT', '/api/v1/photographicEvidences/'.$photographicEvidence->id, $editedPhotographicEvidence);

        $this->assertApiResponse($editedPhotographicEvidence);
    }

    /**
     * @test
     */
    public function testDeletePhotographicEvidence()
    {
        $photographicEvidence = $this->makePhotographicEvidence();
        $this->json('DELETE', '/api/v1/photographicEvidences/'.$photographicEvidence->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/photographicEvidences/'.$photographicEvidence->id);

        $this->assertResponseStatus(404);
    }
}
