<?php

use App\Models\ActionPlan;
use App\Repositories\ActionPlanRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActionPlanRepositoryTest extends TestCase
{
    use MakeActionPlanTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ActionPlanRepository
     */
    protected $actionPlanRepo;

    public function setUp()
    {
        parent::setUp();
        $this->actionPlanRepo = App::make(ActionPlanRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateActionPlan()
    {
        $actionPlan = $this->fakeActionPlanData();
        $createdActionPlan = $this->actionPlanRepo->create($actionPlan);
        $createdActionPlan = $createdActionPlan->toArray();
        $this->assertArrayHasKey('id', $createdActionPlan);
        $this->assertNotNull($createdActionPlan['id'], 'Created ActionPlan must have id specified');
        $this->assertNotNull(ActionPlan::find($createdActionPlan['id']), 'ActionPlan with given id must be in DB');
        $this->assertModelData($actionPlan, $createdActionPlan);
    }

    /**
     * @test read
     */
    public function testReadActionPlan()
    {
        $actionPlan = $this->makeActionPlan();
        $dbActionPlan = $this->actionPlanRepo->find($actionPlan->id);
        $dbActionPlan = $dbActionPlan->toArray();
        $this->assertModelData($actionPlan->toArray(), $dbActionPlan);
    }

    /**
     * @test update
     */
    public function testUpdateActionPlan()
    {
        $actionPlan = $this->makeActionPlan();
        $fakeActionPlan = $this->fakeActionPlanData();
        $updatedActionPlan = $this->actionPlanRepo->update($fakeActionPlan, $actionPlan->id);
        $this->assertModelData($fakeActionPlan, $updatedActionPlan->toArray());
        $dbActionPlan = $this->actionPlanRepo->find($actionPlan->id);
        $this->assertModelData($fakeActionPlan, $dbActionPlan->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteActionPlan()
    {
        $actionPlan = $this->makeActionPlan();
        $resp = $this->actionPlanRepo->delete($actionPlan->id);
        $this->assertTrue($resp);
        $this->assertNull(ActionPlan::find($actionPlan->id), 'ActionPlan should not exist in DB');
    }
}
