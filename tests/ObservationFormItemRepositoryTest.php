<?php

use App\Models\ObservationFormItem;
use App\Repositories\ObservationFormItemRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ObservationFormItemRepositoryTest extends TestCase
{
    use MakeObservationFormItemTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ObservationFormItemRepository
     */
    protected $observationFormItemRepo;

    public function setUp()
    {
        parent::setUp();
        $this->observationFormItemRepo = App::make(ObservationFormItemRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateObservationFormItem()
    {
        $observationFormItem = $this->fakeObservationFormItemData();
        $createdObservationFormItem = $this->observationFormItemRepo->create($observationFormItem);
        $createdObservationFormItem = $createdObservationFormItem->toArray();
        $this->assertArrayHasKey('id', $createdObservationFormItem);
        $this->assertNotNull($createdObservationFormItem['id'], 'Created ObservationFormItem must have id specified');
        $this->assertNotNull(ObservationFormItem::find($createdObservationFormItem['id']), 'ObservationFormItem with given id must be in DB');
        $this->assertModelData($observationFormItem, $createdObservationFormItem);
    }

    /**
     * @test read
     */
    public function testReadObservationFormItem()
    {
        $observationFormItem = $this->makeObservationFormItem();
        $dbObservationFormItem = $this->observationFormItemRepo->find($observationFormItem->id);
        $dbObservationFormItem = $dbObservationFormItem->toArray();
        $this->assertModelData($observationFormItem->toArray(), $dbObservationFormItem);
    }

    /**
     * @test update
     */
    public function testUpdateObservationFormItem()
    {
        $observationFormItem = $this->makeObservationFormItem();
        $fakeObservationFormItem = $this->fakeObservationFormItemData();
        $updatedObservationFormItem = $this->observationFormItemRepo->update($fakeObservationFormItem, $observationFormItem->id);
        $this->assertModelData($fakeObservationFormItem, $updatedObservationFormItem->toArray());
        $dbObservationFormItem = $this->observationFormItemRepo->find($observationFormItem->id);
        $this->assertModelData($fakeObservationFormItem, $dbObservationFormItem->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteObservationFormItem()
    {
        $observationFormItem = $this->makeObservationFormItem();
        $resp = $this->observationFormItemRepo->delete($observationFormItem->id);
        $this->assertTrue($resp);
        $this->assertNull(ObservationFormItem::find($observationFormItem->id), 'ObservationFormItem should not exist in DB');
    }
}
