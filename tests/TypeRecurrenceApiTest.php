<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TypeRecurrenceApiTest extends TestCase
{
    use MakeTypeRecurrenceTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTypeRecurrence()
    {
        $typeRecurrence = $this->fakeTypeRecurrenceData();
        $this->json('POST', '/api/v1/typeRecurrences', $typeRecurrence);

        $this->assertApiResponse($typeRecurrence);
    }

    /**
     * @test
     */
    public function testReadTypeRecurrence()
    {
        $typeRecurrence = $this->makeTypeRecurrence();
        $this->json('GET', '/api/v1/typeRecurrences/'.$typeRecurrence->id);

        $this->assertApiResponse($typeRecurrence->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTypeRecurrence()
    {
        $typeRecurrence = $this->makeTypeRecurrence();
        $editedTypeRecurrence = $this->fakeTypeRecurrenceData();

        $this->json('PUT', '/api/v1/typeRecurrences/'.$typeRecurrence->id, $editedTypeRecurrence);

        $this->assertApiResponse($editedTypeRecurrence);
    }

    /**
     * @test
     */
    public function testDeleteTypeRecurrence()
    {
        $typeRecurrence = $this->makeTypeRecurrence();
        $this->json('DELETE', '/api/v1/typeRecurrences/'.$typeRecurrence->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/typeRecurrences/'.$typeRecurrence->id);

        $this->assertResponseStatus(404);
    }
}
