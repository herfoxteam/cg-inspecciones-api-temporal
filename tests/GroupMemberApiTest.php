<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GroupMemberApiTest extends TestCase
{
    use MakeGroupMemberTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateGroupMember()
    {
        $groupMember = $this->fakeGroupMemberData();
        $this->json('POST', '/api/v1/groupMembers', $groupMember);

        $this->assertApiResponse($groupMember);
    }

    /**
     * @test
     */
    public function testReadGroupMember()
    {
        $groupMember = $this->makeGroupMember();
        $this->json('GET', '/api/v1/groupMembers/'.$groupMember->id);

        $this->assertApiResponse($groupMember->toArray());
    }

    /**
     * @test
     */
    public function testUpdateGroupMember()
    {
        $groupMember = $this->makeGroupMember();
        $editedGroupMember = $this->fakeGroupMemberData();

        $this->json('PUT', '/api/v1/groupMembers/'.$groupMember->id, $editedGroupMember);

        $this->assertApiResponse($editedGroupMember);
    }

    /**
     * @test
     */
    public function testDeleteGroupMember()
    {
        $groupMember = $this->makeGroupMember();
        $this->json('DELETE', '/api/v1/groupMembers/'.$groupMember->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/groupMembers/'.$groupMember->id);

        $this->assertResponseStatus(404);
    }
}
