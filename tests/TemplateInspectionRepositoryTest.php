<?php

use App\Models\TemplateInspection;
use App\Repositories\TemplateInspectionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TemplateInspectionRepositoryTest extends TestCase
{
    use MakeTemplateInspectionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TemplateInspectionRepository
     */
    protected $templateInspectionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->templateInspectionRepo = App::make(TemplateInspectionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTemplateInspection()
    {
        $templateInspection = $this->fakeTemplateInspectionData();
        $createdTemplateInspection = $this->templateInspectionRepo->create($templateInspection);
        $createdTemplateInspection = $createdTemplateInspection->toArray();
        $this->assertArrayHasKey('id', $createdTemplateInspection);
        $this->assertNotNull($createdTemplateInspection['id'], 'Created TemplateInspection must have id specified');
        $this->assertNotNull(TemplateInspection::find($createdTemplateInspection['id']), 'TemplateInspection with given id must be in DB');
        $this->assertModelData($templateInspection, $createdTemplateInspection);
    }

    /**
     * @test read
     */
    public function testReadTemplateInspection()
    {
        $templateInspection = $this->makeTemplateInspection();
        $dbTemplateInspection = $this->templateInspectionRepo->find($templateInspection->id);
        $dbTemplateInspection = $dbTemplateInspection->toArray();
        $this->assertModelData($templateInspection->toArray(), $dbTemplateInspection);
    }

    /**
     * @test update
     */
    public function testUpdateTemplateInspection()
    {
        $templateInspection = $this->makeTemplateInspection();
        $fakeTemplateInspection = $this->fakeTemplateInspectionData();
        $updatedTemplateInspection = $this->templateInspectionRepo->update($fakeTemplateInspection, $templateInspection->id);
        $this->assertModelData($fakeTemplateInspection, $updatedTemplateInspection->toArray());
        $dbTemplateInspection = $this->templateInspectionRepo->find($templateInspection->id);
        $this->assertModelData($fakeTemplateInspection, $dbTemplateInspection->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTemplateInspection()
    {
        $templateInspection = $this->makeTemplateInspection();
        $resp = $this->templateInspectionRepo->delete($templateInspection->id);
        $this->assertTrue($resp);
        $this->assertNull(TemplateInspection::find($templateInspection->id), 'TemplateInspection should not exist in DB');
    }
}
