<?php

use App\Models\StatusTemplateInspection;
use App\Repositories\StatusTemplateInspectionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StatusTemplateInspectionRepositoryTest extends TestCase
{
    use MakeStatusTemplateInspectionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StatusTemplateInspectionRepository
     */
    protected $statusTemplateInspectionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->statusTemplateInspectionRepo = App::make(StatusTemplateInspectionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStatusTemplateInspection()
    {
        $statusTemplateInspection = $this->fakeStatusTemplateInspectionData();
        $createdStatusTemplateInspection = $this->statusTemplateInspectionRepo->create($statusTemplateInspection);
        $createdStatusTemplateInspection = $createdStatusTemplateInspection->toArray();
        $this->assertArrayHasKey('id', $createdStatusTemplateInspection);
        $this->assertNotNull($createdStatusTemplateInspection['id'], 'Created StatusTemplateInspection must have id specified');
        $this->assertNotNull(StatusTemplateInspection::find($createdStatusTemplateInspection['id']), 'StatusTemplateInspection with given id must be in DB');
        $this->assertModelData($statusTemplateInspection, $createdStatusTemplateInspection);
    }

    /**
     * @test read
     */
    public function testReadStatusTemplateInspection()
    {
        $statusTemplateInspection = $this->makeStatusTemplateInspection();
        $dbStatusTemplateInspection = $this->statusTemplateInspectionRepo->find($statusTemplateInspection->id);
        $dbStatusTemplateInspection = $dbStatusTemplateInspection->toArray();
        $this->assertModelData($statusTemplateInspection->toArray(), $dbStatusTemplateInspection);
    }

    /**
     * @test update
     */
    public function testUpdateStatusTemplateInspection()
    {
        $statusTemplateInspection = $this->makeStatusTemplateInspection();
        $fakeStatusTemplateInspection = $this->fakeStatusTemplateInspectionData();
        $updatedStatusTemplateInspection = $this->statusTemplateInspectionRepo->update($fakeStatusTemplateInspection, $statusTemplateInspection->id);
        $this->assertModelData($fakeStatusTemplateInspection, $updatedStatusTemplateInspection->toArray());
        $dbStatusTemplateInspection = $this->statusTemplateInspectionRepo->find($statusTemplateInspection->id);
        $this->assertModelData($fakeStatusTemplateInspection, $dbStatusTemplateInspection->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStatusTemplateInspection()
    {
        $statusTemplateInspection = $this->makeStatusTemplateInspection();
        $resp = $this->statusTemplateInspectionRepo->delete($statusTemplateInspection->id);
        $this->assertTrue($resp);
        $this->assertNull(StatusTemplateInspection::find($statusTemplateInspection->id), 'StatusTemplateInspection should not exist in DB');
    }
}
