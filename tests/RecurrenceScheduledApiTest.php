<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RecurrenceScheduledApiTest extends TestCase
{
    use MakeRecurrenceScheduledTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRecurrenceScheduled()
    {
        $recurrenceScheduled = $this->fakeRecurrenceScheduledData();
        $this->json('POST', '/api/v1/recurrenceScheduleds', $recurrenceScheduled);

        $this->assertApiResponse($recurrenceScheduled);
    }

    /**
     * @test
     */
    public function testReadRecurrenceScheduled()
    {
        $recurrenceScheduled = $this->makeRecurrenceScheduled();
        $this->json('GET', '/api/v1/recurrenceScheduleds/'.$recurrenceScheduled->id);

        $this->assertApiResponse($recurrenceScheduled->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRecurrenceScheduled()
    {
        $recurrenceScheduled = $this->makeRecurrenceScheduled();
        $editedRecurrenceScheduled = $this->fakeRecurrenceScheduledData();

        $this->json('PUT', '/api/v1/recurrenceScheduleds/'.$recurrenceScheduled->id, $editedRecurrenceScheduled);

        $this->assertApiResponse($editedRecurrenceScheduled);
    }

    /**
     * @test
     */
    public function testDeleteRecurrenceScheduled()
    {
        $recurrenceScheduled = $this->makeRecurrenceScheduled();
        $this->json('DELETE', '/api/v1/recurrenceScheduleds/'.$recurrenceScheduled->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/recurrenceScheduleds/'.$recurrenceScheduled->id);

        $this->assertResponseStatus(404);
    }
}
