<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StatusCategoryFormApiTest extends TestCase
{
    use MakeStatusCategoryFormTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStatusCategoryForm()
    {
        $statusCategoryForm = $this->fakeStatusCategoryFormData();
        $this->json('POST', '/api/v1/statusCategoryForms', $statusCategoryForm);

        $this->assertApiResponse($statusCategoryForm);
    }

    /**
     * @test
     */
    public function testReadStatusCategoryForm()
    {
        $statusCategoryForm = $this->makeStatusCategoryForm();
        $this->json('GET', '/api/v1/statusCategoryForms/'.$statusCategoryForm->id);

        $this->assertApiResponse($statusCategoryForm->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStatusCategoryForm()
    {
        $statusCategoryForm = $this->makeStatusCategoryForm();
        $editedStatusCategoryForm = $this->fakeStatusCategoryFormData();

        $this->json('PUT', '/api/v1/statusCategoryForms/'.$statusCategoryForm->id, $editedStatusCategoryForm);

        $this->assertApiResponse($editedStatusCategoryForm);
    }

    /**
     * @test
     */
    public function testDeleteStatusCategoryForm()
    {
        $statusCategoryForm = $this->makeStatusCategoryForm();
        $this->json('DELETE', '/api/v1/statusCategoryForms/'.$statusCategoryForm->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/statusCategoryForms/'.$statusCategoryForm->id);

        $this->assertResponseStatus(404);
    }
}
