<?php

use App\Models\Avatar;
use App\Repositories\AvatarRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AvatarRepositoryTest extends TestCase
{
    use MakeAvatarTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AvatarRepository
     */
    protected $avatarRepo;

    public function setUp()
    {
        parent::setUp();
        $this->avatarRepo = App::make(AvatarRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAvatar()
    {
        $avatar = $this->fakeAvatarData();
        $createdAvatar = $this->avatarRepo->create($avatar);
        $createdAvatar = $createdAvatar->toArray();
        $this->assertArrayHasKey('id', $createdAvatar);
        $this->assertNotNull($createdAvatar['id'], 'Created Avatar must have id specified');
        $this->assertNotNull(Avatar::find($createdAvatar['id']), 'Avatar with given id must be in DB');
        $this->assertModelData($avatar, $createdAvatar);
    }

    /**
     * @test read
     */
    public function testReadAvatar()
    {
        $avatar = $this->makeAvatar();
        $dbAvatar = $this->avatarRepo->find($avatar->id);
        $dbAvatar = $dbAvatar->toArray();
        $this->assertModelData($avatar->toArray(), $dbAvatar);
    }

    /**
     * @test update
     */
    public function testUpdateAvatar()
    {
        $avatar = $this->makeAvatar();
        $fakeAvatar = $this->fakeAvatarData();
        $updatedAvatar = $this->avatarRepo->update($fakeAvatar, $avatar->id);
        $this->assertModelData($fakeAvatar, $updatedAvatar->toArray());
        $dbAvatar = $this->avatarRepo->find($avatar->id);
        $this->assertModelData($fakeAvatar, $dbAvatar->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAvatar()
    {
        $avatar = $this->makeAvatar();
        $resp = $this->avatarRepo->delete($avatar->id);
        $this->assertTrue($resp);
        $this->assertNull(Avatar::find($avatar->id), 'Avatar should not exist in DB');
    }
}
