<?php

use App\Models\RecurrenceScheduled;
use App\Repositories\RecurrenceScheduledRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RecurrenceScheduledRepositoryTest extends TestCase
{
    use MakeRecurrenceScheduledTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RecurrenceScheduledRepository
     */
    protected $recurrenceScheduledRepo;

    public function setUp()
    {
        parent::setUp();
        $this->recurrenceScheduledRepo = App::make(RecurrenceScheduledRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRecurrenceScheduled()
    {
        $recurrenceScheduled = $this->fakeRecurrenceScheduledData();
        $createdRecurrenceScheduled = $this->recurrenceScheduledRepo->create($recurrenceScheduled);
        $createdRecurrenceScheduled = $createdRecurrenceScheduled->toArray();
        $this->assertArrayHasKey('id', $createdRecurrenceScheduled);
        $this->assertNotNull($createdRecurrenceScheduled['id'], 'Created RecurrenceScheduled must have id specified');
        $this->assertNotNull(RecurrenceScheduled::find($createdRecurrenceScheduled['id']), 'RecurrenceScheduled with given id must be in DB');
        $this->assertModelData($recurrenceScheduled, $createdRecurrenceScheduled);
    }

    /**
     * @test read
     */
    public function testReadRecurrenceScheduled()
    {
        $recurrenceScheduled = $this->makeRecurrenceScheduled();
        $dbRecurrenceScheduled = $this->recurrenceScheduledRepo->find($recurrenceScheduled->id);
        $dbRecurrenceScheduled = $dbRecurrenceScheduled->toArray();
        $this->assertModelData($recurrenceScheduled->toArray(), $dbRecurrenceScheduled);
    }

    /**
     * @test update
     */
    public function testUpdateRecurrenceScheduled()
    {
        $recurrenceScheduled = $this->makeRecurrenceScheduled();
        $fakeRecurrenceScheduled = $this->fakeRecurrenceScheduledData();
        $updatedRecurrenceScheduled = $this->recurrenceScheduledRepo->update($fakeRecurrenceScheduled, $recurrenceScheduled->id);
        $this->assertModelData($fakeRecurrenceScheduled, $updatedRecurrenceScheduled->toArray());
        $dbRecurrenceScheduled = $this->recurrenceScheduledRepo->find($recurrenceScheduled->id);
        $this->assertModelData($fakeRecurrenceScheduled, $dbRecurrenceScheduled->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRecurrenceScheduled()
    {
        $recurrenceScheduled = $this->makeRecurrenceScheduled();
        $resp = $this->recurrenceScheduledRepo->delete($recurrenceScheduled->id);
        $this->assertTrue($resp);
        $this->assertNull(RecurrenceScheduled::find($recurrenceScheduled->id), 'RecurrenceScheduled should not exist in DB');
    }
}
