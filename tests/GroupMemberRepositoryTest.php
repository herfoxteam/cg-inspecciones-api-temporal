<?php

use App\Models\GroupMember;
use App\Repositories\GroupMemberRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GroupMemberRepositoryTest extends TestCase
{
    use MakeGroupMemberTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var GroupMemberRepository
     */
    protected $groupMemberRepo;

    public function setUp()
    {
        parent::setUp();
        $this->groupMemberRepo = App::make(GroupMemberRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateGroupMember()
    {
        $groupMember = $this->fakeGroupMemberData();
        $createdGroupMember = $this->groupMemberRepo->create($groupMember);
        $createdGroupMember = $createdGroupMember->toArray();
        $this->assertArrayHasKey('id', $createdGroupMember);
        $this->assertNotNull($createdGroupMember['id'], 'Created GroupMember must have id specified');
        $this->assertNotNull(GroupMember::find($createdGroupMember['id']), 'GroupMember with given id must be in DB');
        $this->assertModelData($groupMember, $createdGroupMember);
    }

    /**
     * @test read
     */
    public function testReadGroupMember()
    {
        $groupMember = $this->makeGroupMember();
        $dbGroupMember = $this->groupMemberRepo->find($groupMember->id);
        $dbGroupMember = $dbGroupMember->toArray();
        $this->assertModelData($groupMember->toArray(), $dbGroupMember);
    }

    /**
     * @test update
     */
    public function testUpdateGroupMember()
    {
        $groupMember = $this->makeGroupMember();
        $fakeGroupMember = $this->fakeGroupMemberData();
        $updatedGroupMember = $this->groupMemberRepo->update($fakeGroupMember, $groupMember->id);
        $this->assertModelData($fakeGroupMember, $updatedGroupMember->toArray());
        $dbGroupMember = $this->groupMemberRepo->find($groupMember->id);
        $this->assertModelData($fakeGroupMember, $dbGroupMember->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteGroupMember()
    {
        $groupMember = $this->makeGroupMember();
        $resp = $this->groupMemberRepo->delete($groupMember->id);
        $this->assertTrue($resp);
        $this->assertNull(GroupMember::find($groupMember->id), 'GroupMember should not exist in DB');
    }
}
