<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActionPlanApiTest extends TestCase
{
    use MakeActionPlanTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateActionPlan()
    {
        $actionPlan = $this->fakeActionPlanData();
        $this->json('POST', '/api/v1/actionPlans', $actionPlan);

        $this->assertApiResponse($actionPlan);
    }

    /**
     * @test
     */
    public function testReadActionPlan()
    {
        $actionPlan = $this->makeActionPlan();
        $this->json('GET', '/api/v1/actionPlans/'.$actionPlan->id);

        $this->assertApiResponse($actionPlan->toArray());
    }

    /**
     * @test
     */
    public function testUpdateActionPlan()
    {
        $actionPlan = $this->makeActionPlan();
        $editedActionPlan = $this->fakeActionPlanData();

        $this->json('PUT', '/api/v1/actionPlans/'.$actionPlan->id, $editedActionPlan);

        $this->assertApiResponse($editedActionPlan);
    }

    /**
     * @test
     */
    public function testDeleteActionPlan()
    {
        $actionPlan = $this->makeActionPlan();
        $this->json('DELETE', '/api/v1/actionPlans/'.$actionPlan->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/actionPlans/'.$actionPlan->id);

        $this->assertResponseStatus(404);
    }
}
