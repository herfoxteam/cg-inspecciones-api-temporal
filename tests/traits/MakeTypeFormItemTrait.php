<?php

use Faker\Factory as Faker;
use App\Models\TypeFormItem;
use App\Repositories\TypeFormItemRepository;

trait MakeTypeFormItemTrait
{
    /**
     * Create fake instance of TypeFormItem and save it in database
     *
     * @param array $typeFormItemFields
     * @return TypeFormItem
     */
    public function makeTypeFormItem($typeFormItemFields = [])
    {
        /** @var TypeFormItemRepository $typeFormItemRepo */
        $typeFormItemRepo = App::make(TypeFormItemRepository::class);
        $theme = $this->fakeTypeFormItemData($typeFormItemFields);
        return $typeFormItemRepo->create($theme);
    }

    /**
     * Get fake instance of TypeFormItem
     *
     * @param array $typeFormItemFields
     * @return TypeFormItem
     */
    public function fakeTypeFormItem($typeFormItemFields = [])
    {
        return new TypeFormItem($this->fakeTypeFormItemData($typeFormItemFields));
    }

    /**
     * Get fake data of TypeFormItem
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTypeFormItemData($typeFormItemFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'description' => $fake->text,
            'type' => $fake->word,
            'icon_name' => $fake->word,
            'rules' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $typeFormItemFields);
    }
}
