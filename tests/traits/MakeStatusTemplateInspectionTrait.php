<?php

use Faker\Factory as Faker;
use App\Models\StatusTemplateInspection;
use App\Repositories\StatusTemplateInspectionRepository;

trait MakeStatusTemplateInspectionTrait
{
    /**
     * Create fake instance of StatusTemplateInspection and save it in database
     *
     * @param array $statusTemplateInspectionFields
     * @return StatusTemplateInspection
     */
    public function makeStatusTemplateInspection($statusTemplateInspectionFields = [])
    {
        /** @var StatusTemplateInspectionRepository $statusTemplateInspectionRepo */
        $statusTemplateInspectionRepo = App::make(StatusTemplateInspectionRepository::class);
        $theme = $this->fakeStatusTemplateInspectionData($statusTemplateInspectionFields);
        return $statusTemplateInspectionRepo->create($theme);
    }

    /**
     * Get fake instance of StatusTemplateInspection
     *
     * @param array $statusTemplateInspectionFields
     * @return StatusTemplateInspection
     */
    public function fakeStatusTemplateInspection($statusTemplateInspectionFields = [])
    {
        return new StatusTemplateInspection($this->fakeStatusTemplateInspectionData($statusTemplateInspectionFields));
    }

    /**
     * Get fake data of StatusTemplateInspection
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStatusTemplateInspectionData($statusTemplateInspectionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'description' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $statusTemplateInspectionFields);
    }
}
