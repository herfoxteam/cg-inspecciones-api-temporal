<?php

use Faker\Factory as Faker;
use App\Models\ObservationFormItem;
use App\Repositories\ObservationFormItemRepository;

trait MakeObservationFormItemTrait
{
    /**
     * Create fake instance of ObservationFormItem and save it in database
     *
     * @param array $observationFormItemFields
     * @return ObservationFormItem
     */
    public function makeObservationFormItem($observationFormItemFields = [])
    {
        /** @var ObservationFormItemRepository $observationFormItemRepo */
        $observationFormItemRepo = App::make(ObservationFormItemRepository::class);
        $theme = $this->fakeObservationFormItemData($observationFormItemFields);
        return $observationFormItemRepo->create($theme);
    }

    /**
     * Get fake instance of ObservationFormItem
     *
     * @param array $observationFormItemFields
     * @return ObservationFormItem
     */
    public function fakeObservationFormItem($observationFormItemFields = [])
    {
        return new ObservationFormItem($this->fakeObservationFormItemData($observationFormItemFields));
    }

    /**
     * Get fake data of ObservationFormItem
     *
     * @param array $postFields
     * @return array
     */
    public function fakeObservationFormItemData($observationFormItemFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'inspection_forms_items_code' => $fake->word,
            'description' => $fake->text,
            'hash_form' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $observationFormItemFields);
    }
}
