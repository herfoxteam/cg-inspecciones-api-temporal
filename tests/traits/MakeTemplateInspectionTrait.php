<?php

use Faker\Factory as Faker;
use App\Models\TemplateInspection;
use App\Repositories\TemplateInspectionRepository;

trait MakeTemplateInspectionTrait
{
    /**
     * Create fake instance of TemplateInspection and save it in database
     *
     * @param array $templateInspectionFields
     * @return TemplateInspection
     */
    public function makeTemplateInspection($templateInspectionFields = [])
    {
        /** @var TemplateInspectionRepository $templateInspectionRepo */
        $templateInspectionRepo = App::make(TemplateInspectionRepository::class);
        $theme = $this->fakeTemplateInspectionData($templateInspectionFields);
        return $templateInspectionRepo->create($theme);
    }

    /**
     * Get fake instance of TemplateInspection
     *
     * @param array $templateInspectionFields
     * @return TemplateInspection
     */
    public function fakeTemplateInspection($templateInspectionFields = [])
    {
        return new TemplateInspection($this->fakeTemplateInspectionData($templateInspectionFields));
    }

    /**
     * Get fake data of TemplateInspection
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTemplateInspectionData($templateInspectionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'description' => $fake->text,
            'user_id' => $fake->randomDigitNotNull,
            'status_template_id' => $fake->randomDigitNotNull,
            'code' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $templateInspectionFields);
    }
}
