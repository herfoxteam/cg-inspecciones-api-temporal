<?php

use Faker\Factory as Faker;
use App\Models\CategoryInspectionForm;
use App\Repositories\CategoryInspectionFormRepository;

trait MakeCategoryInspectionFormTrait
{
    /**
     * Create fake instance of CategoryInspectionForm and save it in database
     *
     * @param array $categoryInspectionFormFields
     * @return CategoryInspectionForm
     */
    public function makeCategoryInspectionForm($categoryInspectionFormFields = [])
    {
        /** @var CategoryInspectionFormRepository $categoryInspectionFormRepo */
        $categoryInspectionFormRepo = App::make(CategoryInspectionFormRepository::class);
        $theme = $this->fakeCategoryInspectionFormData($categoryInspectionFormFields);
        return $categoryInspectionFormRepo->create($theme);
    }

    /**
     * Get fake instance of CategoryInspectionForm
     *
     * @param array $categoryInspectionFormFields
     * @return CategoryInspectionForm
     */
    public function fakeCategoryInspectionForm($categoryInspectionFormFields = [])
    {
        return new CategoryInspectionForm($this->fakeCategoryInspectionFormData($categoryInspectionFormFields));
    }

    /**
     * Get fake data of CategoryInspectionForm
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCategoryInspectionFormData($categoryInspectionFormFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'location_value' => $fake->randomDigitNotNull,
            'label_value' => $fake->word,
            'icon_class' => $fake->word,
            'template_code' => $fake->word,
            'status_categories_forms_id' => $fake->randomDigitNotNull,
            'hash_category' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $categoryInspectionFormFields);
    }
}
