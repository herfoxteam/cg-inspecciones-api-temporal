<?php

use Faker\Factory as Faker;
use App\Models\GroupMember;
use App\Repositories\GroupMemberRepository;

trait MakeGroupMemberTrait
{
    /**
     * Create fake instance of GroupMember and save it in database
     *
     * @param array $groupMemberFields
     * @return GroupMember
     */
    public function makeGroupMember($groupMemberFields = [])
    {
        /** @var GroupMemberRepository $groupMemberRepo */
        $groupMemberRepo = App::make(GroupMemberRepository::class);
        $theme = $this->fakeGroupMemberData($groupMemberFields);
        return $groupMemberRepo->create($theme);
    }

    /**
     * Get fake instance of GroupMember
     *
     * @param array $groupMemberFields
     * @return GroupMember
     */
    public function fakeGroupMember($groupMemberFields = [])
    {
        return new GroupMember($this->fakeGroupMemberData($groupMemberFields));
    }

    /**
     * Get fake data of GroupMember
     *
     * @param array $postFields
     * @return array
     */
    public function fakeGroupMemberData($groupMemberFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_group_id' => $fake->randomDigitNotNull,
            'user_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $groupMemberFields);
    }
}
