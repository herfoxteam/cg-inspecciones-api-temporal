<?php

use Faker\Factory as Faker;
use App\Models\Avatar;
use App\Repositories\AvatarRepository;

trait MakeAvatarTrait
{
    /**
     * Create fake instance of Avatar and save it in database
     *
     * @param array $avatarFields
     * @return Avatar
     */
    public function makeAvatar($avatarFields = [])
    {
        /** @var AvatarRepository $avatarRepo */
        $avatarRepo = App::make(AvatarRepository::class);
        $theme = $this->fakeAvatarData($avatarFields);
        return $avatarRepo->create($theme);
    }

    /**
     * Get fake instance of Avatar
     *
     * @param array $avatarFields
     * @return Avatar
     */
    public function fakeAvatar($avatarFields = [])
    {
        return new Avatar($this->fakeAvatarData($avatarFields));
    }

    /**
     * Get fake data of Avatar
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAvatarData($avatarFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->randomDigitNotNull,
            'thumbnail' => $fake->word,
            'original' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $avatarFields);
    }
}
