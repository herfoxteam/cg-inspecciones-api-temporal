<?php

use Faker\Factory as Faker;
use App\Models\RecurrenceScheduled;
use App\Repositories\RecurrenceScheduledRepository;

trait MakeRecurrenceScheduledTrait
{
    /**
     * Create fake instance of RecurrenceScheduled and save it in database
     *
     * @param array $recurrenceScheduledFields
     * @return RecurrenceScheduled
     */
    public function makeRecurrenceScheduled($recurrenceScheduledFields = [])
    {
        /** @var RecurrenceScheduledRepository $recurrenceScheduledRepo */
        $recurrenceScheduledRepo = App::make(RecurrenceScheduledRepository::class);
        $theme = $this->fakeRecurrenceScheduledData($recurrenceScheduledFields);
        return $recurrenceScheduledRepo->create($theme);
    }

    /**
     * Get fake instance of RecurrenceScheduled
     *
     * @param array $recurrenceScheduledFields
     * @return RecurrenceScheduled
     */
    public function fakeRecurrenceScheduled($recurrenceScheduledFields = [])
    {
        return new RecurrenceScheduled($this->fakeRecurrenceScheduledData($recurrenceScheduledFields));
    }

    /**
     * Get fake data of RecurrenceScheduled
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRecurrenceScheduledData($recurrenceScheduledFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'type_recurrence_id' => $fake->randomDigitNotNull,
            'active' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'template_id' => $fake->randomDigitNotNull,
            'start_date' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $recurrenceScheduledFields);
    }
}
