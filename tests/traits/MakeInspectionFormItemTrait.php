<?php

use Faker\Factory as Faker;
use App\Models\InspectionFormItem;
use App\Repositories\InspectionFormItemRepository;

trait MakeInspectionFormItemTrait
{
    /**
     * Create fake instance of InspectionFormItem and save it in database
     *
     * @param array $inspectionFormItemFields
     * @return InspectionFormItem
     */
    public function makeInspectionFormItem($inspectionFormItemFields = [])
    {
        /** @var InspectionFormItemRepository $inspectionFormItemRepo */
        $inspectionFormItemRepo = App::make(InspectionFormItemRepository::class);
        $theme = $this->fakeInspectionFormItemData($inspectionFormItemFields);
        return $inspectionFormItemRepo->create($theme);
    }

    /**
     * Get fake instance of InspectionFormItem
     *
     * @param array $inspectionFormItemFields
     * @return InspectionFormItem
     */
    public function fakeInspectionFormItem($inspectionFormItemFields = [])
    {
        return new InspectionFormItem($this->fakeInspectionFormItemData($inspectionFormItemFields));
    }

    /**
     * Get fake data of InspectionFormItem
     *
     * @param array $postFields
     * @return array
     */
    public function fakeInspectionFormItemData($inspectionFormItemFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'category_hash' => $fake->word,
            'label' => $fake->text,
            'type_forms_items_id' => $fake->randomDigitNotNull,
            'cod_item' => $fake->word,
            'required' => $fake->randomDigitNotNull,
            'published' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $inspectionFormItemFields);
    }
}
