<?php

use Faker\Factory as Faker;
use App\Models\PhotographicEvidence;
use App\Repositories\PhotographicEvidenceRepository;

trait MakePhotographicEvidenceTrait
{
    /**
     * Create fake instance of PhotographicEvidence and save it in database
     *
     * @param array $photographicEvidenceFields
     * @return PhotographicEvidence
     */
    public function makePhotographicEvidence($photographicEvidenceFields = [])
    {
        /** @var PhotographicEvidenceRepository $photographicEvidenceRepo */
        $photographicEvidenceRepo = App::make(PhotographicEvidenceRepository::class);
        $theme = $this->fakePhotographicEvidenceData($photographicEvidenceFields);
        return $photographicEvidenceRepo->create($theme);
    }

    /**
     * Get fake instance of PhotographicEvidence
     *
     * @param array $photographicEvidenceFields
     * @return PhotographicEvidence
     */
    public function fakePhotographicEvidence($photographicEvidenceFields = [])
    {
        return new PhotographicEvidence($this->fakePhotographicEvidenceData($photographicEvidenceFields));
    }

    /**
     * Get fake data of PhotographicEvidence
     *
     * @param array $postFields
     * @return array
     */
    public function fakePhotographicEvidenceData($photographicEvidenceFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'inspection_forms_items_code' => $fake->word,
            'thumbnail' => $fake->word,
            'original_file' => $fake->word,
            'image' => $fake->word,
            'hash_form' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $photographicEvidenceFields);
    }
}
