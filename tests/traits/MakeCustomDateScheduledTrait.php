<?php

use Faker\Factory as Faker;
use App\Models\CustomDateScheduled;
use App\Repositories\CustomDateScheduledRepository;

trait MakeCustomDateScheduledTrait
{
    /**
     * Create fake instance of CustomDateScheduled and save it in database
     *
     * @param array $customDateScheduledFields
     * @return CustomDateScheduled
     */
    public function makeCustomDateScheduled($customDateScheduledFields = [])
    {
        /** @var CustomDateScheduledRepository $customDateScheduledRepo */
        $customDateScheduledRepo = App::make(CustomDateScheduledRepository::class);
        $theme = $this->fakeCustomDateScheduledData($customDateScheduledFields);
        return $customDateScheduledRepo->create($theme);
    }

    /**
     * Get fake instance of CustomDateScheduled
     *
     * @param array $customDateScheduledFields
     * @return CustomDateScheduled
     */
    public function fakeCustomDateScheduled($customDateScheduledFields = [])
    {
        return new CustomDateScheduled($this->fakeCustomDateScheduledData($customDateScheduledFields));
    }

    /**
     * Get fake data of CustomDateScheduled
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCustomDateScheduledData($customDateScheduledFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'custom_date' => $fake->date('Y-m-d H:i:s'),
            'recurrence_schedules_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $customDateScheduledFields);
    }
}
