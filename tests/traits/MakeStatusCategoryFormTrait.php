<?php

use Faker\Factory as Faker;
use App\Models\StatusCategoryForm;
use App\Repositories\StatusCategoryFormRepository;

trait MakeStatusCategoryFormTrait
{
    /**
     * Create fake instance of StatusCategoryForm and save it in database
     *
     * @param array $statusCategoryFormFields
     * @return StatusCategoryForm
     */
    public function makeStatusCategoryForm($statusCategoryFormFields = [])
    {
        /** @var StatusCategoryFormRepository $statusCategoryFormRepo */
        $statusCategoryFormRepo = App::make(StatusCategoryFormRepository::class);
        $theme = $this->fakeStatusCategoryFormData($statusCategoryFormFields);
        return $statusCategoryFormRepo->create($theme);
    }

    /**
     * Get fake instance of StatusCategoryForm
     *
     * @param array $statusCategoryFormFields
     * @return StatusCategoryForm
     */
    public function fakeStatusCategoryForm($statusCategoryFormFields = [])
    {
        return new StatusCategoryForm($this->fakeStatusCategoryFormData($statusCategoryFormFields));
    }

    /**
     * Get fake data of StatusCategoryForm
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStatusCategoryFormData($statusCategoryFormFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'description' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $statusCategoryFormFields);
    }
}
