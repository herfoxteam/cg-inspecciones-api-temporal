<?php

use Faker\Factory as Faker;
use App\Models\PriorityActionPlan;
use App\Repositories\PriorityActionPlanRepository;

trait MakePriorityActionPlanTrait
{
    /**
     * Create fake instance of PriorityActionPlan and save it in database
     *
     * @param array $priorityActionPlanFields
     * @return PriorityActionPlan
     */
    public function makePriorityActionPlan($priorityActionPlanFields = [])
    {
        /** @var PriorityActionPlanRepository $priorityActionPlanRepo */
        $priorityActionPlanRepo = App::make(PriorityActionPlanRepository::class);
        $theme = $this->fakePriorityActionPlanData($priorityActionPlanFields);
        return $priorityActionPlanRepo->create($theme);
    }

    /**
     * Get fake instance of PriorityActionPlan
     *
     * @param array $priorityActionPlanFields
     * @return PriorityActionPlan
     */
    public function fakePriorityActionPlan($priorityActionPlanFields = [])
    {
        return new PriorityActionPlan($this->fakePriorityActionPlanData($priorityActionPlanFields));
    }

    /**
     * Get fake data of PriorityActionPlan
     *
     * @param array $postFields
     * @return array
     */
    public function fakePriorityActionPlanData($priorityActionPlanFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'description' => $fake->text,
            'icon_class' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $priorityActionPlanFields);
    }
}
