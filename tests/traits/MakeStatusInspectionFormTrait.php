<?php

use Faker\Factory as Faker;
use App\Models\StatusInspectionForm;
use App\Repositories\StatusInspectionFormRepository;

trait MakeStatusInspectionFormTrait
{
    /**
     * Create fake instance of StatusInspectionForm and save it in database
     *
     * @param array $statusInspectionFormFields
     * @return StatusInspectionForm
     */
    public function makeStatusInspectionForm($statusInspectionFormFields = [])
    {
        /** @var StatusInspectionFormRepository $statusInspectionFormRepo */
        $statusInspectionFormRepo = App::make(StatusInspectionFormRepository::class);
        $theme = $this->fakeStatusInspectionFormData($statusInspectionFormFields);
        return $statusInspectionFormRepo->create($theme);
    }

    /**
     * Get fake instance of StatusInspectionForm
     *
     * @param array $statusInspectionFormFields
     * @return StatusInspectionForm
     */
    public function fakeStatusInspectionForm($statusInspectionFormFields = [])
    {
        return new StatusInspectionForm($this->fakeStatusInspectionFormData($statusInspectionFormFields));
    }

    /**
     * Get fake data of StatusInspectionForm
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStatusInspectionFormData($statusInspectionFormFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'description' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $statusInspectionFormFields);
    }
}
