<?php

use Faker\Factory as Faker;
use App\Models\OptionFormItem;
use App\Repositories\OptionFormItemRepository;

trait MakeOptionFormItemTrait
{
    /**
     * Create fake instance of OptionFormItem and save it in database
     *
     * @param array $optionFormItemFields
     * @return OptionFormItem
     */
    public function makeOptionFormItem($optionFormItemFields = [])
    {
        /** @var OptionFormItemRepository $optionFormItemRepo */
        $optionFormItemRepo = App::make(OptionFormItemRepository::class);
        $theme = $this->fakeOptionFormItemData($optionFormItemFields);
        return $optionFormItemRepo->create($theme);
    }

    /**
     * Get fake instance of OptionFormItem
     *
     * @param array $optionFormItemFields
     * @return OptionFormItem
     */
    public function fakeOptionFormItem($optionFormItemFields = [])
    {
        return new OptionFormItem($this->fakeOptionFormItemData($optionFormItemFields));
    }

    /**
     * Get fake data of OptionFormItem
     *
     * @param array $postFields
     * @return array
     */
    public function fakeOptionFormItemData($optionFormItemFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'form_code_item' => $fake->word,
            'label' => $fake->word,
            'value' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $optionFormItemFields);
    }
}
