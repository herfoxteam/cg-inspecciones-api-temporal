<?php

use Faker\Factory as Faker;
use App\Models\TypeRecurrence;
use App\Repositories\TypeRecurrenceRepository;

trait MakeTypeRecurrenceTrait
{
    /**
     * Create fake instance of TypeRecurrence and save it in database
     *
     * @param array $typeRecurrenceFields
     * @return TypeRecurrence
     */
    public function makeTypeRecurrence($typeRecurrenceFields = [])
    {
        /** @var TypeRecurrenceRepository $typeRecurrenceRepo */
        $typeRecurrenceRepo = App::make(TypeRecurrenceRepository::class);
        $theme = $this->fakeTypeRecurrenceData($typeRecurrenceFields);
        return $typeRecurrenceRepo->create($theme);
    }

    /**
     * Get fake instance of TypeRecurrence
     *
     * @param array $typeRecurrenceFields
     * @return TypeRecurrence
     */
    public function fakeTypeRecurrence($typeRecurrenceFields = [])
    {
        return new TypeRecurrence($this->fakeTypeRecurrenceData($typeRecurrenceFields));
    }

    /**
     * Get fake data of TypeRecurrence
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTypeRecurrenceData($typeRecurrenceFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'value' => $fake->word,
            'label' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $typeRecurrenceFields);
    }
}
