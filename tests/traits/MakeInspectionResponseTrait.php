<?php

use Faker\Factory as Faker;
use App\Models\InspectionResponse;
use App\Repositories\InspectionResponseRepository;

trait MakeInspectionResponseTrait
{
    /**
     * Create fake instance of InspectionResponse and save it in database
     *
     * @param array $inspectionResponseFields
     * @return InspectionResponse
     */
    public function makeInspectionResponse($inspectionResponseFields = [])
    {
        /** @var InspectionResponseRepository $inspectionResponseRepo */
        $inspectionResponseRepo = App::make(InspectionResponseRepository::class);
        $theme = $this->fakeInspectionResponseData($inspectionResponseFields);
        return $inspectionResponseRepo->create($theme);
    }

    /**
     * Get fake instance of InspectionResponse
     *
     * @param array $inspectionResponseFields
     * @return InspectionResponse
     */
    public function fakeInspectionResponse($inspectionResponseFields = [])
    {
        return new InspectionResponse($this->fakeInspectionResponseData($inspectionResponseFields));
    }

    /**
     * Get fake data of InspectionResponse
     *
     * @param array $postFields
     * @return array
     */
    public function fakeInspectionResponseData($inspectionResponseFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'form_response' => $fake->text,
            'inspections_forms_hash' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $inspectionResponseFields);
    }
}
