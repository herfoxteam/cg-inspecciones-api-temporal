<?php

use Faker\Factory as Faker;
use App\Models\InspectionForm;
use App\Repositories\InspectionFormRepository;

trait MakeInspectionFormTrait
{
    /**
     * Create fake instance of InspectionForm and save it in database
     *
     * @param array $inspectionFormFields
     * @return InspectionForm
     */
    public function makeInspectionForm($inspectionFormFields = [])
    {
        /** @var InspectionFormRepository $inspectionFormRepo */
        $inspectionFormRepo = App::make(InspectionFormRepository::class);
        $theme = $this->fakeInspectionFormData($inspectionFormFields);
        return $inspectionFormRepo->create($theme);
    }

    /**
     * Get fake instance of InspectionForm
     *
     * @param array $inspectionFormFields
     * @return InspectionForm
     */
    public function fakeInspectionForm($inspectionFormFields = [])
    {
        return new InspectionForm($this->fakeInspectionFormData($inspectionFormFields));
    }

    /**
     * Get fake data of InspectionForm
     *
     * @param array $postFields
     * @return array
     */
    public function fakeInspectionFormData($inspectionFormFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'hash' => $fake->word,
            'template_code' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'status_inspection_form_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $inspectionFormFields);
    }
}
