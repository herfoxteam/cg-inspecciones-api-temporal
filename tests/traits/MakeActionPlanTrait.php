<?php

use Faker\Factory as Faker;
use App\Models\ActionPlan;
use App\Repositories\ActionPlanRepository;

trait MakeActionPlanTrait
{
    /**
     * Create fake instance of ActionPlan and save it in database
     *
     * @param array $actionPlanFields
     * @return ActionPlan
     */
    public function makeActionPlan($actionPlanFields = [])
    {
        /** @var ActionPlanRepository $actionPlanRepo */
        $actionPlanRepo = App::make(ActionPlanRepository::class);
        $theme = $this->fakeActionPlanData($actionPlanFields);
        return $actionPlanRepo->create($theme);
    }

    /**
     * Get fake instance of ActionPlan
     *
     * @param array $actionPlanFields
     * @return ActionPlan
     */
    public function fakeActionPlan($actionPlanFields = [])
    {
        return new ActionPlan($this->fakeActionPlanData($actionPlanFields));
    }

    /**
     * Get fake data of ActionPlan
     *
     * @param array $postFields
     * @return array
     */
    public function fakeActionPlanData($actionPlanFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'description' => $fake->text,
            'date_time_end' => $fake->date('Y-m-d H:i:s'),
            'assigned_user_id' => $fake->randomDigitNotNull,
            'priorities_id' => $fake->randomDigitNotNull,
            'inspections_form_item_code' => $fake->word,
            'closed' => $fake->word,
            'hash_form' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $actionPlanFields);
    }
}
