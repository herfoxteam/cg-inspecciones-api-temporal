<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PriorityActionPlanApiTest extends TestCase
{
    use MakePriorityActionPlanTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePriorityActionPlan()
    {
        $priorityActionPlan = $this->fakePriorityActionPlanData();
        $this->json('POST', '/api/v1/priorityActionPlans', $priorityActionPlan);

        $this->assertApiResponse($priorityActionPlan);
    }

    /**
     * @test
     */
    public function testReadPriorityActionPlan()
    {
        $priorityActionPlan = $this->makePriorityActionPlan();
        $this->json('GET', '/api/v1/priorityActionPlans/'.$priorityActionPlan->id);

        $this->assertApiResponse($priorityActionPlan->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePriorityActionPlan()
    {
        $priorityActionPlan = $this->makePriorityActionPlan();
        $editedPriorityActionPlan = $this->fakePriorityActionPlanData();

        $this->json('PUT', '/api/v1/priorityActionPlans/'.$priorityActionPlan->id, $editedPriorityActionPlan);

        $this->assertApiResponse($editedPriorityActionPlan);
    }

    /**
     * @test
     */
    public function testDeletePriorityActionPlan()
    {
        $priorityActionPlan = $this->makePriorityActionPlan();
        $this->json('DELETE', '/api/v1/priorityActionPlans/'.$priorityActionPlan->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/priorityActionPlans/'.$priorityActionPlan->id);

        $this->assertResponseStatus(404);
    }
}
