<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StatusTemplateInspectionApiTest extends TestCase
{
    use MakeStatusTemplateInspectionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStatusTemplateInspection()
    {
        $statusTemplateInspection = $this->fakeStatusTemplateInspectionData();
        $this->json('POST', '/api/v1/statusTemplateInspections', $statusTemplateInspection);

        $this->assertApiResponse($statusTemplateInspection);
    }

    /**
     * @test
     */
    public function testReadStatusTemplateInspection()
    {
        $statusTemplateInspection = $this->makeStatusTemplateInspection();
        $this->json('GET', '/api/v1/statusTemplateInspections/'.$statusTemplateInspection->id);

        $this->assertApiResponse($statusTemplateInspection->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStatusTemplateInspection()
    {
        $statusTemplateInspection = $this->makeStatusTemplateInspection();
        $editedStatusTemplateInspection = $this->fakeStatusTemplateInspectionData();

        $this->json('PUT', '/api/v1/statusTemplateInspections/'.$statusTemplateInspection->id, $editedStatusTemplateInspection);

        $this->assertApiResponse($editedStatusTemplateInspection);
    }

    /**
     * @test
     */
    public function testDeleteStatusTemplateInspection()
    {
        $statusTemplateInspection = $this->makeStatusTemplateInspection();
        $this->json('DELETE', '/api/v1/statusTemplateInspections/'.$statusTemplateInspection->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/statusTemplateInspections/'.$statusTemplateInspection->id);

        $this->assertResponseStatus(404);
    }
}
