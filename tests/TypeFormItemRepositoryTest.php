<?php

use App\Models\TypeFormItem;
use App\Repositories\TypeFormItemRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TypeFormItemRepositoryTest extends TestCase
{
    use MakeTypeFormItemTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TypeFormItemRepository
     */
    protected $typeFormItemRepo;

    public function setUp()
    {
        parent::setUp();
        $this->typeFormItemRepo = App::make(TypeFormItemRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTypeFormItem()
    {
        $typeFormItem = $this->fakeTypeFormItemData();
        $createdTypeFormItem = $this->typeFormItemRepo->create($typeFormItem);
        $createdTypeFormItem = $createdTypeFormItem->toArray();
        $this->assertArrayHasKey('id', $createdTypeFormItem);
        $this->assertNotNull($createdTypeFormItem['id'], 'Created TypeFormItem must have id specified');
        $this->assertNotNull(TypeFormItem::find($createdTypeFormItem['id']), 'TypeFormItem with given id must be in DB');
        $this->assertModelData($typeFormItem, $createdTypeFormItem);
    }

    /**
     * @test read
     */
    public function testReadTypeFormItem()
    {
        $typeFormItem = $this->makeTypeFormItem();
        $dbTypeFormItem = $this->typeFormItemRepo->find($typeFormItem->id);
        $dbTypeFormItem = $dbTypeFormItem->toArray();
        $this->assertModelData($typeFormItem->toArray(), $dbTypeFormItem);
    }

    /**
     * @test update
     */
    public function testUpdateTypeFormItem()
    {
        $typeFormItem = $this->makeTypeFormItem();
        $fakeTypeFormItem = $this->fakeTypeFormItemData();
        $updatedTypeFormItem = $this->typeFormItemRepo->update($fakeTypeFormItem, $typeFormItem->id);
        $this->assertModelData($fakeTypeFormItem, $updatedTypeFormItem->toArray());
        $dbTypeFormItem = $this->typeFormItemRepo->find($typeFormItem->id);
        $this->assertModelData($fakeTypeFormItem, $dbTypeFormItem->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTypeFormItem()
    {
        $typeFormItem = $this->makeTypeFormItem();
        $resp = $this->typeFormItemRepo->delete($typeFormItem->id);
        $this->assertTrue($resp);
        $this->assertNull(TypeFormItem::find($typeFormItem->id), 'TypeFormItem should not exist in DB');
    }
}
