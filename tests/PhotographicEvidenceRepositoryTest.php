<?php

use App\Models\PhotographicEvidence;
use App\Repositories\PhotographicEvidenceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PhotographicEvidenceRepositoryTest extends TestCase
{
    use MakePhotographicEvidenceTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PhotographicEvidenceRepository
     */
    protected $photographicEvidenceRepo;

    public function setUp()
    {
        parent::setUp();
        $this->photographicEvidenceRepo = App::make(PhotographicEvidenceRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePhotographicEvidence()
    {
        $photographicEvidence = $this->fakePhotographicEvidenceData();
        $createdPhotographicEvidence = $this->photographicEvidenceRepo->create($photographicEvidence);
        $createdPhotographicEvidence = $createdPhotographicEvidence->toArray();
        $this->assertArrayHasKey('id', $createdPhotographicEvidence);
        $this->assertNotNull($createdPhotographicEvidence['id'], 'Created PhotographicEvidence must have id specified');
        $this->assertNotNull(PhotographicEvidence::find($createdPhotographicEvidence['id']), 'PhotographicEvidence with given id must be in DB');
        $this->assertModelData($photographicEvidence, $createdPhotographicEvidence);
    }

    /**
     * @test read
     */
    public function testReadPhotographicEvidence()
    {
        $photographicEvidence = $this->makePhotographicEvidence();
        $dbPhotographicEvidence = $this->photographicEvidenceRepo->find($photographicEvidence->id);
        $dbPhotographicEvidence = $dbPhotographicEvidence->toArray();
        $this->assertModelData($photographicEvidence->toArray(), $dbPhotographicEvidence);
    }

    /**
     * @test update
     */
    public function testUpdatePhotographicEvidence()
    {
        $photographicEvidence = $this->makePhotographicEvidence();
        $fakePhotographicEvidence = $this->fakePhotographicEvidenceData();
        $updatedPhotographicEvidence = $this->photographicEvidenceRepo->update($fakePhotographicEvidence, $photographicEvidence->id);
        $this->assertModelData($fakePhotographicEvidence, $updatedPhotographicEvidence->toArray());
        $dbPhotographicEvidence = $this->photographicEvidenceRepo->find($photographicEvidence->id);
        $this->assertModelData($fakePhotographicEvidence, $dbPhotographicEvidence->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePhotographicEvidence()
    {
        $photographicEvidence = $this->makePhotographicEvidence();
        $resp = $this->photographicEvidenceRepo->delete($photographicEvidence->id);
        $this->assertTrue($resp);
        $this->assertNull(PhotographicEvidence::find($photographicEvidence->id), 'PhotographicEvidence should not exist in DB');
    }
}
