<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InspectionFormApiTest extends TestCase
{
    use MakeInspectionFormTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateInspectionForm()
    {
        $inspectionForm = $this->fakeInspectionFormData();
        $this->json('POST', '/api/v1/inspectionForms', $inspectionForm);

        $this->assertApiResponse($inspectionForm);
    }

    /**
     * @test
     */
    public function testReadInspectionForm()
    {
        $inspectionForm = $this->makeInspectionForm();
        $this->json('GET', '/api/v1/inspectionForms/'.$inspectionForm->id);

        $this->assertApiResponse($inspectionForm->toArray());
    }

    /**
     * @test
     */
    public function testUpdateInspectionForm()
    {
        $inspectionForm = $this->makeInspectionForm();
        $editedInspectionForm = $this->fakeInspectionFormData();

        $this->json('PUT', '/api/v1/inspectionForms/'.$inspectionForm->id, $editedInspectionForm);

        $this->assertApiResponse($editedInspectionForm);
    }

    /**
     * @test
     */
    public function testDeleteInspectionForm()
    {
        $inspectionForm = $this->makeInspectionForm();
        $this->json('DELETE', '/api/v1/inspectionForms/'.$inspectionForm->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/inspectionForms/'.$inspectionForm->id);

        $this->assertResponseStatus(404);
    }
}
