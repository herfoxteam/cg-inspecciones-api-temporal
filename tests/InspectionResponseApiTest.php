<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InspectionResponseApiTest extends TestCase
{
    use MakeInspectionResponseTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateInspectionResponse()
    {
        $inspectionResponse = $this->fakeInspectionResponseData();
        $this->json('POST', '/api/v1/inspectionResponses', $inspectionResponse);

        $this->assertApiResponse($inspectionResponse);
    }

    /**
     * @test
     */
    public function testReadInspectionResponse()
    {
        $inspectionResponse = $this->makeInspectionResponse();
        $this->json('GET', '/api/v1/inspectionResponses/'.$inspectionResponse->id);

        $this->assertApiResponse($inspectionResponse->toArray());
    }

    /**
     * @test
     */
    public function testUpdateInspectionResponse()
    {
        $inspectionResponse = $this->makeInspectionResponse();
        $editedInspectionResponse = $this->fakeInspectionResponseData();

        $this->json('PUT', '/api/v1/inspectionResponses/'.$inspectionResponse->id, $editedInspectionResponse);

        $this->assertApiResponse($editedInspectionResponse);
    }

    /**
     * @test
     */
    public function testDeleteInspectionResponse()
    {
        $inspectionResponse = $this->makeInspectionResponse();
        $this->json('DELETE', '/api/v1/inspectionResponses/'.$inspectionResponse->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/inspectionResponses/'.$inspectionResponse->id);

        $this->assertResponseStatus(404);
    }
}
