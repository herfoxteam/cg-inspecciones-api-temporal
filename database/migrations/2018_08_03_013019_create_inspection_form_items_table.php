<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInspectionFormItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_form_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category_hash');
            $table->text('label');
            $table->integer('type_forms_items_id')->unsigned();
            $table->string('cod_item', 10);
            $table->integer('required')->unsigned();
            $table->integer('published')->unsigned();
            $table->timestamps();
            $table->foreign('category_hash')->references('hash_category')->on('categories_inspections_forms');
            $table->foreign('type_forms_items_id')->references('id')->on('types_form_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inspection_form_items');
    }
}
