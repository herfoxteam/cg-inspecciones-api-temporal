<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomDateScheduledsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_dates_scheduled', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('custom_date');
            $table->integer('recurrence_schedules_id')->unsigned();
            $table->timestamps();
            $table->foreign('recurrence_schedules_id')->references('id')->on('recurrence_schedules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('custom_dates_scheduled');
    }
}
