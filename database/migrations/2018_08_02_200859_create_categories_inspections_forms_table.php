<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryInspectionFormsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_inspections_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_value');
            $table->string('label_value');
            $table->string('icon_class', 30);
            $table->string('template_code');
            $table->integer('status_categories_forms_id')->unsigned();
            $table->string('hash_category', 10);
            $table->timestamps();
            $table->foreign('template_code')->references('code')->on('template_inspections');
            $table->foreign('status_categories_forms_id')->references('id')->on('status_category_forms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories_inspections_forms');
    }
}
