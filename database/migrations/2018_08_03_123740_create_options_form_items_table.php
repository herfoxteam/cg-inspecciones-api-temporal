<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOptionFormItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options_form_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('form_code_item');
            $table->string('label');
            $table->string('value');
            $table->timestamps();
            $table->foreign('form_code_item')->references('cod_item')->on('inspection_form_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('options_form_items');
    }
}
