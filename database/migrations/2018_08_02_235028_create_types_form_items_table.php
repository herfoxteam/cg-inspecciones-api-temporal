<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTypeFormItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types_form_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 40);
            $table->text('description')->nullable();
            $table->string('type');
            $table->string('icon_name', 50);
            $table->text('rules')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('types_form_items');
    }
}
