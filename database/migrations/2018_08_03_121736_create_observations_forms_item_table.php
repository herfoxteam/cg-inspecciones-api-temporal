<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateObservationFormItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observations_forms_item', function (Blueprint $table) {
            $table->increments('id');
            $table->string('inspection_forms_items_code');
            $table->text('description');
            $table->string('hash_form');
            $table->timestamps();
            $table->foreign('inspection_forms_items_code')->references('cod_item')->on('inspection_form_items');
            $table->foreign('hash_form')->references('hash')->on('inspection_forms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('observations_forms_item');
    }
}
