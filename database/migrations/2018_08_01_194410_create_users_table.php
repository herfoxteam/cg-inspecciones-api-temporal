<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 70);
                $table->integer('role_id')->unsigned();
                $table->string('username', 30);
                $table->string('email', 70)->unique();
                $table->string('uid',70);
                $table->string('password');
                $table->rememberToken();
                $table->timestamps();
                $table->softDeletes();

            });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
