<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInspectionFormsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash', 15);
            $table->string('template_code');
            $table->integer('user_id')->unsigned();
            $table->integer('status_inspection_form_id')->unsigned();
            $table->timestamps();
            $table->foreign('template_code')->references('code')->on('template_inspections');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('status_inspection_form_id')->references('id')->on('status_inspection_forms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inspection_forms');
    }
}
