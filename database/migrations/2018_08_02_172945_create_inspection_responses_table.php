<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInspectionResponsesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspection_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->text('form_response');
            $table->string('inspections_forms_hash');
            $table->timestamps();
            $table->foreign('inspections_forms_hash')->references('hash')->on('inspection_forms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inspection_responses');
    }
}
