<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecurrenceScheduledsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recurrence_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_recurrence_id')->unsigned();
            $table->boolean('active');
            $table->integer('user_id')->unsigned();
            $table->integer('template_id')->unsigned();
            $table->datetime('start_date')->nullable();
            $table->timestamps();
            $table->foreign('type_recurrence_id')->references('id')->on('types_recurrences');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('template_id')->references('id')->on('template_inspections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recurrence_schedules');
    }
}
