<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePhotographicEvidencesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photographic_evidences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('inspection_forms_items_code');
            $table->string('thumbnail');
            $table->string('original_file');
            $table->file('image')->nullable();
            $table->string('hash_form');
            $table->timestamps();
            $table->foreign('inspection_forms_items_code')->references('cod_item')->on('inspection_form_items');
            $table->foreign('hash_form')->references('hash')->on('inspection_forms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photographic_evidences');
    }
}
