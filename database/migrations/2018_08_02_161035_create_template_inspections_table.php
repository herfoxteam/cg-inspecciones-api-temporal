<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTemplateInspectionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('template_inspections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 70);
            $table->text('description')->nullable();
            $table->integer('user_id')->insigned();
            $table->integer('status_template_id')->unsigned();
            $table->string('code', 10);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('status_template_id')->references('id')->on('status_template_inspections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('template_inspections');
    }
}
