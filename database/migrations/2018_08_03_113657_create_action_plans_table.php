<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActionPlansTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->datetime('date_time_end');
            $table->integer('assigned_user_id')->unsigned();
            $table->integer('priorities_id')->unsigned();
            $table->string('inspections_form_item_code');
            $table->boolean('closed')->default(false);
            $table->string('hash_form');
            $table->timestamps();
            $table->foreign('assigned_user_id')->references('id')->on('users');
            $table->foreign('priorities_id')->references('id')->on('priorities_actions_plans');
            $table->foreign('inspections_form_item_code')->references('cod_item')->on('inspection_form_items');
            $table->foreign('hash_form')->references('hash')->on('inspection_forms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('action_plans');
    }
}
