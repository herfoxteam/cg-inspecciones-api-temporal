<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('roles', 'RoleAPIController');

Route::resource('users', 'UserAPIController');

Route::resource('user_groups', 'UserGroupAPIController');

Route::resource('group_members', 'GroupMemberAPIController');

Route::resource('avatars', 'AvatarAPIController');

Route::resource('status_template_inspections', 'StatusTemplateInspectionAPIController');

Route::resource('template_inspections', 'TemplateInspectionAPIController');

Route::resource('status_inspection_forms', 'StatusInspectionFormAPIController');

Route::resource('inspection_forms', 'InspectionFormAPIController');

Route::resource('inspection_responses', 'InspectionResponseAPIController');

Route::resource('category_inspection_forms', 'CategoryInspectionFormAPIController');

Route::resource('status_category_forms', 'StatusCategoryFormAPIController');

Route::resource('type_form_items', 'TypeFormItemAPIController');

Route::resource('inspection_form_items', 'InspectionFormItemAPIController');

Route::resource('priority_action_plans', 'PriorityActionPlanAPIController');

Route::resource('action_plans', 'ActionPlanAPIController');

Route::resource('observation_form_items', 'ObservationFormItemAPIController');

Route::resource('option_form_items', 'OptionFormItemAPIController');

Route::resource('photographic_evidences', 'PhotographicEvidenceAPIController');

Route::resource('type_recurrences', 'TypeRecurrenceAPIController');

Route::resource('custom_date_scheduleds', 'CustomDateScheduledAPIController');

Route::resource('recurrence_scheduleds', 'RecurrenceScheduledAPIController');