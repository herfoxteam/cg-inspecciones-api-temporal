<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('roles', 'RoleController');

Route::resource('users', 'UserController');

Route::resource('userGroups', 'UserGroupController');

Route::resource('groupMembers', 'GroupMemberController');

Route::resource('avatars', 'AvatarController');

Route::resource('statusTemplateInspections', 'StatusTemplateInspectionController');

Route::resource('templateInspections', 'TemplateInspectionController');

Route::resource('statusInspectionForms', 'StatusInspectionFormController');

Route::resource('inspectionForms', 'InspectionFormController');

Route::resource('inspectionResponses', 'InspectionResponseController');

Route::resource('categoryInspectionForms', 'CategoryInspectionFormController');

Route::resource('statusCategoryForms', 'StatusCategoryFormController');

Route::resource('typeFormItems', 'TypeFormItemController');

Route::resource('inspectionFormItems', 'InspectionFormItemController');

Route::resource('priorityActionPlans', 'PriorityActionPlanController');

Route::resource('actionPlans', 'ActionPlanController');

Route::resource('observationFormItems', 'ObservationFormItemController');

Route::resource('optionFormItems', 'OptionFormItemController');

Route::resource('photographicEvidences', 'PhotographicEvidenceController');

Route::resource('typeRecurrences', 'TypeRecurrenceController');

Route::resource('customDateScheduleds', 'CustomDateScheduledController');

Route::resource('recurrenceScheduleds', 'RecurrenceScheduledController');